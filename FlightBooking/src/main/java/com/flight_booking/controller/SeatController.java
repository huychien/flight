/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flight_booking.controller;

import com.flight_booking.entities.SeatsEntity;
import com.flight_booking.enums.SeatStatus;
import com.flight_booking.enums.SeatTypeEnum;
import com.flight_booking.enums.Seats;
import com.flight_booking.service.AircraftService;
import com.flight_booking.service.SeatTypeService;
import com.flight_booking.service.SeatsService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author huycong
 */
@Controller
@RequestMapping("/admin")
public class SeatController {
    // Seat Controller  
    @Autowired
    private SeatsService seatsService;

    @Autowired
    private AircraftService aircraftService;

    @Autowired
    private SeatTypeService seatTypeService;

    @RequestMapping(value = {"/ticket"}, method = RequestMethod.GET)
    public String viewTicket(Model model,
            @RequestParam(name = "type", required = false) String type,
            @RequestParam(name = "message", required = false) String message) {

        List<SeatsEntity> seatsEntitys = seatsService.getSeats();
        model.addAttribute("seatType", seatTypeService.getType());
     
        model.addAttribute("aircrafts", aircraftService.getAircrafts());
        model.addAttribute("seats", seatsEntitys);
        model.addAttribute("type", type);
        model.addAttribute("message", message);
        return "admin/ticket";
    }

    @RequestMapping("/delete-seats/{id}")
    public String deleteTicket(Model model,
            @PathVariable("id") int id) {
        SeatsEntity seatsEntity = seatsService.findSeatsById(id);
        if (seatsEntity.getId() > 0) {
            if (!seatsService.deleteSeats(id)) {
                return "redirect:/admin/ticket?type=success&message=Delete id: " + id + " success";
            } else {
                return "redirect:/admin/ticket?type=error&message=Delete id: " + id + " fail";
            }
        } else {
            return "redirect:/admin/ticket?type=error&message=Not found id: " + id;
        }
    }

    @RequestMapping("/add_ticket_form")
    public String addTicket(Model model) {
        model.addAttribute("seats", new SeatsEntity());

        model.addAttribute("seatStatus", SeatStatus.values());
        model.addAttribute("seatsList", Seats.values()); 
        model.addAttribute("seatTypeEnum", SeatTypeEnum.values());
        model.addAttribute("seatType", seatTypeService.getType());
        model.addAttribute("aircrafts", aircraftService.getAircrafts());

        model.addAttribute("action", "add_ticket_form");
        return "admin/ticket_form";
    }

    @RequestMapping(value = "/add_ticket_form", method = RequestMethod.POST)
    public String createTicket(Model model,
            @RequestParam(name = "code", required = false) String code,
            @RequestParam(name = "aircraft", required = false) int aircraft,
            @ModelAttribute("seats") SeatsEntity seatsEntity) {
        
        SeatsEntity entity = seatsService.findCodeAndAircraft(seatsEntity.getCode(), seatsEntity.getAircraft());
        
        if (entity != null) {
            return "redirect:/admin/add_ticket_form";  
        }
        seatsService.save(seatsEntity, "add_ticket_form");
        return "redirect:/admin/ticket";
    }

    @RequestMapping("/update_ticket_form/{id}")
    public String updateTicket(Model model,
            @PathVariable("id") int id) {

        SeatsEntity seatsEntity = seatsService.findSeatsById(id); 
        if (seatsEntity.getId() > 0) {
            model.addAttribute("seats", seatsEntity);
            model.addAttribute("seatStatus", SeatStatus.values());
            model.addAttribute("seatsList", Seats.values());    
            model.addAttribute("seatType", seatTypeService.getType());
            model.addAttribute("seatTypeEnum", SeatTypeEnum.values());
            model.addAttribute("aircrafts", aircraftService.getAircrafts());

            model.addAttribute("action", "update_ticket");
            return "admin/ticket_form";
        } else {
            return "redirect:/admin/ticket?type=error&message=Not found account id: " + id;
        }
    }

    @RequestMapping(value = "/update_ticket", method = RequestMethod.POST)
    public String updateTicket(Model model,
            @ModelAttribute("seats") SeatsEntity seatsEntity) {
        seatsService.save(seatsEntity, "update_ticket");
        return "redirect:/admin/ticket";
    }
    
    @RequestMapping(value = "/searchTicket", method = RequestMethod.GET)
    public String searchSeat(Model model,
            @RequestParam(name = "id", required = false) int id) {
        
        model.addAttribute("seats", seatsService.findByAircraftId(id));
        model.addAttribute("aircrafts", aircraftService.getAircrafts());
        return "admin/ticket";
    }
}

