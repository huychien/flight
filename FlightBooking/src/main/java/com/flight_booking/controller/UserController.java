/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flight_booking.controller;

import com.flight_booking.entities.FlightEntity;
import com.flight_booking.entities.UserEntity;
import com.flight_booking.service.CityOfAirportService;
import com.flight_booking.service.FlightService;
import com.flight_booking.service.PromotionsService;
import com.flight_booking.service.SeatTypeService;
import com.flight_booking.service.UserRoleService;
import com.flight_booking.service.UserService;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private FlightService flightService;

    @Autowired
    private SeatTypeService seatTypeService;

    @Autowired
    private CityOfAirportService cityService;

    @Autowired
    private UserService userService;

    @Autowired
    private PromotionsService promotionsService;

    @Autowired
    private UserRoleService userRoleService;

//    @RequestMapping(value = "/home", method = RequestMethod.GET)
//    public String viewHome(Model model, @RequestParam(value = "error", required = false) boolean error,
//            @RequestParam(value = "bad_request", required = false) boolean bad_request,
//            @RequestParam(value = "timeUp", required = false) boolean timeUp,
//            HttpServletRequest request) {
//
//        HttpSession session = request.getSession();
//
//        if (error) {
//            model.addAttribute("error", "Flight not avaliable!!!");
//        }
//        if (bad_request) {
//            model.addAttribute("bad_request", "Please fill out this field!!!");
//        }
//        if (timeUp) {
//            model.addAttribute("timeUp", "Time up your booking!!!");
//        }
//
//        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//        String username = principal.toString();
//        if (principal instanceof UserDetails) {
//            username = ((UserDetails) principal).getUsername();
//        }
//
//        model.addAttribute("now", LocalDate.now());
//        model.addAttribute("departureCities", cityService.getCityOfAirports());
//        session.setAttribute("user", userService.getUser(username));
//        model.addAttribute("promotions", promotionsService.getAll());
//        return "user/home";
//    }

    @RequestMapping(value = "/viewInfo", method = RequestMethod.GET)
    public String viewInfo(Model model) {

        return "user/viewInfo";
    }

    @RequestMapping(value = "/change_password_page", method = RequestMethod.GET)
    public String changePasswordPage(Model model, @RequestParam(name = "type", required = false) String type,
            @RequestParam(name = "message", required = false) String message) {

        model.addAttribute("type", type);
        model.addAttribute("message", message);
        return "user/changePassword";
    }

    @RequestMapping(value = "/change_password", method = RequestMethod.GET)
    public String changePassword(Model model, @RequestParam(name = "old_password", required = false) String old_password,
            @RequestParam(name = "new_password_1", required = false) String new_password_1,
            @RequestParam(name = "news_password_2", required = false) String news_password_2) {

        try {
            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            String username = principal.toString();
            if (principal instanceof UserDetails) {
                username = ((UserDetails) principal).getUsername();
            }
            UserEntity user = userService.getUser(username);

            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

            if (user != null) {
                if (passwordEncoder.matches(old_password, user.getPassword())) {
                    if (new_password_1.equals(news_password_2)) {
                        user.setPassword(encrytePassword(new_password_1));
                        user.setUserRoles(userRoleService.setRoleUser());
                        userService.save(user);
                        return "redirect:/user/change_password_page?type=true&message=Change password success.";
                    } else {
                        return "redirect:/user/change_password_page?type=false&message=The two new passwords did not match.";
                    }
                } else {
                    return "redirect:/user/change_password_page?type=false&message=Incorrect password.";
                }
            }
        } catch (Exception e) {
            return "redirect:/home";
        }
        return "redirect:/user/change_password_page?type=false&message=No data.";
    }

    public static String encrytePassword(String password) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder.encode(password);
    }

    @RequestMapping(value = "/update_info_page", method = RequestMethod.GET)
    public String updateInfoPage(Model model, @RequestParam(name = "type", required = false) String type,
            @RequestParam(name = "message", required = false) String message) {

        model.addAttribute("type", type);
        model.addAttribute("message", message);
        return "user/updateInfo";
    }

    @RequestMapping(value = "/update_info", method = RequestMethod.POST)
    public String updateInfo(Model model, @ModelAttribute(name = "user") UserEntity user,
            @RequestParam(name = "birthDateStr") String birthDateStr,
            HttpServletRequest request) {

        try {
            HttpSession session = request.getSession();

            SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
            Date birthDate = formatter1.parse(birthDateStr);

            user.setBirthDate(birthDate);
            user.setUserRoles(userRoleService.setRoleUser());
            userService.save(user);
            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            String username = principal.toString();
            if (principal instanceof UserDetails) {
                username = ((UserDetails) principal).getUsername();
            }
            session.setAttribute("user", userService.getUser(username));

        } catch (ParseException e) {
            return "redirect:/user/update_info_page?type=false&message=Update faild.";
        }
        return "redirect:/user/update_info_page?type=true&message=Update success.";
    }

//    @RequestMapping(value = "/search", method = RequestMethod.GET)
//    public String searchFlights(Model model, @RequestParam(name = "departure", required = false) String departureStr,
//            @RequestParam(name = "destination") String destinationStr,
//            @RequestParam(name = "departureDate") String departureDate,
//            @RequestParam(name = "returnDate") String returnDate,
//            @RequestParam(name = "adults") int adults,
//            @RequestParam(name = "children") int children,
//            @RequestParam(name = "infants") int infants,
//            @RequestParam(name = "trip") String trip,
//            HttpServletRequest request) {
//        HttpSession session = request.getSession();
//
//        if (departureStr.isEmpty() || destinationStr.isEmpty()) {
//            return "redirect:/home?bad_request=true";
//        }
//
//        int departureId = Integer.parseInt(departureStr);
//        int destinationId = Integer.parseInt(destinationStr);
//
//        LocalDate date1 = LocalDate.parse(departureDate);
//
//        List<FlightEntity> goFlights = flightService.findByDepartureDateAndPlace(date1, departureId, destinationId);
//
//        if (goFlights.isEmpty() || CollectionUtils.isEmpty(goFlights)) {
//            return "redirect:/home?error=true";
//        }
//
//        if (trip.equals("round")) {
//
//            LocalDate date2 = LocalDate.parse(returnDate);
//            model.addAttribute("returnFlights", flightService.findByDepartureDateAndPlace(date2, destinationId, departureId));
//            model.addAttribute("dateFormat2", date2);
//        }
//
//        for (FlightEntity goFlight : goFlights) {
//            int i = goFlight.getAircraft().getId();
//            model.addAttribute("aircraft", i);
//            break;
//        }
//
//        model.addAttribute("goFlights", goFlights);
//        model.addAttribute("seatTypes", seatTypeService.getType());
//        model.addAttribute("departure", cityService.findById(departureId));
//        model.addAttribute("destination", cityService.findById(destinationId));
//        model.addAttribute("dateFormat1", date1);
//        session.setAttribute("adults", adults);
//        session.setAttribute("children", children);
//        session.setAttribute("infants", infants);
//        return "flights";
//    }

}
