/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flight_booking.controller;

import com.flight_booking.entities.AircraftEntity;
import com.flight_booking.entities.AirportEntity;
import com.flight_booking.entities.ImagesEntity;
import com.flight_booking.entities.SeatsEntity;
import com.flight_booking.enums.CommonStatus;
import com.flight_booking.enums.Seats;
import com.flight_booking.service.AircraftService;
import com.flight_booking.service.AirportService;
import com.flight_booking.service.CityOfAirportService;
import com.flight_booking.service.ImageService;
import com.flight_booking.service.SeatsService;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author huycong
 */
@Controller
@RequestMapping("/admin")
public class AircraftController {

    @Autowired
    private AircraftService aircraftService;

    @Autowired
    private ImageService imageService;

    @RequestMapping(value = {"/aircraft"}, method = RequestMethod.GET)
    public String home(Model model,
            @RequestParam(name = "type", required = false) String type,
            @RequestParam(name = "message", required = false) String message,
            HttpServletRequest request) {

        HttpSession session = request.getSession();
        model.addAttribute("commomstatus", CommonStatus.values());
        model.addAttribute("aircrafts", aircraftService.getAircrafts());
        model.addAttribute("seatsList", Seats.values());
        session.setAttribute("images", imageService.getImages());
        model.addAttribute("type", type);
        model.addAttribute("message", message);
        return "admin/aircraft";
    }

    @RequestMapping("/add_aircraft")
    public String addAircraft(Model model) {

        model.addAttribute("seatsList", Seats.values());
        model.addAttribute("aircraft", new AircraftEntity());
        model.addAttribute("commomstatus", CommonStatus.values());
        model.addAttribute("seatsList", Seats.values());

        model.addAttribute("action", "add-aircraft");
        return "admin/aircraft_form";
    }

    @RequestMapping(value = "/add-aircraft", method = RequestMethod.POST)
    public String createAircraft(Model model,
            @RequestParam("file") MultipartFile file,
            HttpSession session, HttpServletRequest request,
            @ModelAttribute("aircraft") AircraftEntity aircraft) {

        aircraftService.save(aircraft, "add-aircraft");
        ImagesEntity imageEntity = new ImagesEntity();
        if (!file.isEmpty()) {
            try {
                byte[] bytes = file.getBytes();
                // Creating the directory to store file
                ServletContext context = request.getServletContext();
                String pathUrl = context.getRealPath("/");

                String pathFolder = pathUrl + "/resources/img/aircraft";
                Path path = Paths.get(pathFolder + file.getOriginalFilename());
                Files.write(path, bytes);

                // sau khi upload file xong lấy file name ra để insert vào database
                String name = file.getOriginalFilename();

                imageEntity.setName(name);
                imageEntity.setAircraft(aircraft);

                imageService.save(imageEntity);

                return "redirect:/admin/aircraft";
            } catch (IOException e) {
                return "redirect:/admin/aircraft?type=error&message=" + e.getMessage();
            }
        } else {
            imageEntity.setAircraft(aircraft);
            imageService.save(imageEntity);
            return "redirect:/admin/aircraft?type=error&message=You have not uploaded an image "
                    + " because the file was empty.";
        }
    }

    @RequestMapping("/update-aircraft/{id}")
    public String updateAircra(Model model,
            @PathVariable("id") int id) {
        AircraftEntity aircraftEntity = aircraftService.findAircraftById(id);
        if (aircraftEntity.getId() > 0) {
            model.addAttribute("aircraft", aircraftEntity);
            model.addAttribute("commomstatus", CommonStatus.values());
            model.addAttribute("images", imageService.getImages());
            model.addAttribute("action", "update-aircraft");
            return "admin/aircraft_form";
        } else {
            return "redirect:/admin/aircraft?type=error&message=Not found  id: " + id;
        }
    }

    @RequestMapping(value = "/update-aircraft", method = RequestMethod.POST)
    public String updateAircraft(Model model,
            @RequestParam("file") MultipartFile file,
            HttpSession session, HttpServletRequest request,
            @ModelAttribute("aircraft") AircraftEntity aircraft) {

        aircraftService.save(aircraft, "update-aircraft");
        if (!file.isEmpty()) {
            try {
                byte[] bytes = file.getBytes();
                // Creating the directory to store file
                ServletContext context = request.getServletContext();
                String pathUrl = context.getRealPath("/");

                String pathFolder = pathUrl + "/resources/img/aircraft";
                Path path = Paths.get(pathFolder + file.getOriginalFilename());
                Files.write(path, bytes);

                // sau khi upload file xong lấy file name ra để insert vào database
                String name = file.getOriginalFilename();

                List<ImagesEntity> images = imageService.getImages();
                for (ImagesEntity image : images) {
                    if (image.getAircraft() != null) {
                        
                        if (image.getAircraft().getId() == aircraft.getId()) {
                            image.setName(name);
                            image.setAircraft(aircraft);
                            imageService.save(image);
                        }
                    }
                }
            } catch (IOException e) {
                return "redirect:/admin/aircraft?type=error&message=" + e.getMessage();
            }
        }
        return "redirect:/admin/aircraft";
    }

    @RequestMapping(value = "/search_aircraft", method = RequestMethod.GET)
    public String searchAircraft(Model model,
            @RequestParam(name = "strSearch", required = false) String schAircraft) {
       
        List<AircraftEntity> aircrafts = aircraftService.findByNameOrNumber(schAircraft);
        model.addAttribute("aircrafts", aircrafts);
        

        return "admin/aircraft";
    }
}
