/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flight_booking.controller;

import com.flight_booking.entities.BookingDetailEntity;
import com.flight_booking.entities.BookingEntity;
import com.flight_booking.entities.CreditCardEntity;
import com.flight_booking.entities.FlightEntity;
import com.flight_booking.entities.PaymentEntity;
import com.flight_booking.entities.UserEntity;
import com.flight_booking.enums.BookingStatus;
import com.flight_booking.enums.Gender;
import com.flight_booking.enums.PaymentStatus;
import com.flight_booking.enums.SeatStatus;
import com.flight_booking.enums.UserStatus;
import com.flight_booking.service.BookingDetailService;
import com.flight_booking.service.BookingService;
import com.flight_booking.service.CityOfAirportService;
import com.flight_booking.service.CreditCardService;
import com.flight_booking.service.FlightService;
import com.flight_booking.service.PaymentService;
import com.flight_booking.service.PromotionsService;
import com.flight_booking.service.SeatTypeService;
import com.flight_booking.service.SeatsService;
import com.flight_booking.service.UserRoleService;
import com.flight_booking.service.UserService;
import com.flight_booking.utils.SecurityUtils;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HomeController {

    @Autowired
    private FlightService flightService;

    @Autowired
    private SeatTypeService seatTypeService;

    @Autowired
    private CityOfAirportService cityService;

    @Autowired
    private BookingService bookingService;

    @Autowired
    private UserService userService;

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private CreditCardService creditCardService;

    @Autowired
    private BookingDetailService bookingDetailService;

    @Autowired
    private SeatsService seatsService;

    @Autowired
    private UserRoleService userRoleService;

    @Autowired
    private PromotionsService promotionsService;

    @RequestMapping(value = {"/", "/home"}, method = RequestMethod.GET)
    public String welcomePage(Model model, @RequestParam(value = "error", required = false) boolean error,
            @RequestParam(value = "bad_request", required = false) boolean bad_request,
            @RequestParam(value = "timeUp", required = false) boolean timeUp,
            HttpServletRequest request) {

        HttpSession session = request.getSession();

        if (error) {
            model.addAttribute("error", "Flight not avaliable!!!");
        }
        if (bad_request) {
            model.addAttribute("bad_request", "Please fill out this field!!!");
        }
        if (timeUp) {
            model.addAttribute("timeUp", "Time up your booking!!!");
        }

        model.addAttribute("now", LocalDate.now());
        model.addAttribute("departureCities", cityService.getCityOfAirports());
        model.addAttribute("title", "Welcome");
        model.addAttribute("message", "This is welcome page!");
        List<String> roles = SecurityUtils.getRolesOfUser();
        if (!CollectionUtils.isEmpty(roles) && (roles.contains("ROLE_ADMIN") || roles.contains("ROLE_MANAGER"))) {
            return "redirect:/admin/home";
        }

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = principal.toString();
        if (principal instanceof UserDetails) {
            username = ((UserDetails) principal).getUsername();
        }
        if (!username.equals("anonymousUser")) {
            session.setAttribute("user", userService.getUser(username));
        }
        model.addAttribute("promotions", promotionsService.getAll());
        return "home";
    }

    @RequestMapping("/login")
    public String loginPage(Model model, @RequestParam(value = "error", required = false) boolean error,
            @RequestParam(name = "type", required = false) String type,
            @RequestParam(name = "message", required = false) String message) {
        if (error) {
            model.addAttribute("error", "Login Fail!!!");
        }
        model.addAttribute("type", type);
        model.addAttribute("message", message);
        return "login";
    }

    @RequestMapping("/register")
    public String registerPage(Model model, @RequestParam(name = "fail", required = false) boolean fail,
            @RequestParam(name = "message", required = false) String message) {

        if (fail) {
            model.addAttribute("fail", "Email existed");
        }
        model.addAttribute("message", message);
        model.addAttribute("gender", Gender.values());
        return "register";
    }

    @RequestMapping(value = "/addUser", method = RequestMethod.POST)
    public String addUser(Model model, @ModelAttribute("user") UserEntity user) throws Exception {

        if (userService.getUser(user.getEmail()) == null) {
            user.setPassword(encrytePassword(user.getPassword()));
            user.setStatus(UserStatus.INACTIVE);
            user.setUserRoles(userRoleService.setRoleUser());
            userService.save(user);

            userService.sendEmail(user.getEmail(), "Hello " + user.getFullName(),
                    "<!DOCTYPE html>\n"
                    + "<html>\n"
                    + "    <head>\n"
                    + "        <title>TODO supply a title</title>\n"
                    + "        <meta charset=\"UTF-8\">\n"
                    + "        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n"
                    + "    </head>"
                    + "    <body>\n"
                    + "        <div style=\"text-align: center\">\n"
                    + "            <h3>Verify Account</h3>\n"
                    + "            <h5>Click the button below to confirm that you have registered for an account with FlightNow</h5>"
                    + "            <a style=\"color: white; background-color: deepskyblue; font-weight: bold; padding: 10px 20px; border: none; text-decoration: none;\" href = 'http://localhost:8080/FlightBooking/verify_account/" + user.getId() + "'>Verify</a>"
                    + "        </div>\n"
                    + "    </body>\n"
                    + "</html>");
        } else {
            return "redirect:/register?fail=true";
        }
        return "redirect:/register?message=Please go to your email to confirm your account";
    }

    @RequestMapping("/verify_account/{id}")
    public String verifyAccount(Model model, @PathVariable(name = "id") Integer id) {

        UserEntity user = userService.findById(id);
        user.setStatus(UserStatus.ACTIVE);
        user.setUserRoles(userRoleService.setRoleUser());
        userService.save(user);

        return "redirect:/login?type=true&message=Successful registration and now let's login.";
    }

    @RequestMapping("/findAccount")
    public String findAccount(Model model) {

        try {
            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            String username = principal.toString();
            if (principal instanceof UserDetails) {
                username = ((UserDetails) principal).getUsername();
            }
            UserEntity user = userService.getUser(username);
            if (user.getStatus().equals(UserStatus.INACTIVE)) {
                return "redirect:/logout";
            }
        } catch (Exception e) {
            return "redirect:/home";
        }
        return "redirect:/home";
    }

    public static String encrytePassword(String password) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder.encode(password);
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String searchFlights(Model model, @RequestParam(name = "departure") String departureStr,
            @RequestParam(name = "destination") String destinationStr,
            @RequestParam(name = "departureDate") String departureDate,
            @RequestParam(name = "returnDate") String returnDate,
            @RequestParam(name = "adults") int adults,
            @RequestParam(name = "children") int children,
            @RequestParam(name = "infants") int infants,
            @RequestParam(name = "trip") String trip,
            @RequestParam(name = "message", required = false) String message,
            HttpServletRequest request) {
        HttpSession session = request.getSession();

        if (departureStr.isEmpty() || destinationStr.isEmpty()) {
            return "redirect:/home?bad_request=true";
        }

        int departureId = Integer.parseInt(departureStr);
        int destinationId = Integer.parseInt(destinationStr);

        LocalDate date1 = LocalDate.parse(departureDate);

        List<FlightEntity> goFlights = flightService.findByDepartureDateAndPlace(date1, departureId, destinationId);

        if (goFlights.isEmpty() || CollectionUtils.isEmpty(goFlights)) {
            return "redirect:/home?error=true";
        }

        if (trip.equals("round")) {

            LocalDate date2 = LocalDate.parse(returnDate);

            model.addAttribute("returnFlights", flightService.findByDepartureDateAndPlace(date2, destinationId, departureId));
            model.addAttribute("dateFormat2", date2);
        }

        model.addAttribute("goFlights", goFlights);
        model.addAttribute("seatTypes", seatTypeService.getType());
        model.addAttribute("departure", cityService.findById(departureId));
        model.addAttribute("destination", cityService.findById(destinationId));
        model.addAttribute("dateFormat1", date1);
        session.setAttribute("adults", adults);
        session.setAttribute("children", children);
        session.setAttribute("infants", infants);
        model.addAttribute("message", message);
        return "flights";
    }

    @RequestMapping("/403")
    public String accessDenied(Model model) {
        return "403page";
    }

    @RequestMapping("/info_booking")
    public String InfoBooking(Model model, @RequestParam(name = "type", required = false) String type,
            @RequestParam(name = "message", required = false) String message,
            HttpServletRequest request) {

        HttpSession session = request.getSession();
        UserEntity user = (UserEntity) session.getAttribute("user");

        if (user != null) {
            model.addAttribute("list_booking", bookingService.findByUser(user));
        }

        model.addAttribute("type", type);
        model.addAttribute("message", message);
        return "info_booking";
    }

    @RequestMapping("/search_booking")
    public String searchBooking(Model model, @RequestParam(name = "code") String code) {

        model.addAttribute("list_booking", bookingService.findByBookingCode(code));
        return "info_booking";
    }

    @RequestMapping(value = "/booking_detail", method = RequestMethod.GET)
    public String bookingDetail(Model model, @RequestParam(name = "id", required = false) String idStr,
            @RequestParam(name = "type", required = false) String type,
            @RequestParam(name = "message", required = false) String message) {

        if (idStr != null) {
            int id = Integer.parseInt(idStr);
            model.addAttribute("paymentAmount", paymentService.findByBookingId(id));
            model.addAttribute("booking_details", bookingDetailService.findByBookingId(id));
        }
        model.addAttribute("type", type);
        model.addAttribute("message", message);
        return "booking_detail";
    }

    @Transactional(rollbackFor = Exception.class)
    @RequestMapping("/cancel_booking_detail/{booking_detail_id}/{booking_id}")
    public String cancelBookingDetail(Model model, @PathVariable(name = "booking_detail_id") Integer booking_detail_id,
            @PathVariable(name = "booking_id") Integer booking_id) throws Exception {

        try {
            PaymentEntity payment = paymentService.findByBookingId(booking_id);

            LocalDateTime departureDateTime1 = LocalDateTime.now();
            List<BookingDetailEntity> list_booking_detail = payment.getBooking().getBookingDetailEntities();

            for (int i = 0; i < list_booking_detail.size(); i++) {
                if (i >= 1) {
                    departureDateTime1 = LocalDateTime.of(list_booking_detail.get(i - 1).getFlight().getDepartureDate(), list_booking_detail.get(i - 1).getFlight().getDepartureTime());
                    LocalDateTime departureDateTime2 = LocalDateTime.of(list_booking_detail.get(i).getFlight().getDepartureDate(), list_booking_detail.get(i).getFlight().getDepartureTime());

                    if (departureDateTime1.isAfter(departureDateTime2)) {
                        departureDateTime1 = departureDateTime2;
                    }
                } else {
                    departureDateTime1 = LocalDateTime.of(list_booking_detail.get(i).getFlight().getDepartureDate(), list_booking_detail.get(i).getFlight().getDepartureTime());
                }
            }
            LocalDateTime now = LocalDateTime.now();

            Duration duration = Duration.between(now, departureDateTime1);
            long daysLeft = duration.toDays();

            if (daysLeft > 1) {

                BookingDetailEntity bookingDetailEntity = bookingDetailService.findById(booking_detail_id);

                if (bookingDetailEntity.getSeat() != null) {

                    CreditCardEntity creditCard = creditCardService.findById(payment.getCreditCard().getId());
                    BookingEntity bookingEntity = bookingService.findById(booking_id);

                    creditCard.setBalance(creditCard.getBalance() + (bookingDetailEntity.getUnitPrice() * (100 - 20) / 100));

                    payment.setAmount(payment.getAmount() - (bookingDetailEntity.getUnitPrice() * (100 - 20) / 100));
                    payment.setCreditCard(creditCard);

                    bookingDetailEntity.getSeat().setSeatStatus(SeatStatus.AVAILABLE);
                    seatsService.save(bookingDetailEntity.getSeat());

                    bookingDetailEntity.setStatus(BookingStatus.CANCEL);
                    bookingDetailService.save(bookingDetailEntity);

                    boolean check = true;
                    for (BookingDetailEntity bookingDetail : list_booking_detail) {

                        bookingDetail = bookingDetailService.findById(bookingDetail.getId());
                        if (bookingDetail.getStatus().equals(BookingStatus.BOOKED)) {
                            check = false;
                        }
                    }

                    if (check == true) {
                        payment.setStatus(PaymentStatus.REFUND);
                        bookingEntity.setStatus(BookingStatus.CANCEL);
                    }
                    payment.setBooking(bookingEntity);

                    bookingService.save(bookingEntity);
                    paymentService.save(payment);
                    creditCardService.save(creditCard);

                    return "redirect:/booking_detail?id=" + booking_id + "&type=true&message=Delete success. We have refunded 80%25 of the amount to the order account.";
                }
            }

        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return "redirect:/booking_detail?id=" + booking_id + "&type=false&message=Delete failed.";
        }
        return "redirect:/booking_detail?id=" + booking_id + "&type=false&message=Delete failed. You can only delete passenger 1 day before the check-in date.";
    }

    @Transactional(rollbackFor = Exception.class)
    @RequestMapping("/cancel_booking/{id}")
    public String cancelBooking(Model model, @PathVariable(name = "id") int id) throws Exception {

        try {
            PaymentEntity payment = paymentService.findByBookingId(id);

            LocalDateTime departureDateTime1 = LocalDateTime.now();
            List<BookingDetailEntity> list_booking = payment.getBooking().getBookingDetailEntities();

            for (int i = 0; i < list_booking.size(); i++) {
                if (i >= 1) {
                    departureDateTime1 = LocalDateTime.of(list_booking.get(i - 1).getFlight().getDepartureDate(), list_booking.get(i - 1).getFlight().getDepartureTime());
                    LocalDateTime departureDateTime2 = LocalDateTime.of(list_booking.get(i).getFlight().getDepartureDate(), list_booking.get(i).getFlight().getDepartureTime());

                    if (departureDateTime1.isAfter(departureDateTime2)) {
                        departureDateTime1 = departureDateTime2;
                    }
                } else {
                    departureDateTime1 = LocalDateTime.of(list_booking.get(i).getFlight().getDepartureDate(), list_booking.get(i).getFlight().getDepartureTime());
                }
            }
            LocalDateTime now = LocalDateTime.now();

            Duration duration = Duration.between(now, departureDateTime1);
            long daysLeft = duration.toDays();

            if (daysLeft > 1) {

                for (BookingDetailEntity bookingDetailEntity : list_booking) {
                    if (bookingDetailEntity.getSeat() != null) {
                        bookingDetailEntity.setStatus(BookingStatus.CANCEL);
                        bookingDetailEntity.getSeat().setSeatStatus(SeatStatus.AVAILABLE);
                        seatsService.save(bookingDetailEntity.getSeat());
                    }
                }

                CreditCardEntity creditCard = creditCardService.findById(payment.getCreditCard().getId());

                creditCard.setBalance(creditCard.getBalance() + (payment.getAmount() * (100 - 20) / 100));

                payment.setAmount(payment.getAmount() * (100 - 80) / 100);
                payment.setStatus(PaymentStatus.REFUND);
                payment.getBooking().setStatus(BookingStatus.CANCEL);

                bookingDetailService.saveAll(list_booking);
                bookingService.save(payment.getBooking());
                paymentService.save(payment);
                creditCardService.save(creditCard);

                return "redirect:/info_booking?type=true&message=Cancel success. We have refunded 80%25 of the amount to the order account.";
            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return "redirect:/info_booking?type=false&message=Cancel failed.";
        }
        return "redirect:/info_booking?type=false&message=Cancel failed. You can only cancel the booking 1 day before the check-in date.";
    }

}
