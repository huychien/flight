/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flight_booking.controller;

import com.flight_booking.entities.AirportEntity;
import com.flight_booking.entities.BookingEntity;
import com.flight_booking.entities.CityOfAirportEntity;
import com.flight_booking.entities.FlightEntity;
import com.flight_booking.entities.FlightRouteEntity;
import com.flight_booking.entities.ImagesEntity;
import com.flight_booking.entities.PromotionEntity;
import com.flight_booking.entities.SeatTypeEntity;
import com.flight_booking.entities.SeatsEntity;
import com.flight_booking.enums.CommonStatus;
import com.flight_booking.enums.FlightStatus;
import com.flight_booking.enums.SeatStatus;
import com.flight_booking.enums.SeatTypeEnum;
import com.flight_booking.enums.Seats;
import com.flight_booking.service.AircraftService;
import com.flight_booking.service.AirportService;
import com.flight_booking.service.BookingDetailService;
import com.flight_booking.service.BookingService;
import com.flight_booking.service.CityOfAirportService;
import com.flight_booking.service.FlightRouteService;
import com.flight_booking.service.FlightService;
import com.flight_booking.service.ImageService;
import com.flight_booking.service.PromotionService;
import com.flight_booking.service.SeatTypeService;
import com.flight_booking.service.SeatsService;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    private FlightRouteService flightRouteService;

    @RequestMapping("/home")
    public String viewHome(Model model) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = principal.toString();
        if (principal instanceof UserDetails) {
            username = ((UserDetails) principal).getUsername();
        }

        model.addAttribute("message", "Hello Admin: " + username);
        return "admin/home";
    }

    @RequestMapping(value = {"/logout"}, method = RequestMethod.GET)
    public String logout(HttpServletRequest request, SessionStatus session) {
        session.setComplete();
        request.getSession().invalidate();
        return "login";
    }

    @RequestMapping("/aircraft")
    public String viewAircraft(Model model) {

        return "admin/aircraft";
    }

    @Autowired
    private AirportService airportService;

    @Autowired
    private CityOfAirportService cityOfAirportService;

    @RequestMapping(value = {"/airport"}, method = RequestMethod.GET)
    public String home(Model model,
            @RequestParam(name = "type", required = false) String type,
            @RequestParam(name = "message", required = false) String message) {
        model.addAttribute("cityOfAirports", cityOfAirportService.getCityOfAirports());
        model.addAttribute("airports", airportService.getAirports());
        model.addAttribute("type", type);
        model.addAttribute("message", message);
        return "admin/airport";
    }

    @RequestMapping("/add_airport")
    public String addAirport(Model model) {
        model.addAttribute("airport", new AirportEntity());
        model.addAttribute("cityOfAirports", cityOfAirportService.getCityOfAirports());
        model.addAttribute("commomstatus", CommonStatus.values());
        model.addAttribute("action", "add");
        return "admin/airport_form";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String createAirport(Model model,
            @ModelAttribute("airport") AirportEntity airportEntity) { 
        airportService.save(airportEntity, "add");
        return "redirect:/admin/airport";
    }

    @RequestMapping("/update/{id}")
    public String updateAirport(Model model,
            @PathVariable("id") int id) {
        AirportEntity airportEntity = airportService.findAirportById(id);
        if (airportEntity.getId() > 0) {
            model.addAttribute("airport", airportEntity);
            model.addAttribute("commomstatus", CommonStatus.values());
            model.addAttribute("cityOfAirports", cityOfAirportService.getCityOfAirports());
            model.addAttribute("action", "update");
            return "admin/airport_form";
        } else {
            return "redirect:/admin/airport?type=error&message=Not found id: " + id;
        }
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String updateAirport(Model model,
            @ModelAttribute("airport") AirportEntity airportEntity) {
        airportService.save(airportEntity, "update");
        return "redirect:/admin/airport";
    }

    @RequestMapping(value = "/search_airport", method = RequestMethod.GET)
    public String searchAirport(Model model,
            @RequestParam(name = "strSearch", required = false) String strSearch) {
        model.addAttribute("airports", airportService.findByCityNameOrAirportName(strSearch));
        model.addAttribute("cityOfAirports", cityOfAirportService.getCityOfAirports());
        return "admin/airport";
    }

    // flight Route Controller
    @RequestMapping(value = {"/flight_route"}, method = RequestMethod.GET)
    public String homeFlightRoute(Model model,
            @RequestParam(name = "type", required = false) String type,
            @RequestParam(name = "message", required = false) String message) {

        List<FlightRouteEntity> entitys = flightRouteService.getFlightRoutes();

        model.addAttribute("cityOfAirports", cityOfAirportService.getCityOfAirports());
        model.addAttribute("airports", airportService.getAirports());
        model.addAttribute("commomstatus", CommonStatus.values());
        model.addAttribute("flightRoutes", entitys);
        model.addAttribute("type", type);
        model.addAttribute("message", message);
        return "admin/flight_route";
    }

    @RequestMapping("/delete-flight_route/{id}")
    public String deleteFlightRoute(Model model,
            @PathVariable("id") int id) {
        FlightRouteEntity f = flightRouteService.findFlightRouteById(id);
        if (f.getId() > 0) {
            if (!flightRouteService.deleteFlightRoute(id)) {
                return "redirect:/admin/flight_route?type=success&message=Delete id: " + id + " success";
            } else {
                return "redirect:/admin/flight_route?type=error&message=Delete id: " + id + " fail";
            }
        } else {
            return "redirect:/admin/flight_route?type=error&message=Not found  id: " + id;
        }
    }

    @RequestMapping("/add_flight_route")
    public String addFlightRoute(Model model) {
        model.addAttribute("flightRoute", new FlightRouteEntity());
        model.addAttribute("airports", airportService.getAirports());
        model.addAttribute("cityOfAirports", cityOfAirportService.getCityOfAirports());
        model.addAttribute("commomstatus", CommonStatus.values());
        model.addAttribute("action", "add_flight_route");
        return "admin/flight_route_form";
    }

    @RequestMapping(value = "/add_flight_route", method = RequestMethod.POST)
    public String createFlightRoute(Model model, @RequestParam(name = "durationStr", required = false) String durationStr,
            @ModelAttribute("flightRoute") FlightRouteEntity flightRouteEntity) {

        LocalTime duration = LocalTime.parse(durationStr);
        flightRouteEntity.setDuration(duration);
        flightRouteService.save(flightRouteEntity);
        return "redirect:/admin/flight_route";
    }

    @RequestMapping(value = "/search_flight_route", method = RequestMethod.GET)
    public String searchFlightRoute(Model model,
            @RequestParam(name = "strSearch", required = false) String departureName) {

        model.addAttribute("flightRoutes", flightRouteService.findByDepartureName(departureName));
        return "admin/flight_route";
    }

    //Flight Controller
    @Autowired
    private FlightService flightService;

    @Autowired
    private AircraftService aircraftService;

    @RequestMapping(value = {"/flight"}, method = RequestMethod.GET)
    public String viewFlight(Model model,
            @RequestParam(name = "type", required = false) String type,
            @RequestParam(name = "message", required = false) String message) {

        model.addAttribute("flights", flightService.getFlights());
        model.addAttribute("statusFlight", FlightStatus.values());
        model.addAttribute("flightRoute", flightRouteService.getFlightRoutes());
        model.addAttribute("aircraft", aircraftService.getAircrafts());
        model.addAttribute("cityOfAirports", cityOfAirportService.getCityOfAirports());
        model.addAttribute("airport", airportService.getAirports());
        model.addAttribute("type", type);
        model.addAttribute("message", message);
        return "admin/flight";
    }

    @RequestMapping("/add_flight")
    public String addFlight(Model model) {

        model.addAttribute("flights", new FlightEntity());
        model.addAttribute("statusFlight", FlightStatus.values());
        model.addAttribute("flightRoute", flightRouteService.getFlightRoutes());
        model.addAttribute("aircraft", aircraftService.getAircrafts());
        model.addAttribute("cityOfAirports", cityOfAirportService.getCityOfAirports());
        model.addAttribute("airport", airportService.getAirports());
        model.addAttribute("action", "add_flight");
        return "admin/flight_form";
    }

//    @RequestMapping(value = "/add_flight", method = RequestMethod.POST)
//    public String createFlight(Model model,
//            @RequestParam(name = "departureStr", required = false) String departureStr,
//            @RequestParam(name = "arriveStr", required = false) String arriveStr,
//            @RequestParam(name = "departureDateStr", required = false) String departureDateStr,
//            @ModelAttribute("flights") FlightEntity flightEntity) {
//
//        LocalTime departureTime = LocalTime.parse(departureStr);
//        LocalTime arriveTime = LocalTime.parse(arriveStr);
//        LocalDate departureDate = LocalDate.parse(departureDateStr);
//
//        flightEntity.setDepartureDate(departureDate);
//        flightEntity.setArriveTime(arriveTime);
//        flightEntity.setDepartureTime(departureTime);
//
//        flightService.save(flightEntity, "add_flight");
//        return "redirect:/admin/flight";
//    }

    @RequestMapping("/delete_flight/{id}")
    public String deleteFlight(Model model,
            @PathVariable("id") int id) {
        FlightEntity f = flightService.findFlightById(id);
        if (f.getId() > 0) {
            if (!flightService.deleteFlight(id)) {
                return "redirect:/admin/flight?type=success&message=Delete id: " + id + " success";
            } else {
                return "redirect:/admin/flight?type=error&message=Delete id: " + id + " fail";
            }
        } else {
            return "redirect:/admin/flight?type=error&message=Not found id: " + id;
        }
    }

    @RequestMapping("/update_flight/{id}")
    public String updateFlight(Model model,
            @PathVariable("id") int id) {
        FlightEntity f = flightService.findFlightById(id);

        if (f.getId() > 0) {
            model.addAttribute("statusFlight", FlightStatus.values());
            model.addAttribute("flightRoute", flightRouteService.getFlightRoutes());
            model.addAttribute("aircraft", aircraftService.getAircrafts());
            model.addAttribute("cityOfAirports", cityOfAirportService.getCityOfAirports());
            model.addAttribute("airport", airportService.getAirports());
            model.addAttribute("flights", f);
            model.addAttribute("action", "update_flight");
            return "admin/flight_form";
        } else {
            return "redirect:/admin/flight?type=error&message=Not found id: " + id;
        }
    }

//    @RequestMapping(value = "/update_flight", method = RequestMethod.POST)
//    public String updateFlight(Model model,
//            @RequestParam(name = "departureStr", required = false) String departureStr,
//            @RequestParam(name = "arriveStr", required = false) String arriveStr,
//            @RequestParam(name = "departureDateStr", required = false) String departureDateStr,
//            @ModelAttribute("flights") FlightEntity flightEntity) {
//
//        LocalTime departureTime = LocalTime.parse(departureStr);
//        LocalTime arriveTime = LocalTime.parse(arriveStr);
//        LocalDate departureDate = LocalDate.parse(departureDateStr);
//
//        flightEntity.setDepartureDate(departureDate);
//        flightEntity.setArriveTime(arriveTime);
//        flightEntity.setDepartureTime(departureTime);
//
//        flightService.save(flightEntity, "update_flight");
//        return "redirect:/admin/flight";
//    }

    @RequestMapping(value = "/search_flight", method = RequestMethod.GET)
    public String searchFlight(Model model,
            @RequestParam(name = "date", required = false) String departureDate) {

        LocalDate date1 = LocalDate.parse(departureDate);

        model.addAttribute("flights", flightService.findByDepartureDate(date1));
        return "admin/flight";
    }

    //Promotion Controller
    @Autowired
    private PromotionService promotionService;

    @Autowired
    private ImageService imageService;

    @RequestMapping(value = {"/promotion"}, method = RequestMethod.GET)
    public String viewPromotion(Model model,
            @RequestParam(name = "type", required = false) String type,
            @RequestParam(name = "message", required = false) String message,
            HttpServletRequest request) {
        HttpSession session = request.getSession();

        session.setAttribute("images", imageService.getImages());

        model.addAttribute("promotions", promotionService.getPromotions());
        return "admin/promotion";
    }

    @RequestMapping("/add_promotion")
    public String addPromotion(Model model) {

        model.addAttribute("promotions", new PromotionEntity());
        model.addAttribute("action", "add_promotion");
        return "admin/promotion_form";
    }

    @RequestMapping(value = "/add_promotion", method = RequestMethod.POST)
    public String createPromotiom(Model model,
            @RequestParam(name = "startDateStr", required = false) String startDateStr,
            @RequestParam(name = "endDateStr", required = false) String endDateStr,
            @ModelAttribute("promotions") PromotionEntity promotionEntity,
            @RequestParam("file") MultipartFile file,
            HttpSession session, HttpServletRequest request
    ) {

        LocalDate startDate = LocalDate.parse(startDateStr);
        LocalDate endDate = LocalDate.parse(endDateStr);

        promotionEntity.setStartDate(startDate);
        promotionEntity.setEndDate(endDate);

        promotionService.save(promotionEntity, "add_promotion");
        ImagesEntity imageEntity = new ImagesEntity();
        if (!file.isEmpty()) {
            try {
                byte[] bytes = file.getBytes();
                // Creating the directory to store file
                ServletContext context = request.getServletContext();
                String pathUrl = context.getRealPath("/");

                String pathFolder = pathUrl + "/resources/img/promotion";
                Path path = Paths.get(pathFolder + file.getOriginalFilename());
                Files.write(path, bytes);

                // sau khi upload file xong lấy file name ra để insert vào database
                String name = file.getOriginalFilename();

                imageEntity.setName(name);
                imageEntity.setPromotion(promotionEntity);

                imageService.save(imageEntity);

                return "redirect:/admin/promotion";
            } catch (IOException e) {
                return "redirect:/admin/promotion?type=error&message=" + e.getMessage();
            }
        } else {
            imageEntity.setPromotion(promotionEntity);
            imageService.save(imageEntity);
            return "redirect:/admin/promotion?type=error&message=You have not uploaded an image "
                    + " because the file was empty.";
        }

    }

    @RequestMapping("/delete_promotion/{id}")
    public String deletePromotion(Model model,
            @PathVariable("id") int id,
            HttpServletRequest request, HttpServletResponse response) {

        HttpSession session = request.getSession();
        PromotionEntity f = promotionService.findPromotionById(id);

        List<PromotionEntity> promotionEntitys = promotionService.getPromotions();

        if (f.getId() > 0) {
            if (!promotionService.deletePromotion(id)) {

                model.addAttribute("promotions", promotionEntitys);
                return "redirect:/admin/promotion?type=success&message=delete success";
            } else {
                return "redirect:/admin/promotion?type=error&message=delete failed";
            }
        } else {
            return "redirect:/admin/promotion?type=error&message=Not found id: " + id;
        }
    }

    @RequestMapping("/update_promotion/{id}")
    public String updatePromotion(Model model,
            @PathVariable("id") int id) {
        PromotionEntity f = promotionService.findPromotionById(id);
        if (f.getId() > 0) {
            model.addAttribute("promotions", f);
            model.addAttribute("images", imageService.getImages());
            model.addAttribute("action", "update_promotion");

            return "admin/promotion_form";
        } else {
            return "redirect:/admin/promotion?type=error&message=Not found id: " + id;
        }
    }

    @RequestMapping(value = "/update_promotion", method = RequestMethod.POST)
    public String updatePromotion(Model model,
            @RequestParam(name = "startDateStr", required = false) String startDateStr,
            @RequestParam(name = "endDateStr", required = false) String endDateStr,
            @ModelAttribute("promotions") PromotionEntity promotionEntity,
            @RequestParam("file") MultipartFile file,
            HttpSession session, HttpServletRequest request) {

        LocalDate startDate = LocalDate.parse(startDateStr);
        LocalDate endDate = LocalDate.parse(endDateStr);

        promotionEntity.setStartDate(startDate);
        promotionEntity.setEndDate(endDate);

        promotionService.save(promotionEntity, "update_promotion");
        if (!file.isEmpty()) {
            try {
                byte[] bytes = file.getBytes();
                // Creating the directory to store file
                ServletContext context = request.getServletContext();
                String pathUrl = context.getRealPath("/");

                String pathFolder = pathUrl + "/resources/img/promotion";
                Path path = Paths.get(pathFolder + file.getOriginalFilename());
                Files.write(path, bytes);

                // sau khi upload file xong lấy file name ra để insert vào database
                String name = file.getOriginalFilename();

                List<ImagesEntity> images = imageService.getImages();
                for (ImagesEntity image : images) {
                    if (image.getPromotion() != null) {

                        if (image.getPromotion().getId() == promotionEntity.getId()) {
                            image.setName(name);
                            image.setPromotion(promotionEntity);
                            imageService.save(image);
                        }
                    }
                }
            } catch (IOException e) {
                return "redirect:/admin/promotion?type=error&message=" + e.getMessage();
            }
        }
        return "redirect:/admin/promotion";

    }

    @RequestMapping(value = "/search_promotion", method = RequestMethod.GET)
    public String searchPromotion(Model model,
            @RequestParam(name = "startDate", required = false) String startDate) {

        LocalDate date2 = LocalDate.parse(startDate);

        model.addAttribute("promotions", promotionService.findByStartDate(date2));
        return "admin/promotion";
    }

    @Autowired
    private BookingService bookingService;

    @RequestMapping(value = {"/booking"}, method = RequestMethod.GET)
    public String viewBooking(Model model,
            @RequestParam(name = "type", required = false) String type,
            @RequestParam(name = "message", required = false) String message) {

        model.addAttribute("bookinges", bookingService.getBookinges());
        return "admin/booking";
    }

    @Autowired
    private BookingDetailService bookingDetailService;

//    @RequestMapping(value = {"/booking_detail"}, method = RequestMethod.GET)
//    public String viewBookingDetail(Model model,
//            @RequestParam(name = "type", required = false) String type,
//            @RequestParam(name = "message", required = false) String message) {
//
//        model.addAttribute("bookingDetail", bookingDetailService.getBookingDetails());
//        return "admin/booking_detail";
//    }
    
    @RequestMapping("/bookingdetail/{id}")
    public String viewBookingDetail(Model model,
            @PathVariable("id") int id) {
 
        model.addAttribute("bookingDetail", bookingDetailService.findByBookingId(id));
        return "admin/booking_detail";
        }
    }

