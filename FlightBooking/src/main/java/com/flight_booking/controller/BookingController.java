/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flight_booking.controller;

import com.flight_booking.entities.BookingDetailEntity;
import com.flight_booking.entities.BookingEntity;
import com.flight_booking.entities.CreditCardEntity;
import com.flight_booking.entities.FlightEntity;
import com.flight_booking.entities.PassengerEntity;
import com.flight_booking.entities.PaymentEntity;
import com.flight_booking.entities.SeatsEntity;
import com.flight_booking.entities.UserEntity;
import com.flight_booking.enums.BookingStatus;
import com.flight_booking.enums.PaymentMethod;
import com.flight_booking.enums.PaymentStatus;
import com.flight_booking.enums.SeatStatus;
import com.flight_booking.model.BookingFlight;
import com.flight_booking.service.BookingDetailService;
import com.flight_booking.service.BookingService;
import com.flight_booking.service.CreditCardService;
import com.flight_booking.service.FlightService;
import com.flight_booking.service.PassengerService;
import com.flight_booking.service.PaymentService;
import com.flight_booking.service.SeatsService;
import com.flight_booking.service.UserService;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author huythang
 */
@Controller
public class BookingController {

    @Autowired
    private FlightService flightService;

    @Autowired
    private SeatsService seatsService;

    @Autowired
    private UserService userService;

    @Autowired
    private CreditCardService creditCardService;

    @Autowired
    private PassengerService passengerService;

    @Autowired
    private BookingService bookingService;

    @Autowired
    private BookingDetailService bookingDetailService;

    @Autowired
    private PaymentService paymentService;

    @RequestMapping(value = "/seats", method = RequestMethod.GET)
    public String ordererPage(Model model, @RequestParam(name = "flight_go_id", required = false) int flight_go_id,
            @RequestParam(name = "flight_return_id", required = false) int flight_return_id,
            @RequestParam(name = "seatType_go", required = false) String seatType_go,
            @RequestParam(name = "seatType_return", required = false) String seatType_return,
            HttpServletRequest request) {

        HttpSession session = request.getSession();

        BookingFlight bookingFlight = (BookingFlight) session.getAttribute("bookingFlight");

        if (bookingFlight == null) {
            bookingFlight = new BookingFlight();
        }

        FlightEntity flightGo = flightService.findById(flight_go_id);
        bookingFlight.setFlightGo(flightGo);

        if (flight_return_id != 0) {
            FlightEntity flightReturn = flightService.findById(flight_return_id);

            bookingFlight.setFlightReturn(flightReturn);
            model.addAttribute("seatsOfAircraftReturn", seatsService.findByAircraft(flightReturn.getAircraft()));
            model.addAttribute("seatType_return", seatType_return);
        }

        if (bookingFlight.getSeatIdsGo() != null) {
            for (int i = 0; i < bookingFlight.getSeatIdsGo().length; i++) {
                SeatsEntity seat = seatsService.findSeatsById(bookingFlight.getSeatIdsGo()[i]);
                seat.setSeatStatus(SeatStatus.AVAILABLE);
                seatsService.save(seat);
            }
        }
        if (bookingFlight.getSeatIdsReturn() != null) {
            for (int i = 0; i < bookingFlight.getSeatIdsReturn().length; i++) {
                SeatsEntity seat = seatsService.findSeatsById(bookingFlight.getSeatIdsReturn()[i]);
                seat.setSeatStatus(SeatStatus.AVAILABLE);
                seatsService.save(seat);
            }
        }
        model.addAttribute("seatsOfAircraftGo", seatsService.findByAircraft(flightGo.getAircraft()));
        model.addAttribute("seatType_go", seatType_go);
        session.setAttribute("bookingFlight", bookingFlight);

        return "select_seats";
    }

    @RequestMapping(value = "/orderer", method = RequestMethod.GET)
    public String getSeats(Model model) {

        try {
            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            String username = principal.toString();
            if (principal instanceof UserDetails) {
                username = ((UserDetails) principal).getUsername();
            }

            if (!username.equals("anonymousUser")) {
                model.addAttribute("user", userService.getUser(username));
            }
        } catch (Exception e) {
            return "redirect:/home";
        }
        return "orderer";
    }

    @RequestMapping(value = "/add_orderer", method = RequestMethod.POST)
    public String addOrderer(Model model, @ModelAttribute(name = "orderer") BookingEntity bookingEntity,
            HttpServletRequest request) {

        try {
            HttpSession session = request.getSession();

            BookingFlight bookingFlight = (BookingFlight) session.getAttribute("bookingFlight");

            if (bookingFlight == null) {
                bookingFlight = new BookingFlight();
            }
            bookingFlight.setBookingEntity(bookingEntity);

            session.setAttribute("bookingFlight", bookingFlight);
        } catch (Exception e) {
            return "redirect:/home";
        }
        return "redirect:/passenger";
    }

    @RequestMapping(value = "/passenger", method = RequestMethod.GET)
    public String passengerPage(Model model, HttpServletRequest request) {

        try {
            HttpSession session = request.getSession();

            BookingFlight bookingFlight = (BookingFlight) session.getAttribute("bookingFlight");

            if (bookingFlight == null) {
                bookingFlight = new BookingFlight();
            }

            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            String username = principal.toString();
            if (principal instanceof UserDetails) {
                username = ((UserDetails) principal).getUsername();
            }

            if (!username.equals("anonymousUser")) {
                UserEntity user = userService.getUser(username);
                if (user != null) {
                    if (bookingFlight.getBookingEntity() != null) {
                        bookingFlight.getBookingEntity().setUser(user);
                    }
                    model.addAttribute("user", user);
                }
            }
            session.setAttribute("bookingFlight", bookingFlight);
        } catch (Exception e) {
            return "redirect:/home";
        }
        return "passenger";
    }

    @RequestMapping(value = "/payment_by_credit_card", method = RequestMethod.GET)
    public String paymentPage(Model model, @RequestParam(name = "notEnought", required = false) boolean notEnought,
            @RequestParam(name = "error", required = false) boolean error,
            @RequestParam(name = "expired", required = false) boolean expired,
            HttpServletRequest request) {

        try {
            HttpSession session = request.getSession();

            BookingFlight bookingFlight = (BookingFlight) session.getAttribute("bookingFlight");

            if (bookingFlight == null) {
                bookingFlight = new BookingFlight();
            }

            if (error == true) {
                model.addAttribute("message", "The wrong card!!!");
            }

            if (notEnought == true) {
                model.addAttribute("message", "Not enough money!!!");
            }

            if (expired == true) {
                model.addAttribute("message", "The card has expired!!!");
            }

            List<PassengerEntity> passengers = bookingFlight.getPassengerEntities();
            List<BookingDetailEntity> bookingDetails = new ArrayList<>();

            FlightEntity flightGo = bookingFlight.getFlightGo();
            FlightEntity flightReturn = bookingFlight.getFlightReturn();

            Date now = new Date();
            LocalDate nowConverted = convertToLocalDateViaInstant(now);

            for (int i = 0; i < passengers.size(); i++) {
                BookingDetailEntity bookingDetail = new BookingDetailEntity();
                bookingDetail.setPassenger(passengers.get(i));

                Date birthDate = passengers.get(i).getBirthDate();
                LocalDate birthDateConverted = convertToLocalDateViaInstant(birthDate);
                int age = calculateAge(birthDateConverted, nowConverted);

                SeatsEntity seat;

                if (age >= 12) {
                    bookingDetail.setUnitPrice(bookingFlight.getUnit_price_of_adults_for_flightGo());

                    seat = seatsService.findSeatsById(bookingFlight.getSeatIdsGo()[i]);

                    bookingDetail.setSeat(seat);
                    seat.setSeatStatus(SeatStatus.BOOKED);

                } else if (age < 12 && age >= 2) {
                    bookingDetail.setUnitPrice(bookingFlight.getUnit_price_of_children_for_flightGo());

                    seat = seatsService.findSeatsById(bookingFlight.getSeatIdsGo()[i]);
                    bookingDetail.setSeat(seat);
                    seat.setSeatStatus(SeatStatus.BOOKED);

                } else {
                    bookingDetail.setUnitPrice(bookingFlight.getUnit_price_of_infants_for_flightGo());
                }
                bookingDetail.setStatus(BookingStatus.BOOKED);
                bookingDetail.setFlight(flightGo);

                bookingDetails.add(bookingDetail);

                if (flightReturn != null) {
                    bookingDetail = new BookingDetailEntity();
                    bookingDetail.setPassenger(passengers.get(i));

                    if (age >= 12) {
                        bookingDetail.setUnitPrice(bookingFlight.getUnit_price_of_adults_for_flightReturn());

                        seat = seatsService.findSeatsById(bookingFlight.getSeatIdsReturn()[i]);
                        bookingDetail.setSeat(seat);
                        seat.setSeatStatus(SeatStatus.BOOKED);

                    } else if (age < 12 && age >= 2) {
                        bookingDetail.setUnitPrice(bookingFlight.getUnit_price_of_children_for_flightReturn());

                        seat = seatsService.findSeatsById(bookingFlight.getSeatIdsReturn()[i]);
                        bookingDetail.setSeat(seat);
                        seat.setSeatStatus(SeatStatus.BOOKED);

                    } else {
                        bookingDetail.setUnitPrice(bookingFlight.getUnit_price_of_infants_for_flightReturn());
                    }
                    bookingDetail.setStatus(BookingStatus.BOOKED);
                    bookingDetail.setFlight(flightReturn);

                    bookingDetails.add(bookingDetail);
                }

            }
            bookingFlight.setBookingDetails(bookingDetails);

            model.addAttribute("totalPrice", bookingFlight.getTotalPrice());
            model.addAttribute("flightGo", flightGo);
            model.addAttribute("flightReturn", flightReturn);
            model.addAttribute("booking_details", bookingDetails);
            session.setAttribute("bookingFlight", bookingFlight);
        } catch (Exception e) {
            return "redirect:/home";
        }

        return "payment_by_credit_card";
    }

    @Transactional(rollbackFor = Exception.class)
    @RequestMapping(value = "/confirm", method = RequestMethod.POST)
    public String confirmAndSaveData(Model model, @ModelAttribute(name = "creditCard") CreditCardEntity creditCardEntity,
            HttpServletRequest request) throws Exception {

        try {
            HttpSession session = request.getSession();

            BookingFlight bookingFlight = (BookingFlight) session.getAttribute("bookingFlight");

            if (bookingFlight != null) {

                boolean expired = creditCardEntity.getCardExDate().before(new Date());

                if (expired == false) {

                    BookingEntity booking = bookingFlight.getBookingEntity();

                    CreditCardEntity creditCard = creditCardService.check(creditCardEntity.getCode(), creditCardEntity.getCardExDate(), creditCardEntity.getCardNumber(), creditCardEntity.getCardName());
                    double amount;
                    if (creditCard != null) {
                        amount = bookingFlight.getTotalPrice() * (100 - booking.getDiscount()) / 100;
                        double balance = creditCard.getBalance();

                        if (balance >= amount) {
                            creditCard.setBalance(balance - amount);
                            creditCardService.save(creditCard);
                        } else {
                            return "redirect:/payment_by_credit_card?notEnought=true";
                        }

                    } else {
                        return "redirect:/payment_by_credit_card?error=true";
                    }

                    List<PassengerEntity> passengers = bookingFlight.getPassengerEntities();
                    List<BookingDetailEntity> bookingDetails = bookingFlight.getBookingDetails();
                    
                    passengerService.save(passengers);

                    UUID uuid = UUID.randomUUID();

                    booking.setBookingCode(uuid.toString());
                    booking.setBookingDetailEntities(bookingDetails);
                    booking.setTotalPrice(bookingFlight.getTotalPrice());
                    booking.setBookingDate(new Date());
                    booking.setStatus(BookingStatus.BOOKED);

                    bookingService.save(booking);

                    for (BookingDetailEntity bookingDetail : bookingDetails) {
                        bookingDetail.setBooking(booking);
                        bookingDetailService.save(bookingDetail);
                    }

                    PaymentEntity payment = new PaymentEntity();

                    payment.setBooking(booking);
                    payment.setCreditCard(creditCard);
                    payment.setMethod(PaymentMethod.CREDIT_CARD);
                    payment.setPaymentDate(new Date());
                    payment.setAmount(amount);
                    payment.setStatus(PaymentStatus.PAYMENT_ORDER);

                    paymentService.save(payment);

                    sendEmail(booking, bookingDetails, amount, bookingFlight);

                    session.removeAttribute("bookingFlight");

                    return "redirect:/thankyou";

                } else {
                    return "redirect:/payment_by_credit_card?expired=true";
                }
            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return "redirect:/home";
        }
        return "redirect:/home";
    }

    public void sendEmail(BookingEntity booking, List<BookingDetailEntity> bookingDetails,
            double amount, BookingFlight bookingFlight) throws Exception {

        NumberFormat formatter2 = new DecimalFormat();

        List<String> trs1 = new ArrayList<>();
        List<String> trs2 = new ArrayList<>();

        FlightEntity flightGo = bookingFlight.getFlightGo();
        LocalTime departureTimeOfFlightGo = LocalTime.of(flightGo.getDepartureTime().getHour() - 1, flightGo.getDepartureTime().getMinute());
        LocalDateTime departureDateTimeOfFlightGo = LocalDateTime.of(flightGo.getDepartureDate(), departureTimeOfFlightGo);

        FlightEntity flightReturn = bookingFlight.getFlightReturn();
        LocalDateTime departureDateTimeOfFlightReturn = null;
        if (flightReturn != null) {
            LocalTime departureTimeOfFlightReturn = LocalTime.of(flightReturn.getDepartureTime().getHour() - 1, flightReturn.getDepartureTime().getMinute());
            departureDateTimeOfFlightReturn = LocalDateTime.of(flightReturn.getDepartureDate(), departureTimeOfFlightReturn);
        }

        for (BookingDetailEntity bookingDetail : bookingDetails) {

            SimpleDateFormat formatter1 = new SimpleDateFormat("dd/MM/yyyy");
            String birthDateStr = formatter1.format(bookingDetail.getPassenger().getBirthDate());

            LocalTime departureTime = LocalTime.of(bookingDetail.getFlight().getDepartureTime().getHour() - 1, bookingDetail.getFlight().getDepartureTime().getMinute());
            LocalDateTime departureDateTime = LocalDateTime.of(bookingDetail.getFlight().getDepartureDate(), departureTime);
            DateTimeFormatter dtf
                    = DateTimeFormatter.ofLocalizedDateTime(
                            FormatStyle.MEDIUM);
            String departureDateTimeFormatted
                    = dtf.format(departureDateTime);

            if (departureDateTime.isEqual(departureDateTimeOfFlightGo)) {
                String tr1 = "                <tr>\n"
                        + "                    <td>" + bookingDetail.getPassenger().getFullName() + "</td><!-- comment -->\n"
                        + "                    <td>" + bookingDetail.getPassenger().getAddress() + "</td>\n"
                        + "                    <td>" + birthDateStr + "</td>\n"
                        + "                    <td>" + bookingDetail.getPassenger().getPhone() + "</td>\n"
                        + "                    <td>" + bookingDetail.getPassenger().getGender() + "</td>\n"
                        + "                    <td>" + bookingDetail.getPassenger().getIdCard() + "</td>           \n"
                        + "                    <td>" + departureDateTimeFormatted + "</td>\n"
                        + "                    <td>" + bookingDetail.getSeat().getCode() + "</td>\n"
                        + "                    <td>" + bookingDetail.getFlight().getAircraft().getName() + " - " + bookingDetail.getFlight().getAircraft().getAircraftNumber() + "</td>\n"
                        + "                    <td>" + formatter2.format(bookingDetail.getUnitPrice()) + " $</td>\n"
                        + "                </tr>            \n";
                trs1.add(tr1);
            }

            if (departureDateTimeOfFlightReturn != null) {
                if (departureDateTime.isEqual(departureDateTimeOfFlightReturn)) {
                    String tr2 = "                <tr>\n"
                            + "                    <td>" + bookingDetail.getPassenger().getFullName() + "</td><!-- comment -->\n"
                            + "                    <td>" + bookingDetail.getPassenger().getAddress() + "</td>\n"
                            + "                    <td>" + birthDateStr + "</td>\n"
                            + "                    <td>" + bookingDetail.getPassenger().getPhone() + "</td>\n"
                            + "                    <td>" + bookingDetail.getPassenger().getGender() + "</td>\n"
                            + "                    <td>" + bookingDetail.getPassenger().getIdCard() + "</td>           \n"
                            + "                    <td>" + departureDateTimeFormatted + "</td>\n"
                            + "                    <td>" + bookingDetail.getSeat().getCode() + "</td>\n"
                            + "                    <td>" + bookingDetail.getFlight().getAircraft().getName() + " - " + bookingDetail.getFlight().getAircraft().getAircraftNumber() + "</td>\n"
                            + "                    <td>" + formatter2.format(bookingDetail.getUnitPrice()) + " $</td>\n"
                            + "                </tr>            \n";
                    trs2.add(tr2);
                }
            }

        }
        String trs1Replaced = trs1.toString().replace("[", "");
        trs1Replaced = trs1Replaced.replace("]", "");
        trs1Replaced = trs1Replaced.replace(",", "");

        String tableOfFlightReturn = "";
        if (flightReturn != null) {
            String trs2Replaced = trs2.toString().replace("[", "");
            trs2Replaced = trs2Replaced.replace("]", "");
            trs2Replaced = trs2Replaced.replace(",", "");

            if (trs2Replaced != null) {
                tableOfFlightReturn
                        = "          <table style=\"text-align: center; margin: auto; border: 1px solid; border-collapse: collapse; margin-top: 20px;\">\n"
                        + "            <thead>\n"
                        + "                <tr>\n"
                        + "                    <th colspan=\"10\" style=\"text-align: center; color: #0d89d2\">" + flightGo.getFlightRoute().getDestination().getCityOfAirportEntity().getName() + " - " + flightGo.getFlightRoute().getDeparture().getCityOfAirportEntity().getName() + "</th>\n"
                        + "                </tr>"
                        + "                <tr>\n"
                        + "                    <th>Full name</th>\n"
                        + "                    <th>Address</th><!-- comment -->\n"
                        + "                    <th>Birth date</th>\n"
                        + "                    <th>Phone</th><!-- comment -->\n"
                        + "                    <th>Gender</th>\n"
                        + "                    <th>Id Card</th>\n"
                        + "                    <th>Take-off time</th>\n"
                        + "                    <th>Seat code</th>\n"
                        + "                    <th>Aircraft</th>\n"
                        + "                    <th>Unit price</th>\n"
                        + "                </tr>\n"
                        + "            </thead>\n"
                        + "            <tbody>\n"
                        + trs2Replaced
                        + "            </tbody>\n"
                        + "        </table>\n";
            }
        }

        bookingService.sendEmail(booking.getEmail(), "Hello " + booking.getFullName(),
                "<!DOCTYPE html>\n"
                + "<html>\n"
                + "    <head>\n"
                + "        <title>TODO supply a title</title>\n"
                + "        <meta charset=\"UTF-8\">\n"
                + "        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n"
                + "        <style>\n"
                + "             td {"
                + "                 border: 1px solid;"
                + "                 }\n"
                + "             th {"
                + "                 border: 1px solid;"
                + "                 }\n"
                + "        </style>\n"
                + "    </head>\n"
                + "    <body>\n"
                + "        <div style=\"text-align: center\">\n"
                + "            <h3>Passenger information</h3>\n"
                + "        </div>\n"
                + "        <h5>Here is your booking code: " + booking.getBookingCode() + "</h5>"
                + "        <p>click this link to check booking: <a href='http://localhost:8080/FlightBooking/info_booking'>http://localhost:8080/FlightBooking/info_booking</a></p>"
                + "        <table style=\"text-align: center; margin: auto; border: 1px solid; border-collapse: collapse; margin-top: 20px;\">\n"
                + "            <thead>\n"
                + "                <tr>\n"
                + "                    <th colspan=\"10\" style=\"text-align: center; color: #0d89d2\">" + flightGo.getFlightRoute().getDeparture().getCityOfAirportEntity().getName() + " - " + flightGo.getFlightRoute().getDestination().getCityOfAirportEntity().getName() + "</th>\n"
                + "                </tr>"
                + "                <tr>\n"
                + "                    <th>Full name</th>\n"
                + "                    <th>Address</th><!-- comment -->\n"
                + "                    <th>Birth date</th>\n"
                + "                    <th>Phone</th><!-- comment -->\n"
                + "                    <th>Gender</th>\n"
                + "                    <th>Id Card</th>\n"
                + "                    <th>Take-off time</th>\n"
                + "                    <th>Seat code</th>\n"
                + "                    <th>Aircraft</th>\n"
                + "                    <th>Unit price</th>\n"
                + "                </tr>\n"
                + "            </thead>\n"
                + "            <tbody>\n"
                + trs1Replaced
                + "            </tbody>\n"
                + "         </table>\n"
                + tableOfFlightReturn
                + "        <strong style=\"color: red\">The amount paid: " + formatter2.format(amount) + " $</strong>\n"
                + "    </body>\n"
                + "</html>");
    }

    public LocalDate convertToLocalDateViaInstant(Date dateToConvert) {
        return dateToConvert.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
    }

    public int calculateAge(LocalDate birthDate, LocalDate currentDate) {
        return currentDate.getYear() - birthDate.getYear();
    }

    @RequestMapping(value = "/thankyou")
    public String thankyouPage(Model model) {
        return "thankyou";
    }
}
