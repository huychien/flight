/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flight_booking.api;

import com.flight_booking.entities.FlightEntity;
import com.flight_booking.entities.FlightRouteEntity;
import com.flight_booking.entities.PassengerEntity;
import com.flight_booking.entities.SeatsEntity;
import com.flight_booking.enums.Gender;
import com.flight_booking.enums.SeatStatus;
import com.flight_booking.model.BookingFlight;
import com.flight_booking.service.FlightRouteService;
import com.flight_booking.service.FlightService;
import com.flight_booking.service.SeatsService;
import com.flight_booking.utils.CityTemp;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author huythang
 */
@RestController
@RequestMapping("/api")
public class RestAPI {

    @Autowired
    private FlightRouteService flightRouteService;

    @Autowired
    private SeatsService seatsService;

    @Autowired
    private FlightService flightService;

    @RequestMapping(value = "/search_cities", method = RequestMethod.GET)
    public Object search_cities(@RequestParam(name = "departure_id") Integer departure_id) {

        List<FlightRouteEntity> flightRoutes = flightRouteService.getDestinationByDeparture(departure_id);
        List<CityTemp> cityTemps = new ArrayList<>();

        for (FlightRouteEntity flightRoute : flightRoutes) {
            CityTemp cityTemp = new CityTemp();
            cityTemp.setId(flightRoute.getDestination().getCityOfAirportEntity().getId());
            cityTemp.setName(flightRoute.getDestination().getCityOfAirportEntity().getName());

            cityTemps.add(cityTemp);
        }
        for (int i = 0; i < cityTemps.size(); i++) {
            for (int j = i + 1; j < cityTemps.size(); j++) {
                if (cityTemps.get(i).getId().equals(cityTemps.get(j).getId())) {
                    cityTemps.remove(j);
                }
            }
        }
        return cityTemps;
    }

    @RequestMapping(value = "/departure-date", method = RequestMethod.GET)
    public Object getDepartureDate(@RequestParam(name = "departureDate") String departureDateStr) {

        LocalDate departureDate = LocalDate.parse(departureDateStr);
        return departureDate;
    }

    @RequestMapping(value = "/compareTwoFlight", method = RequestMethod.GET)
    public Object compareTwoFlight(@RequestParam(name = "flight_go_id") int flight_go_id,
            @RequestParam(name = "flight_return_id") int flight_return_id) {

        FlightEntity flightGo = flightService.findById(flight_go_id);

        if (flight_return_id != 0) {
            FlightEntity flightReturn = flightService.findById(flight_return_id);

            LocalDateTime takeOffDateTimeOfFlightGo = LocalDateTime.of(flightGo.getDepartureDate(), flightGo.getDepartureTime());
            LocalDateTime takeOffDateTimeOfFlightReturn = LocalDateTime.of(flightReturn.getDepartureDate(), flightReturn.getDepartureTime());

            if (takeOffDateTimeOfFlightGo.isAfter(takeOffDateTimeOfFlightReturn)) {
                return false;
            }
        }
        return true;
    }

    @RequestMapping(value = "/lockSeats", method = RequestMethod.GET)
    public Object lockSeats(@RequestParam(name = "seat_go_ids[]") Integer[] seat_go_ids,
            @RequestParam(name = "seat_return_ids[]", required = false) Integer[] seat_return_ids, HttpServletRequest request) {

        HttpSession session = request.getSession();

        BookingFlight bookingFlight = (BookingFlight) session.getAttribute("bookingFlight");

        if (bookingFlight == null) {
            bookingFlight = new BookingFlight();
        }

        if (seat_go_ids != null) {
            bookingFlight.setSeatIdsGo(seat_go_ids);
            
            for (int i = 0; i < seat_go_ids.length; i++) {
                SeatsEntity seat = seatsService.findSeatsById(seat_go_ids[i]);

                if (seat.getSeatStatus().equals(SeatStatus.BOOKED)) {
                    bookingFlight.setSeatIdsGo(null);
                    return false;
                }
                seat.setSeatStatus(SeatStatus.BOOKED);
                seatsService.save(seat);
            }
        } else {
            return false;
        }
        
        if (seat_return_ids != null) {
            bookingFlight.setSeatIdsReturn(seat_return_ids);
            
            for (int i = 0; i < seat_return_ids.length; i++) {
                SeatsEntity seat = seatsService.findSeatsById(seat_return_ids[i]);

                if (seat.getSeatStatus().equals(SeatStatus.BOOKED)) {
                    bookingFlight.setSeatIdsReturn(null);
                    return false;
                }
                seat.setSeatStatus(SeatStatus.BOOKED);
                seatsService.save(seat);
            }
        }
        session.setAttribute("bookingFlight", bookingFlight);
        return true;
    }

    @RequestMapping(value = "/cancel_booking", method = RequestMethod.GET)
    public Object cancelBooking(HttpServletRequest request) {

        HttpSession session = request.getSession();

        BookingFlight bookingFlight = (BookingFlight) session.getAttribute("bookingFlight");

        if (bookingFlight == null) {
            bookingFlight = new BookingFlight();
        }

        if (bookingFlight.getSeatIdsGo() != null) {
            for (int i = 0; i < bookingFlight.getSeatIdsGo().length; i++) {
                SeatsEntity seat = seatsService.findSeatsById(bookingFlight.getSeatIdsGo()[i]);
                seat.setSeatStatus(SeatStatus.AVAILABLE);
                seatsService.save(seat);
            }
        }
        if (bookingFlight.getSeatIdsReturn() != null) {
            for (int i = 0; i < bookingFlight.getSeatIdsReturn().length; i++) {
                SeatsEntity seat = seatsService.findSeatsById(bookingFlight.getSeatIdsReturn()[i]);
                seat.setSeatStatus(SeatStatus.AVAILABLE);
                seatsService.save(seat);
            }
        }

        session.removeAttribute("bookingFlight");
        return "";
    }

    @RequestMapping(value = "/initPassenger", method = RequestMethod.GET)
    public Object initPassenger(HttpServletRequest request) {

        HttpSession session = request.getSession();

        BookingFlight bookingFlight = (BookingFlight) session.getAttribute("bookingFlight");

        if (bookingFlight == null) {
            bookingFlight = new BookingFlight();
        }
        bookingFlight.setPassengerEntities(new ArrayList<>());
        session.setAttribute("bookingFlight", bookingFlight);
        return "";
    }

    @RequestMapping(value = "/info_adults", method = RequestMethod.GET)
    public Object takeInfoAdults(@RequestParam(name = "fullName") String fullName, @RequestParam(name = "phone") String phone,
            @RequestParam(name = "address") String address, @RequestParam(name = "birthDate") Date birthDate,
            @RequestParam(name = "gender") String gender, @RequestParam(name = "idCard") String idCard, HttpServletRequest request) throws ParseException {

        HttpSession session = request.getSession();

        BookingFlight bookingFlight = (BookingFlight) session.getAttribute("bookingFlight");

        if (bookingFlight == null) {
            bookingFlight = new BookingFlight();
        }
        
        PassengerEntity passengerEntity = new PassengerEntity();
        Gender g = Gender.valueOf(gender);

        passengerEntity.setFullName(fullName);
        passengerEntity.setPhone(phone);
        passengerEntity.setAddress(address);
        passengerEntity.setBirthDate(birthDate);
        passengerEntity.setGender(g);
        passengerEntity.setIdCard(idCard);

        bookingFlight.addPassenger(passengerEntity);
        session.setAttribute("bookingFlight", bookingFlight);
        return "";
    }

    @RequestMapping(value = "/info_children", method = RequestMethod.GET)
    public Object takeInfoChildren(@RequestParam(name = "fullName") String fullName,
            @RequestParam(name = "address") String address, @RequestParam(name = "birthDate") Date birthDate,
            @RequestParam(name = "gender") String gender, HttpServletRequest request) throws ParseException {

        HttpSession session = request.getSession();

        BookingFlight bookingFlight = (BookingFlight) session.getAttribute("bookingFlight");

        if (bookingFlight == null) {
            bookingFlight = new BookingFlight();
        }

        PassengerEntity passengerEntity = new PassengerEntity();
        Gender g = Gender.valueOf(gender);

        passengerEntity.setFullName(fullName);
        passengerEntity.setAddress(address);
        passengerEntity.setBirthDate(birthDate);
        passengerEntity.setGender(g);

        bookingFlight.addPassenger(passengerEntity);
        session.setAttribute("bookingFlight", bookingFlight);
        return "";
    }

    @RequestMapping(value = "/info_infants", method = RequestMethod.GET)
    public Object takeInfoInfants(@RequestParam(name = "fullName") String fullName,
            @RequestParam(name = "address") String address, @RequestParam(name = "birthDate") Date birthDate,
            @RequestParam(name = "gender") String gender, HttpServletRequest request) throws ParseException {

        HttpSession session = request.getSession();

        BookingFlight bookingFlight = (BookingFlight) session.getAttribute("bookingFlight");

        if (bookingFlight == null) {
            bookingFlight = new BookingFlight();
        }

        PassengerEntity passengerEntity = new PassengerEntity();
        Gender g = Gender.valueOf(gender);

        passengerEntity.setFullName(fullName);
        passengerEntity.setAddress(address);
        passengerEntity.setBirthDate(birthDate);
        passengerEntity.setGender(g);

        bookingFlight.addPassenger(passengerEntity);
        session.setAttribute("bookingFlight", bookingFlight);
        return "";
    }

    @RequestMapping(value = "/total_and_unit_price", method = RequestMethod.GET)
    public Object total(@RequestParam(name = "total") double total,
            @RequestParam(name = "unit_price_of_adults_for_flightGo") double unit_price_of_adults_for_flightGo,
            @RequestParam(name = "unit_price_of_children_for_flightGo") double unit_price_of_children_for_flightGo,
            @RequestParam(name = "unit_price_of_infants_for_flightGo") double unit_price_of_infants_for_flightGo,
            @RequestParam(name = "unit_price_of_adults_for_flightReturn", required = false) double unit_price_of_adults_for_flightReturn,
            @RequestParam(name = "unit_price_of_children_for_flightReturn", required = false) double unit_price_of_children_for_flightReturn,
            @RequestParam(name = "unit_price_of_infants_for_flightReturn", required = false) double unit_price_of_infants_for_flightReturn,
            HttpServletRequest request) {

        HttpSession session = request.getSession();

        BookingFlight bookingFlight = (BookingFlight) session.getAttribute("bookingFlight");

        if (bookingFlight == null) {
            bookingFlight = new BookingFlight();
        }

        bookingFlight.setTotalPrice(total);

        bookingFlight.setUnit_price_of_adults_for_flightGo(unit_price_of_adults_for_flightGo);
        bookingFlight.setUnit_price_of_children_for_flightGo(unit_price_of_children_for_flightGo);
        bookingFlight.setUnit_price_of_infants_for_flightGo(unit_price_of_infants_for_flightGo);

        bookingFlight.setUnit_price_of_adults_for_flightReturn(unit_price_of_adults_for_flightReturn);
        bookingFlight.setUnit_price_of_children_for_flightReturn(unit_price_of_children_for_flightReturn);
        bookingFlight.setUnit_price_of_infants_for_flightReturn(unit_price_of_infants_for_flightReturn);
        
        session.setAttribute("bookingFlight", bookingFlight);
        return "";
    }

    @RequestMapping(value = "/search_available_seats_num", method = RequestMethod.GET)
    public Object search_available_seats_num(@RequestParam(name = "aircraft_ids[]") Integer[] aircraft_ids) {

        List<Object> list = new ArrayList<>();

        for (Integer aircraft_id : aircraft_ids) {
            list.add(seatsService.getAvailableEconomySeatNum(aircraft_id));
            list.add(seatsService.getAvailableBusinessSeatNum(aircraft_id));
        }
        return list;
    }

    @RequestMapping(value = "/clearSessionScop", method = RequestMethod.GET)
    public Object clearSessionScop(HttpServletRequest request) {

        HttpSession session = request.getSession();

        session.removeAttribute("bookingFlight");
        return "";
    }

}
