/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flight_booking.entities;

import com.flight_booking.enums.CommonStatus;
import java.io.Serializable;
import java.time.LocalTime;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author huycong
 */
@Entity
@Table(name = "flight_route")
public class FlightRouteEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private double price;
    private double distance;

    private LocalTime duration;

    @Enumerated(EnumType.STRING)
    private CommonStatus status;

    @ManyToOne
    @JoinColumn(name = "departure_id")
    private AirportEntity departure;

    @ManyToOne
    @JoinColumn(name = "destination_id")
    private AirportEntity destination;

    @OneToMany(mappedBy = "flightRoute", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<FlightEntity> flightEntities;

    public FlightRouteEntity() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public List<FlightEntity> getFlightEntities() {
        return flightEntities;
    }

    public void setFlightEntities(List<FlightEntity> flightEntities) {
        this.flightEntities = flightEntities;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public LocalTime getDuration() {
        return duration;
    }

    public void setDuration(LocalTime duration) {
        this.duration = duration;
    }

    public CommonStatus getStatus() {
        return status;
    }

    public void setStatus(CommonStatus status) {
        this.status = status;
    }

    public AirportEntity getDeparture() {
        return departure;
    }

    public void setDeparture(AirportEntity departure) {
        this.departure = departure;
    }

    public AirportEntity getDestination() {
        return destination;
    }

    public void setDestination(AirportEntity destination) {
        this.destination = destination;
    }

}
