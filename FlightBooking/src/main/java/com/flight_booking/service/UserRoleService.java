/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flight_booking.service;

import com.flight_booking.entities.UserRoleEntity;
import com.flight_booking.enums.Role;
import com.flight_booking.repository.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Set;

/**
 *
 * @author huythang
 */
@Service
public class UserRoleService {
    
    @Autowired
    private UserRoleRepository repository;
    
    public Set<UserRoleEntity> setRoleUser() {
        return repository.findByRoles(Role.ROLE_USER);
    }
}
