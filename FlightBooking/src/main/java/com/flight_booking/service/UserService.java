/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flight_booking.service;

import com.flight_booking.entities.UserEntity;
import com.flight_booking.repository.UserRepository;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

/**
 *
 * @author huythang
 */
@Service
public class UserService {
    
    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private JavaMailSender emailSender;

    public void sendEmail(
            String to, String subject, String text) throws MessagingException {

        MimeMessage mimeMessage = emailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, "utf-8");

        helper.setFrom("huychienvv@gmail.com");
        helper.setTo(to);
        helper.setSubject(subject);
        helper.setText(text, true);
        emailSender.send(mimeMessage);

    }
    
    public UserEntity getUser(String email) {
        return userRepository.findByEmailLike(email);
    }
    
    public UserEntity findById(Integer id) {
        return userRepository.findById(id).get();
    }
    
    public void save(UserEntity user) {
        userRepository.save(user);
    }
}
