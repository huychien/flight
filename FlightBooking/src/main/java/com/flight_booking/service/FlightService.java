/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flight_booking.service;

import com.flight_booking.entities.FlightEntity;
import com.flight_booking.entities.SeatsEntity;
import com.flight_booking.enums.FlightStatus;
import com.flight_booking.enums.SeatStatus;
import com.flight_booking.repository.FlightRepository;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 *
 * @author huythang
 */
@Service
public class FlightService {

    @Autowired
    private FlightRepository flightRepository;
    
    @Autowired
    private SeatsService seatsService;

    public List<FlightEntity> findByDepartureDateAndPlace(LocalDate departureDate, int departureId, int destinationId) {
        return flightRepository.findByDepartureDateAndPlace(departureDate, departureId, destinationId);
    }

    public FlightEntity findById(int id) {
        return flightRepository.findById(id).get();
    }

    public List<FlightEntity> getFlights() {
        return flightRepository.findAll();
    }

    public void save(FlightEntity flightEntity, String action) {
        flightRepository.save(flightEntity);
    }

    public FlightEntity findFlightById(int id) {
        Optional<FlightEntity> f = flightRepository.findById(id);
        if (f.isPresent()) {
            return f.get();
        } else {
            return new FlightEntity();
        }
    }

    public boolean deleteFlight(int id) {
        flightRepository.deleteById(id);
        return flightRepository.existsById(id);
    }

    public List<FlightEntity> findByDepartureDate(LocalDate departureDate) {

        return flightRepository.findByDepartureDate(departureDate);
    }
    
    @Scheduled(fixedDelay = 10000)
    public void autoSetStatus() {
        
        List<FlightEntity> allFlights = flightRepository.findAll();
        LocalDateTime now = LocalDateTime.now();
        
        for (FlightEntity flight : allFlights) {
            
            int hourGo = flight.getDepartureTime().getHour() - 1;
            int minuteGo = flight.getDepartureTime().getMinute(); 
            
            if (hourGo == -1) {
                hourGo = 23;
            }        
            LocalTime timeGo = LocalTime.of(hourGo, minuteGo);
            
            LocalDateTime dateAndTimeToGo = LocalDateTime.of(flight.getDepartureDate(), timeGo);
            
            Duration duration = Duration.between(now, dateAndTimeToGo);
            long hoursLeftToGo = duration.toHours();
            long minutesLeftToGo = duration.toMinutes();
            
            if (hoursLeftToGo == 0 && minutesLeftToGo == 0) {
                flight.setStatus(FlightStatus.TAKE_OFF);
            }
            
            LocalDateTime dateAndTimeToArrive = flight.getArriveTime();
            
            duration = Duration.between(now, dateAndTimeToArrive);
            long hoursLeftToArrive = duration.toHours();
            long minutesLeftToArrive = duration.toMinutes();
            
            if (hoursLeftToArrive == 0 && minutesLeftToArrive == 0) {
                flight.setStatus(FlightStatus.LANDING);
                
                List<SeatsEntity> seats = seatsService.findByAircraftId(flight.getAircraft().getId());
                for (SeatsEntity seat : seats) {
                    seat.setSeatStatus(SeatStatus.AVAILABLE);
                    seatsService.save(seat);
                }            
            }
            flightRepository.save(flight);
        }
        
    }
}
