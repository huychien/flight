/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flight_booking.service;

import com.flight_booking.entities.AircraftEntity;
import com.flight_booking.entities.SeatsEntity;
import com.flight_booking.repository.SeatsRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author huythang
 */
@Service
public class SeatsService {

    @Autowired
    private SeatsRepository seatsRepository;

    public List<SeatsEntity> getSeats() {
        return seatsRepository.findAll();
    }

    public SeatsEntity findSeatsById(Integer id) {
        Optional<SeatsEntity> f = seatsRepository.findById(id);
        if (f.isPresent()) {
            return f.get();
        } else {
            return new SeatsEntity();
        }
    }

    public SeatsEntity findCodeAndAircraft(String code, AircraftEntity aircraft) {
        return seatsRepository.findByCodeAndAircraft(code, aircraft);
    }

//    public SeatsEntity findCodeContaining(String code){
//        return seatsRepository.findCodeContaining(code);
//    } 
    public boolean deleteSeats(int id) {
        seatsRepository.deleteById(id);
        return seatsRepository.existsById(id);
    }

    public void save(SeatsEntity seatsEntity) {
        seatsRepository.save(seatsEntity);
    }
    
    public void save(SeatsEntity seatsEntity, String action) {
        seatsRepository.save(seatsEntity);

    }

    public List<SeatsEntity> findByAircraft(AircraftEntity aircraftEntity) {
        return seatsRepository.findByAircraft(aircraftEntity);
    }
    
    public List<SeatsEntity> findByAircraftId(int id){
        return seatsRepository.findByAircraftId(id);
    }
    
    public int getAvailableEconomySeatNum(Integer id){
        return seatsRepository.getAvailableEconomySeatNum(id);
    }
    
     public int getAvailableBusinessSeatNum(Integer id){
        return seatsRepository.getAvailableBusinessSeatNum(id);
    }

}
