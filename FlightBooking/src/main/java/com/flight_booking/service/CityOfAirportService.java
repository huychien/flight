/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flight_booking.service;

import com.flight_booking.entities.CityOfAirportEntity;
import com.flight_booking.repository.CityOfAirportRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author huycong
 */
@Service
public class CityOfAirportService {

    @Autowired
    private CityOfAirportRepository cityOfAirportRepository;

    public List<CityOfAirportEntity> getCityOfAirports() {
        return cityOfAirportRepository.findAll();
    }

    public CityOfAirportEntity findById(int id) {
        return cityOfAirportRepository.findById(id).get();
    }
    
    public void save(CityOfAirportEntity airportEntity) {
        cityOfAirportRepository.save(airportEntity);
    }
}
