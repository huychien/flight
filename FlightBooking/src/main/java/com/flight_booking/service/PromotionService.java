/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flight_booking.service;

import com.flight_booking.entities.PromotionEntity;
import com.flight_booking.repository.PromotionRepository;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author huycong
 */
@Service
public class PromotionService {
    
    @Autowired
    private PromotionRepository promotionRepository;
    
    public List<PromotionEntity> getPromotions() {
        return (List<PromotionEntity>) promotionRepository.findAll();
    }

    public void save(PromotionEntity promotionEntity, String action) {
        promotionRepository.save(promotionEntity);
    }

   
    
    
    public PromotionEntity findPromotionById(int id) {
        Optional<PromotionEntity> f = promotionRepository.findById(id);
        if (f.isPresent()) {
            return f.get();
        } else {
            return new PromotionEntity();
        }
    }

    public boolean deletePromotion(int id) {
        promotionRepository.deleteById(id);
        return promotionRepository.existsById(id);
    }
    
    public List<PromotionEntity> findByStartDate(LocalDate startDate) {

        return promotionRepository.findByStartDate(startDate);
    }

   

    
    
}
