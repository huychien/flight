/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flight_booking.service;

import com.flight_booking.entities.CreditCardEntity;
import com.flight_booking.repository.CreditCardRepository;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author huythang
 */
@Service
public class CreditCardService {
    
    @Autowired
    private CreditCardRepository creditCardRepository;
    
    public CreditCardEntity check(String code, Date cardExDate, String cardNumber, String cardName) {
        return creditCardRepository.findByCodeAndCardExDateAndCardNumberAndCardName(code, cardExDate, cardNumber, cardName);
    }

    public void save(CreditCardEntity creditCardEntity) {
        creditCardRepository.save(creditCardEntity);
    }

    public CreditCardEntity findById(int id) {
        return creditCardRepository.findById(id).get();
    }
}
