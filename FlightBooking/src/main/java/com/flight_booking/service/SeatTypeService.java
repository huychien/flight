/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flight_booking.service;

import com.flight_booking.entities.SeatTypeEntity;
import com.flight_booking.repository.SeatTypeRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author huythang
 */
@Service
public class SeatTypeService {
    
    @Autowired
    private SeatTypeRepository seatTypeRepository;
    
    public List<SeatTypeEntity> getType() {
        return seatTypeRepository.findAll();
    }
    
    public SeatTypeEntity findById(int id) {
        return seatTypeRepository.findById(id).get();
    }
    
    public void save(SeatTypeEntity seatTypeEntity) {
        seatTypeRepository.save(seatTypeEntity);
    }
}
