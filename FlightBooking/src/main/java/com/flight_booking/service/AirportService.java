/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flight_booking.service;

import com.flight_booking.entities.AirportEntity;
import com.flight_booking.repository.AirportRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author huycong
 */
@Service
public class AirportService {

    @Autowired
    private AirportRepository airportRepository;

    public List<AirportEntity> getAirports() {
        return (List<AirportEntity>) airportRepository.findAll();
    }

    public void save(AirportEntity airportEntity, String action) {
        airportRepository.save(airportEntity);
    }

    public AirportEntity findAirportById(int id) {
        Optional<AirportEntity> o = airportRepository.findById(id);
        if (o.isPresent()) {
            return o.get();
        } else {
            return new AirportEntity();
        }
    }

    public List<AirportEntity> findByCityNameOrAirportName(String strSearch) {

        return airportRepository.findByCityNameOrAirportName(strSearch, strSearch);
    }

}
