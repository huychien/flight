/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flight_booking.service;

import com.flight_booking.entities.BookingEntity;
import com.flight_booking.entities.UserEntity;
import com.flight_booking.repository.BookingRepository;
import java.util.List;
import java.util.Optional;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

/**
 *
 * @author huycong
 */
@Service
public class BookingService {

    @Autowired
    private BookingRepository bookingRepository;

    @Autowired
    private JavaMailSender emailSender;

    public void sendEmail(
            String to, String subject, String text) throws MessagingException {

        MimeMessage mimeMessage = emailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, "utf-8");

        helper.setFrom("huychienvv@gmail.com");
        helper.setTo(to);
        helper.setSubject(subject);
        helper.setText(text, true);
        emailSender.send(mimeMessage);

    }

    public List<BookingEntity> getBookinges() {
        return bookingRepository.findAll();
    }

    public List<BookingEntity> findByUser(UserEntity user) {
        return bookingRepository.findByUser(user);
    }

    public BookingEntity findById(Integer id) {
        return bookingRepository.findById(id).get();
    }

    public List<BookingEntity> findByBookingCode(String code) {
        return bookingRepository.findByBookingCode(code);
    }

    public void save(BookingEntity booking) {
        bookingRepository.save(booking);
    }
}
