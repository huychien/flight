/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flight_booking.service;

import com.flight_booking.entities.ImagesEntity;
import com.flight_booking.repository.ImagesRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author huycong
 */
@Service
public class ImageService {
    @Autowired
    private ImagesRepository imageRepository;
    
    public List<ImagesEntity> getImages() {
        return (List<ImagesEntity>) imageRepository.findAll();
    }
    
    public void save(ImagesEntity imageEntity) {
        imageRepository.save(imageEntity);
    }
}

