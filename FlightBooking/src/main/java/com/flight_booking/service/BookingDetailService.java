/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flight_booking.service;

import com.flight_booking.entities.BookingDetailEntity;
import com.flight_booking.repository.BookingDetailRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author huycong
 */
@Service
public class BookingDetailService {

    @Autowired
    private BookingDetailRepository bookingDetailRepository;

    public List<BookingDetailEntity> getBookingDetails() {
        return (List<BookingDetailEntity>) bookingDetailRepository.findAll();
    }

    public List<BookingDetailEntity> findByBookingId(int id) {
        return bookingDetailRepository.findByBookingId(id);
    }

    public BookingDetailEntity findById(Integer id) {
        return bookingDetailRepository.findById(id).get();
    }

    public void save(BookingDetailEntity bookingDetail) {
        bookingDetailRepository.save(bookingDetail);
    }
    
    public void saveAll(List<BookingDetailEntity> bookingDetails) {
        bookingDetailRepository.saveAll(bookingDetails);
    }
}
