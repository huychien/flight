/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flight_booking.service;

import com.flight_booking.entities.PaymentEntity;
import com.flight_booking.repository.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author huythang
 */
@Service
public class PaymentService {
    
    @Autowired
    private PaymentRepository paymentRepository;
    
    public void save(PaymentEntity paymentEntity) {
        paymentRepository.save(paymentEntity);
    }
    
    public PaymentEntity findByBookingId(Integer id) {
        return paymentRepository.findByBookingId(id);
    }
}
