/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flight_booking.service;

import com.flight_booking.entities.PassengerEntity;
import com.flight_booking.repository.PassengerRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author huythang
 */
@Service
public class PassengerService {
    
    @Autowired
    private PassengerRepository passengerRepository;
    
    public void save(List<PassengerEntity> passengers) {
        passengerRepository.saveAll(passengers);
    }
}
