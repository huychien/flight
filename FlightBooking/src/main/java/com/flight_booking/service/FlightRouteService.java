/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flight_booking.service;

import com.flight_booking.entities.FlightRouteEntity;
import com.flight_booking.repository.FlightRouteRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author huythang
 */
@Service
public class FlightRouteService {

    @Autowired
    private FlightRouteRepository flightRouteRepository;

    public List<FlightRouteEntity> getDestinationByDeparture(int departure) {
        return flightRouteRepository.findByDeparture(departure);

    }

    public List<FlightRouteEntity> getFlightRoutes() {
        return flightRouteRepository.findAll();
    }

    public FlightRouteEntity findFlightRouteById(int id) {
        Optional<FlightRouteEntity> f = flightRouteRepository.findById(id);
        if (f.isPresent()) {
            return f.get();
        } else {
            return new FlightRouteEntity();
        }
    }

    public boolean deleteFlightRoute(int id) {
        flightRouteRepository.deleteById(id);
        return flightRouteRepository.existsById(id);
    }

    public void save(FlightRouteEntity flightRouteEntity) {
        flightRouteRepository.save(flightRouteEntity);
    }
    
    public List<FlightRouteEntity> findByDepartureName(String departure) {
        return flightRouteRepository.findByDepartureName(departure);
    }
}
