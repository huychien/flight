/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flight_booking.service;

import com.flight_booking.entities.AircraftEntity;
import com.flight_booking.repository.AircraftRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author huythang
 */
@Service
public class AircraftService {

    @Autowired
    private AircraftRepository aircraftRepository;

    public List<AircraftEntity> getAircrafts() {
        return (List<AircraftEntity>) aircraftRepository.findAll();
    }

    public void save(AircraftEntity aircraft, String action) {
        aircraftRepository.save(aircraft);
    }

    public AircraftEntity findAircraftById(int id) {
        Optional<AircraftEntity> o = aircraftRepository.findById(id);
        if (o.isPresent()) {
            return o.get();
        } else {
            return new AircraftEntity();
        }
    }

    public List<AircraftEntity> findByNameOrNumber(String schAircraft) {

        return aircraftRepository.findByName(schAircraft);
    }
}
