/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flight_booking.service;

import com.flight_booking.entities.PromotionEntity;
import com.flight_booking.repository.PromotionRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author huythang
 */
@Service
public class PromotionsService {
    
    @Autowired
    private PromotionRepository promotionRepository;
    
    public List<PromotionEntity> getAll() {
        return promotionRepository.findAll();
    }
    
    public PromotionEntity getPromotion(int id) {
        return promotionRepository.findById(id).get();
    }
}
