/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flight_booking.enums;

/**
 *
 * @author ASUS
 */
public enum FlightStatus {
    WAITING_DEPARTURE, // chờ khởi hành
    TAKE_OFF, // cất cánh
    LANDING; // hạ cánh
}
