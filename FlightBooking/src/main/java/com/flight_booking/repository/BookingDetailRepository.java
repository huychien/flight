/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flight_booking.repository;

import com.flight_booking.entities.BookingDetailEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author huycong
 */
@Repository
public interface BookingDetailRepository extends JpaRepository<BookingDetailEntity, Integer>{
    
    @Query(value = " SELECT * from booking_detail s "
            + " where s.booking_id = ?1", nativeQuery = true)
    List<BookingDetailEntity> findByBookingId(int bookingId);
}

