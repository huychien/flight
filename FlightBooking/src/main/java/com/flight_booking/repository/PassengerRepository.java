/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flight_booking.repository;

import com.flight_booking.entities.PassengerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author huythang
 */
@Repository
public interface PassengerRepository extends JpaRepository<PassengerEntity, Integer> {
    
}
