/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flight_booking.repository;

import com.flight_booking.entities.AirportEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author huythang
 */
@Repository
public interface AirportRepository extends JpaRepository<AirportEntity, Integer>{
  
    @Query(" Select b From AirportEntity b where b.cityOfAirportEntity.name like %?1% "
            + " or b.name like %?2% ")
    List<AirportEntity> findByCityNameOrAirportName(
            String cityOfAirportName, String airportName);
}
