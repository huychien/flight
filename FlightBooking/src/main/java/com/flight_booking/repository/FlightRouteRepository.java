/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flight_booking.repository;

import com.flight_booking.entities.FlightRouteEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author huythang
 */
@Repository
public interface FlightRouteRepository extends JpaRepository<FlightRouteEntity, Integer>{
    
    @Query(value = " SELECT * from flight_route fr "
            + " inner join airport ap on fr.departure_id = ap.id "
            + " where ap.city_of_airport_id=?1 ", nativeQuery = true)
    List<FlightRouteEntity> findByDeparture(int departure);
    
    @Query(value = " SELECT * from flight_route fr "
            + " inner join airport ap on fr.departure_id = ap.id "
            + " inner join city_of_airport ct on ct.id = ap.city_of_airport_id "
            + " where ct.name like %?1% ", nativeQuery = true)
    List<FlightRouteEntity> findByDepartureName(String departure);
}
