/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flight_booking.repository;

import com.flight_booking.entities.AircraftEntity;
import com.flight_booking.entities.SeatsEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author huythang
 */
@Repository
public interface SeatsRepository extends JpaRepository<SeatsEntity, Integer> {
    
    List<SeatsEntity> findByAircraft(AircraftEntity aircraftEntity);
    
    SeatsEntity findByCodeAndAircraft(String code, AircraftEntity aircraft);
    
    @Query(value = " SELECT * from seats s "
            + " where s.aircraft_id = ?1 ", nativeQuery = true)
    List<SeatsEntity> findByAircraftId(Integer aircraftId);
    
    @Query(value = " SELECT COUNT(id) from seats s "
            + " where s.aircraft_id = ?1 and s.seat_type_id = '1' and s.seat_status = 'AVAILABLE'", nativeQuery = true)
    int getAvailableEconomySeatNum(Integer aircraftId);
    
    @Query(value = " SELECT COUNT(id) from seats s "
            + " where s.aircraft_id = ?1 and s.seat_status = 'AVAILABLE' and s.seat_type_id = '2'", nativeQuery = true)
    int getAvailableBusinessSeatNum(Integer aircraftId);
}
