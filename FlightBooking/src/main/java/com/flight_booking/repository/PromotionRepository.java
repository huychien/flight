/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flight_booking.repository;

import com.flight_booking.entities.PromotionEntity;
import java.time.LocalDate;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author huythang
 */
@Repository
public interface PromotionRepository extends JpaRepository<PromotionEntity, Integer>{
    
    @Query(value = " SELECT * from promotion p "
            + " where p.start_date = ?1 ", nativeQuery = true)
    List<PromotionEntity> findByStartDate(LocalDate startDate);
    
    
}
