/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flight_booking.repository;

import com.flight_booking.entities.PaymentEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author huythang
 */
@Repository
public interface PaymentRepository extends JpaRepository<PaymentEntity, Integer> {
    
    @Query(value = " Select * from payment p "
            + " where p.booking_id = ?1 ", nativeQuery = true)
    PaymentEntity findByBookingId(Integer id);
}
