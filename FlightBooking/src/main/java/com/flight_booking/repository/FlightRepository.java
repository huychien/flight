/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flight_booking.repository;

import com.flight_booking.entities.FlightEntity;
import com.flight_booking.entities.FlightRouteEntity;
import java.time.LocalDate;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author huythang
 */
@Repository
public interface FlightRepository extends JpaRepository<FlightEntity, Integer> {

    @Query(value = " SELECT * FROM flight f "
            + " inner join flight_route fr on f.flight_route_id = fr.id "
            + " inner join airport ap1 on fr.departure_id = ap1.id "
            + " inner join airport ap2 on fr.destination_id = ap2.id "
            + " where f.departure_date = ?1 and ap1.city_of_airport_id = ?2 and ap2.city_of_airport_id = ?3 and f.status = 'WAITING_DEPARTURE'",
            nativeQuery = true)
    List<FlightEntity> findByDepartureDateAndPlace(LocalDate departureDate, int departureId, int destinationId);

    FlightEntity findByFlightRoute(FlightRouteEntity flightRouteEntity);

    List<FlightEntity> findByDepartureDate(LocalDate departureDate);

}

