/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flight_booking.repository;

import com.flight_booking.entities.AircraftEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface AircraftRepository extends JpaRepository<AircraftEntity, Integer>{
//    @Query(" Select b From AircraftEntity b where b.name like %?1% ")
//    List<AircraftEntity> findByName(String name);
    
    List<AircraftEntity> findByName(String name);
}
