/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flight_booking.model;

import com.flight_booking.entities.BookingDetailEntity;
import com.flight_booking.entities.BookingEntity;
import com.flight_booking.entities.FlightEntity;
import com.flight_booking.entities.PassengerEntity;
import java.util.List;

/**
 *
 * @author huythang
 */
public class BookingFlight {

    private FlightEntity flightGo;
    private FlightEntity flightReturn;
    private List<PassengerEntity> passengerEntities;
    private List<BookingDetailEntity> bookingDetails;
    private Integer[] seatIdsGo;
    private Integer[] seatIdsReturn;
    private BookingEntity bookingEntity;
    private double totalPrice;
    private double unit_price_of_adults_for_flightGo;
    private double unit_price_of_children_for_flightGo;
    private double unit_price_of_infants_for_flightGo;
    private double unit_price_of_adults_for_flightReturn;
    private double unit_price_of_children_for_flightReturn;
    private double unit_price_of_infants_for_flightReturn;
    
    public BookingFlight() {
    }

    public FlightEntity getFlightGo() {
        return flightGo;
    }

    public void setFlightGo(FlightEntity flightGo) {
        this.flightGo = flightGo;
    }

    public FlightEntity getFlightReturn() {
        return flightReturn;
    }

    public void setFlightReturn(FlightEntity flightReturn) {
        this.flightReturn = flightReturn;
    } 

    public List<BookingDetailEntity> getBookingDetails() {
        return bookingDetails;
    }

    public void setBookingDetails(List<BookingDetailEntity> bookingDetails) {
        this.bookingDetails = bookingDetails;
    }

    public List<PassengerEntity> getPassengerEntities() {
        return passengerEntities;
    }

    public void setPassengerEntities(List<PassengerEntity> passengerEntities) {
        this.passengerEntities = passengerEntities;
    }

    public BookingEntity getBookingEntity() {
        return bookingEntity;
    }

    public void setBookingEntity(BookingEntity bookingEntity) {
        this.bookingEntity = bookingEntity;
    }
    
    public void addPassenger(PassengerEntity passengerEntity) {
        passengerEntities.add(passengerEntity);
    }

    public double getUnit_price_of_adults_for_flightGo() {
        return unit_price_of_adults_for_flightGo;
    }

    public void setUnit_price_of_adults_for_flightGo(double unit_price_of_adults_for_flightGo) {
        this.unit_price_of_adults_for_flightGo = unit_price_of_adults_for_flightGo;
    }

    public double getUnit_price_of_children_for_flightGo() {
        return unit_price_of_children_for_flightGo;
    }

    public void setUnit_price_of_children_for_flightGo(double unit_price_of_children_for_flightGo) {
        this.unit_price_of_children_for_flightGo = unit_price_of_children_for_flightGo;
    }

    public double getUnit_price_of_infants_for_flightGo() {
        return unit_price_of_infants_for_flightGo;
    }

    public void setUnit_price_of_infants_for_flightGo(double unit_price_of_infants_for_flightGo) {
        this.unit_price_of_infants_for_flightGo = unit_price_of_infants_for_flightGo;
    }

    public double getUnit_price_of_adults_for_flightReturn() {
        return unit_price_of_adults_for_flightReturn;
    }

    public void setUnit_price_of_adults_for_flightReturn(double unit_price_of_adults_for_flightReturn) {
        this.unit_price_of_adults_for_flightReturn = unit_price_of_adults_for_flightReturn;
    }

    public double getUnit_price_of_children_for_flightReturn() {
        return unit_price_of_children_for_flightReturn;
    }

    public void setUnit_price_of_children_for_flightReturn(double unit_price_of_children_for_flightReturn) {
        this.unit_price_of_children_for_flightReturn = unit_price_of_children_for_flightReturn;
    }

    public double getUnit_price_of_infants_for_flightReturn() {
        return unit_price_of_infants_for_flightReturn;
    }

    public void setUnit_price_of_infants_for_flightReturn(double unit_price_of_infants_for_flightReturn) {
        this.unit_price_of_infants_for_flightReturn = unit_price_of_infants_for_flightReturn;
    }

    public Integer[] getSeatIdsGo() {
        return seatIdsGo;
    }

    public void setSeatIdsGo(Integer[] seatIdsGo) {
        this.seatIdsGo = seatIdsGo;
    }

    public Integer[] getSeatIdsReturn() {
        return seatIdsReturn;
    }

    public void setSeatIdsReturn(Integer[] seatIdsReturn) {
        this.seatIdsReturn = seatIdsReturn;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }
}
