/* global value, ul */

$('#oneway').click(function () {
    $('#return-date').hide();
    $('#returnDate').removeAttr("required");
    $('#departure-date').removeClass("col-md-6");
    $('#departure-date').addClass("col-md-12");
});

$('#round').click(function () {
    $('#return-date').show();

    var input = document.getElementById("returnDate");
    input.setAttribute("required", "");

    $('#departure-date').addClass("col-md-6");
    $('#departure-date').removeClass("col-md-12");
});

$(function () {
    $('#departure').next().children("span.current").html("Select a location...");
    $('#departure').next().children("ul.list").children('li').removeClass('selected');
    $('#departure').next().children("ul.list").children('li')[0].setAttribute("class", "option selected");
    
    $('#destination').next().children("span.current").html("Select a location...");
    $('#destination').next().children("ul.list").html("<li data-value='' class='option selected'>Select a location...</li>");
});

$('#departure').change(function () {
    $.ajax({url: "/FlightBooking/api/search_cities", data: {'departure_id': this.value}, success: function (result) {
            $('#destination').next().children("span.current").html("Select a location...");
            $('#destination').next().children("ul.list").html("<li data-value='' class='option selected'>Select a location...</li>");
            for (i in result) {
                li = "<li data-value='" + result[i].id + "' class='option'>" + result[i].name + "</li>";
                $('#destination').next().children("ul.list").append(li);
                $('#destination').append("<option value='" + result[i].id + "'>" + result[i].name + "</option>");
            }
        }});
});

$('#departureDate').change(function () {
    $.ajax({url: "/FlightBooking/api/departure-date", data: {'departureDate': this.value}, success: function (result) {

            var input = document.getElementById("returnDate");
            input.setAttribute("min", result);
        }});
});

var totalGo = 0;
var totalReturn = 0;
$('#total_price').html(totalGo + totalReturn);

var Number_of_adults = parseInt($('#adults')[0].value);
var Number_of_children = parseInt($('#children')[0].value);
var Number_of_infants = parseInt($('#infants')[0].value);
var totalPeople = Number_of_adults + Number_of_children + Number_of_infants;
var people_sitting = Number_of_adults + Number_of_children;

$('.ECONOMY1').click(function () {
    id = this.getAttribute("id");

    var seatsLeft = $('#' + id).next().val();
    if (people_sitting > seatsLeft) {
        $('#' + id).prop('checked', false);
        $('#' + id).next().next().next().html('There are not enough seats for this price');
        return;
    }

    id = id.replace('ECONOMY_', '');
    route_price = parseInt($('#priceOfRoute_' + id)[0].value);
    economy_price = parseInt(this.value);

    unit_price_of_adults_for_flightGo = route_price + economy_price;
    unit_price_of_children_for_flightGo = route_price + (economy_price * 50 / 100);
    unit_price_of_infants_for_flightGo = route_price + (economy_price * 30 / 100);

    i = (unit_price_of_adults_for_flightGo * Number_of_adults) + (unit_price_of_children_for_flightGo * Number_of_children)
            + (unit_price_of_infants_for_flightGo * Number_of_infants);
    totalGo += (i);

    business_price = parseInt($('#BUSINESS_' + id)[0].value);

    if (totalGo / i === 2) {
        totalGo -= i;
    }

    if (totalGo > i) {
        totalGo -= (route_price * totalPeople + (business_price * Number_of_adults) + (business_price * Number_of_children * 50 / 100) + (business_price * Number_of_infants * 30 / 100));
    }

    $('#total_price').html(totalGo + totalReturn);

    $('#title1').html('Departure flight');

    $('#price_of_members_flightGo').html('Adults: ' + Number_of_adults + ' x ' + unit_price_of_adults_for_flightGo +
            ' = ' + (unit_price_of_adults_for_flightGo * Number_of_adults) + ' $<br>' +
            'Children: ' + Number_of_children + ' x ' + unit_price_of_children_for_flightGo +
            ' = ' + (unit_price_of_children_for_flightGo * Number_of_children) + ' $<br>' +
            'Infants: ' + Number_of_infants + ' x ' + unit_price_of_infants_for_flightGo +
            ' = ' + (unit_price_of_infants_for_flightGo * Number_of_infants) + ' $<br>');
});

$('.BUSINESS1').click(function () {
    id = this.getAttribute("id");

    var seatsLeft = $('#' + id).next().val();
    if (people_sitting > seatsLeft) {
        $('#' + id).prop('checked', false);
        $('#' + id).next().next().next().html('There are not enough seats for this price');
        return;
    }

    id = id.replace('BUSINESS_', '');
    route_price = parseInt($('#priceOfRoute_' + id)[0].value);
    business_price = parseInt(this.value);

    unit_price_of_adults_for_flightGo = route_price + business_price;
    unit_price_of_children_for_flightGo = route_price + (business_price * 50 / 100);
    unit_price_of_infants_for_flightGo = route_price + (business_price * 30 / 100);

    i = (unit_price_of_adults_for_flightGo * Number_of_adults) + (unit_price_of_children_for_flightGo * Number_of_children)
            + (unit_price_of_infants_for_flightGo * Number_of_infants);
    totalGo += (i);

    economy_price = parseInt($('#ECONOMY_' + id)[0].value);

    if (totalGo / i === 2) {
        totalGo -= i;
    }
    if (totalGo > i) {
        totalGo -= (route_price * totalPeople + (economy_price * Number_of_adults) + (economy_price * Number_of_children * 50 / 100) + (economy_price * Number_of_infants * 30 / 100));
    }

    $('#total_price').html(totalGo + totalReturn);

    $('#title1').html('Departure flight');

    $('#price_of_members_flightGo').html('Adults: ' + Number_of_adults + ' x ' + unit_price_of_adults_for_flightGo +
            ' = ' + (unit_price_of_adults_for_flightGo * Number_of_adults) + ' $<br>' +
            'Children: ' + Number_of_children + ' x ' + unit_price_of_children_for_flightGo +
            ' = ' + (unit_price_of_children_for_flightGo * Number_of_children) + ' $<br>' +
            'Infants: ' + Number_of_infants + ' x ' + unit_price_of_infants_for_flightGo +
            ' = ' + (unit_price_of_infants_for_flightGo * Number_of_infants) + ' $<br>');
});

$('.ECONOMY2').click(function () {
    id = this.getAttribute('id');

    var seatsLeft = $('#' + id).next().val();
    if (people_sitting > seatsLeft) {
        $('#' + id).prop('checked', false);
        $('#' + id).next().next().next().html('There are not enough seats for this price');
        return;
    }

    id = id.replace('ECONOMY_', '');
    route_price = parseInt($('#priceOfRoute_' + id)[0].value);
    economy_price = parseInt(this.value);

    unit_price_of_adults_for_flightReturn = route_price + economy_price;
    unit_price_of_children_for_flightReturn = route_price + (economy_price * 50 / 100);
    unit_price_of_infants_for_flightReturn = route_price + (economy_price * 30 / 100);

    i = (unit_price_of_adults_for_flightReturn * Number_of_adults) + (unit_price_of_children_for_flightReturn * Number_of_children)
            + (unit_price_of_infants_for_flightReturn * Number_of_infants);
    totalReturn += (i);

    business_price = parseInt($('#BUSINESS_' + id)[0].value);

    if (totalReturn / i === 2) {
        totalReturn -= i;
    }

    if (totalReturn > i) {
        totalReturn -= (route_price * totalPeople + (business_price * Number_of_adults) + (business_price * Number_of_children * 50 / 100) + (business_price * Number_of_infants * 30 / 100));
    }

    $('#total_price').html(totalGo + totalReturn);

    $('#title2').html('Return flight');

    $('#price_of_members_flightReturn').html('Adults: ' + Number_of_adults + ' x ' + unit_price_of_adults_for_flightReturn +
            ' = ' + (unit_price_of_adults_for_flightReturn * Number_of_adults) + ' $<br>' +
            'Children: ' + Number_of_children + ' x ' + unit_price_of_children_for_flightReturn +
            ' = ' + (unit_price_of_children_for_flightReturn * Number_of_children) + ' $<br>' +
            'Infants: ' + Number_of_infants + ' x ' + unit_price_of_infants_for_flightReturn +
            ' = ' + (unit_price_of_infants_for_flightReturn * Number_of_infants) + ' $<br>');
});

$('.BUSINESS2').click(function () {
    id = this.getAttribute('id');

    var seatsLeft = $('#' + id).next().val();
    if (people_sitting > seatsLeft) {
        $('#' + id).prop('checked', false);
        $('#' + id).next().next().next().html('There are not enough seats for this price');
        return;
    }

    id = id.replace('BUSINESS_', '');
    route_price = parseInt($('#priceOfRoute_' + id)[0].value);
    business_price = parseInt(this.value);

    unit_price_of_adults_for_flightReturn = route_price + business_price;
    unit_price_of_children_for_flightReturn = route_price + (business_price * 50 / 100);
    unit_price_of_infants_for_flightReturn = route_price + (business_price * 30 / 100);

    i = (unit_price_of_adults_for_flightReturn * Number_of_adults) + (unit_price_of_children_for_flightReturn * Number_of_children)
            + (unit_price_of_infants_for_flightReturn * Number_of_infants);
    totalReturn += (i);

    economy_price = parseInt($('#ECONOMY_' + id)[0].value);

    if (totalReturn / i === 2) {
        totalReturn -= i;
    }
    if (totalReturn > i) {
        totalReturn -= (route_price * totalPeople + (economy_price * Number_of_adults) + (economy_price * Number_of_children * 50 / 100) + (economy_price * Number_of_infants * 30 / 100));
    }

    $('#total_price').html(totalGo + totalReturn);

    $('#title2').html('Return flight');

    $('#price_of_members_flightReturn').html('Adults: ' + Number_of_adults + ' x ' + unit_price_of_adults_for_flightReturn +
            ' = ' + (unit_price_of_adults_for_flightReturn * Number_of_adults) + ' $<br>' +
            'Children: ' + Number_of_children + ' x ' + unit_price_of_children_for_flightReturn +
            ' = ' + (unit_price_of_children_for_flightReturn * Number_of_children) + ' $<br>' +
            'Infants: ' + Number_of_infants + ' x ' + unit_price_of_infants_for_flightReturn +
            ' = ' + (unit_price_of_infants_for_flightReturn * Number_of_infants) + ' $<br>');
});

$(function () {

    function getSeats() {

        var aircraft_ids = new Array();

        for (var item in $('.aircraft_id')) {

            aircraft_ids.push($('.aircraft_id')[item].value);
        }

        $.ajax({
            url: "/FlightBooking/api/search_available_seats_num",
            dataType: "json",
            traditional: true,
            data: {'aircraft_ids[]': aircraft_ids}, success: function (result) {
                for (var item in result) {
                    $('#' + $('.input_of_availableSeatNum')[item].getAttribute('id')).attr("value", result[item]);
                    var availableSeatNum = $('#' + $('.availableSeatNum')[item].getAttribute('id'));
                    
                    switch (result[item]) {
                        case 0:
                            availableSeatNum.html('Sold out');
                            availableSeatNum.next().next().hide();
                            break;
                        case 1:
                            availableSeatNum.html('Only ' + result[item] + ' seat left');
                            break;
                        default:
                            availableSeatNum.html('Only ' + result[item] + ' seats left');
                    }
                }
            }});
    }
    ;
    getSeats();
    setInterval(getSeats, 1000);
});

$(function () {
    var $inputsReturn = $('input.select-seat-type-return').prop("checked", false);
    $inputsReturn.click(function () {

        if ($('input.select-seat-type-go:checked').length === 1 && $('input.select-seat-type-return:checked').length === 1) {
            $('#btn').removeAttr("disabled");
        } else {
            $('#btn').attr('disabled', '');
        }

        if ($('input.select-seat-type-go:checked').length === 0) {
            $('#title1').html('');
            $('#price_of_members_flightGo').html('');
            totalGo = 0;
            $('#total_price').html(totalGo + totalReturn);
        }

        if ($('input.select-seat-type-return:checked').length === 0) {
            $('#title2').html('');
            $('#price_of_members_flightReturn').html('');
            totalReturn = 0;
            $('#total_price').html(totalGo + totalReturn);
        }
    });

    var $inputsGo = $('input.select-seat-type-go').prop("checked", false);
    $inputsGo.click(function () {

        if ($inputsReturn.length !== 0) {
            if ($('input.select-seat-type-return:checked').length === 1 && $('input.select-seat-type-go:checked').length === 1) {
                $('#btn').removeAttr("disabled");
            } else {
                $('#btn').attr('disabled', '');
            }
        } else if ($('input.select-seat-type-go:checked').length === 1) {
            $('#btn').removeAttr("disabled");
        } else {
            $('#btn').attr('disabled', '');
        }

        if ($('input.select-seat-type-go:checked').length === 0) {
            $('#title1').html('');
            $('#price_of_members_flightGo').html('');
            totalGo = 0;
            $('#total_price').html(totalGo + totalReturn);
        }

        if ($('input.select-seat-type-return:checked').length === 0) {
            $('#title2').html('');
            $('#price_of_members_flightReturn').html('');
            totalReturn = 0;
            $('#total_price').html(totalGo + totalReturn);
        }
    });
});

$('#btn').click(function () {

    try {
        if ($('.select-seat-type-return').length <= 0) {
            unit_price_of_adults_for_flightReturn = 0;
            unit_price_of_children_for_flightReturn = 0;
            unit_price_of_infants_for_flightReturn = 0;
        }

        var flight_go_id = 0, flight_return_id = 0, seatType_go = "", seatType_return = "";

        for (var i = 0; i < $('.select-seat-type-go').length; i++) {
            if ($('.select-seat-type-go')[i].checked) {
                flight_go_id = $('.select-seat-type-go')[i].getAttribute('id');
                flight_go_id = flight_go_id.replace('BUSINESS_', '');
                flight_go_id = flight_go_id.replace('ECONOMY_', '');
                seatType_go = $('.select-seat-type-go')[i].getAttribute('id');
                seatType_go = seatType_go.replace('_' + flight_go_id, '');
                break;
            }
        }

        for (var i = 0; i < $('.select-seat-type-return').length; i++) {
            if ($('.select-seat-type-return')[i].checked) {
                flight_return_id = $('.select-seat-type-return')[i].getAttribute('id');
                flight_return_id = flight_return_id.replace('BUSINESS_', '');
                flight_return_id = flight_return_id.replace('ECONOMY_', '');
                seatType_return = $('.select-seat-type-return')[i].getAttribute('id');
                seatType_return = seatType_return.replace('_' + flight_return_id, '');
                break;
            }
        }

        if (flight_go_id === 0) {
            throw 400;
        }

        if ($('.select-seat-type-return').length > 0) {
            if (flight_return_id === 0) {
                throw 400;
            }
        }

        $.ajax({url: "/FlightBooking/api/compareTwoFlight", data: {'flight_go_id': flight_go_id,
                'flight_return_id': flight_return_id}, success: function (result) {

                if (result === true) {

                    $.ajax({url: "/FlightBooking/api/total_and_unit_price", data: {'total': totalGo + totalReturn,
                            'unit_price_of_adults_for_flightGo': unit_price_of_adults_for_flightGo,
                            'unit_price_of_children_for_flightGo': unit_price_of_children_for_flightGo,
                            'unit_price_of_infants_for_flightGo': unit_price_of_infants_for_flightGo,
                            'unit_price_of_adults_for_flightReturn': unit_price_of_adults_for_flightReturn,
                            'unit_price_of_children_for_flightReturn': unit_price_of_children_for_flightReturn,
                            'unit_price_of_infants_for_flightReturn': unit_price_of_infants_for_flightReturn}, success: function (result) {

                        }});

                    window.location.href = '/FlightBooking/seats?flight_go_id=' + flight_go_id + '&flight_return_id=' + flight_return_id + '&seatType_go=' + seatType_go + '&seatType_return=' + seatType_return;
                } else {
                    $('#message').html("The date and time of the departure flight must be earlier than the return flight !");
                }
            }});
    } catch (exception) {
        $('#message').html("Please choice flight!!!");
    }
});
