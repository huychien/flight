/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$('#btn').click(function () {

    Number_of_adults = parseInt($('#adults')[0].value);
    Number_of_children = parseInt($('#children')[0].value);
    Number_of_infants = parseInt($('#infants')[0].value);
    error = true;

    $.ajax({url: "/FlightBooking/api/initPassenger"
    });

    for (var i = 0; i < Number_of_adults; i++) {
        var adults = $('#adults' + i).children('input.adults');
        var birthDate_of_adults = new Date(adults[3].value);
        now = new Date();
        age = now.getFullYear() - birthDate_of_adults.getFullYear();

        if (age >= 12) {
            $('#message_for_adults' + i).html('');

            $.ajax({url: "/FlightBooking/api/info_adults", data: {'fullName': adults[0].value, 'phone': adults[1].value,
                    'address': adults[2].value, 'birthDate': birthDate_of_adults, 'gender': $('select.adults' + i).children('option:selected').val(), 'idCard': adults[4].value}, success: function (result) {

                }});
        } else {
            $('#message_for_adults' + i).html(adults[0].value + ' are not an adult !!!').css('color', 'red');
            error = false;
        }
    }

    for (var i = 0; i < Number_of_children; i++) {
        var children = $('#children' + i).children('input.children');
        var birthDate_of_children = new Date(children[2].value);
        now = new Date();
        age = now.getFullYear() - birthDate_of_children.getFullYear();

        if (age < 12 && age >= 2) {
            $('#message_for_children' + i).html('');

            $.ajax({url: "/FlightBooking/api/info_children", data: {'fullName': children[0].value,
                    'address': children[1].value, 'birthDate': birthDate_of_children, 'gender': $('select.children' + i).children('option:selected').val()}, success: function (result) {

                }});
        } else {
            $('#message_for_children' + i).html(children[0].value + ' are not a child !!!').css('color', 'red');
            error = false;
        }
    }

    for (var i = 0; i < Number_of_infants; i++) {
        var infants = $('#infants' + i).children('input.infants');
        var birthDate_of_infants = new Date(infants[2].value);
        now = new Date();
        age = now.getFullYear() - birthDate_of_infants.getFullYear();

        if (age < 2) {
            $('#message_for_infants' + i).html('');

            $.ajax({url: "/FlightBooking/api/info_infants", data: {'fullName': infants[0].value,
                    'address': infants[1].value, 'birthDate': birthDate_of_infants, 'gender': $('select.infants' + i).children('option:selected').val()}, success: function (result) {

                }});
        } else {
            $('#message_for_infants' + i).html(infants[0].value + ' are not a baby !!!').css('color', 'red');
            error = false;
        }
    }

    if (error === true) {
        window.location.href = '/FlightBooking/payment_by_credit_card';
    }
});

