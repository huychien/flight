/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function startTimer(duration, display) {
    var start = Date.now(),
            diff,
            minutes,
            seconds;
    function timer() {
        // get the number of seconds that have elapsed since 
        // startTimer() was called   
        if (duration !== null) {
            $('#warning').show();
            diff = duration - (((Date.now() - start) / 1000) | 0);
            
            sessionStorage.setItem("diff", diff);
            // does the same job as parseInt truncates the float
            minutes = (diff / 60) | 0;
            seconds = (diff % 60) | 0;

            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;

            display.textContent = minutes + ":" + seconds;

            if (diff === 0) {
                sessionStorage.clear();
                $.ajax({url: "/FlightBooking/api/cancel_booking"});
                window.location.href = '/FlightBooking/home?timeUp=true';
            }
        } else {
            $('#warning').hide();
        }
    }
    ;
    // we don't want to wait a full second before the timer starts
    timer();
    setInterval(timer, 1000);
}

window.onload = function () {
    var fiveMinutes = sessionStorage.getItem("diff"),
            display = document.querySelector('#time');
    startTimer(fiveMinutes, display);
};

