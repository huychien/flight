/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
total_people = parseInt($('#total_people')[0].value);

$(function () {

    var $inputsReturn = $('input.seatsOfAircraftReturn').prop('checked', false);
    $inputsReturn.change(function () {
        if ($('input.seatsOfAircraftReturn:checked').length === total_people) {
            $inputsReturn.not(':checked').prop('disabled', true);
        }

        if ($('input.seatsOfAircraftReturn:checked').length === total_people && $('input.seatsOfAircraftGo:checked').length === total_people) {
            $('#advice')[0].style.display = "none";
            $('#button').removeAttr("disabled");
        }
    });

    var $inputsGo = $('input.seatsOfAircraftGo').prop('checked', false);
    $inputsGo.change(function () {
        if ($('input.seatsOfAircraftGo:checked').length === total_people) {
            $inputsGo.not(':checked').prop('disabled', true);
        }

        if ($inputsReturn.length === 0) {
            if ($('input.seatsOfAircraftGo:checked').length === total_people) {
                $('#advice')[0].style.display = "none";
                $('#button').removeAttr("disabled");
            }
        }

        if ($('input.seatsOfAircraftReturn:checked').length === total_people && $('input.seatsOfAircraftGo:checked').length === total_people) {
            $('#advice')[0].style.display = "none";
            $('#button').removeAttr("disabled");
        }
    });

});

$('#button').click(function () {

    var seat_go_ids = new Array(), seat_return_ids = new Array();

    for (var i = 0; i < $('.seatsOfAircraftGo').length; i++) {
        if ($('.seatsOfAircraftGo')[i].checked) {
            seat_go_id = $('.seatsOfAircraftGo')[i].getAttribute('id');
            seat_go_ids.push(seat_go_id);
        }
    }

    for (var i = 0; i < $('.seatsOfAircraftReturn').length; i++) {
        if ($('.seatsOfAircraftReturn')[i].checked) {
            seat_return_id = $('.seatsOfAircraftReturn')[i].getAttribute('id');
            seat_return_ids.push(seat_return_id);
        }
    }

    if ($('.seatsOfAircraftReturn').length > 0) {
        if ($('.seatsOfAircraftGo:checked').length === total_people && $('.seatsOfAircraftReturn:checked').length === total_people) {
            $.ajax({
                url: "/FlightBooking/api/lockSeats",
                dataType: "json",
                traditional: true,
                data: {'seat_go_ids[]': seat_go_ids, 'seat_return_ids[]': seat_return_ids}, success: function (result) {

                    if (result === true) {
                        window.location.href = '/FlightBooking/orderer';
                    } else {
                        window.location.reload(true);
                        sessionStorage.setItem('message', 'The seats are not available!!!');
                    }     
                }
            });
        } else {
            $('#advice')[0].style.display = "block";
        }
    } else {
        if ($('.seatsOfAircraftGo:checked').length === total_people) {
            $.ajax({
                url: "/FlightBooking/api/lockSeats",
                dataType: "json",
                traditional: true,
                data: {'seat_go_ids[]': seat_go_ids, 'seat_return_ids[]': seat_return_ids}, success: function (result) {

                    if (result === true) {
                        window.location.href = '/FlightBooking/orderer';
                    } else {
                        window.location.reload(true);
                        sessionStorage.setItem('message', 'The seats are not available!!!');
                    }
                }
            });
        } else {
            $('#advice')[0].style.display = "block";
        }
    }
});

$(function () {
    $('#message').html(sessionStorage.getItem('message'));
    sessionStorage.removeItem('message');
});