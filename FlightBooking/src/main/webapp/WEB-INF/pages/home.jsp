<%-- 
    Document   : welcomePage
    Created on : Jun 9, 2020, 10:09:35 AM
    Author     : ldanh
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="zxx" class="no-js">
    <head>
        <jsp:include page="include/head.jsp"/>
        <style>
            label {
                color: white;
            }
            .l {
                float: left;
            }
            .price {
                text-align: center;
            }
        </style>
        <link rel="stylesheet" href="<c:url value="/resources/user/css/slide/carousel.css"/>">
    </head>
    <body>  
        <jsp:include page="include/user/header.jsp"/>    
        <section class="banner-area relative" id="home">
            <div class="container-fluid">
                <div class="row align-items-end justify-content-center" style="margin-top: 130px;">
                    <div class="banner-content col-lg-6 col-md-12" style="text-align: center">
                        <h1 class="title-top" style="text-align: start; font-size: 30px; margin-bottom: 10px">Search Flight</h1>
                        <p style="color: white; background-color: red; border-radius: 10px; margin: auto; width: 50%;">${error}</p>
                        <p style="color: white; background-color: red; border-radius: 10px; margin: auto; width: 50%;">${timeUp}</p>
                        <form action="${pageContext.request.contextPath}/search" method="GET">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="radio-select">
                                        <div class="row">
                                            <div class="col-md-2 col-sm-2 col-xs-2">                                    
                                                <input type="radio" name="trip" id="round" value="round" onchange='this.form.()' checked="">
                                                <label for="round">Round trip</label>
                                            </div>
                                            <div class="col-md-2 col-sm-2 col-xs-2">                   
                                                <input type="radio" name="trip" id="oneway" value="one-way" onchange='this.form.()'>
                                                <label for="oneway">Oneway</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <fieldset>
                                        <label for="departure" class="l">From:</label>
                                        <select name="departure" id="departure">
                                            <option value="">Select a location...</option>
                                            <c:forEach items="${departureCities}" var="city">
                                                <option value="${city.id}">${city.name}</option>                                               
                                            </c:forEach>
                                        </select>
                                    </fieldset>
                                    <p style="margin: 0; color: red">${bad_request}</p>
                                </div>
                                <div class="col-md-6">
                                    <fieldset>
                                        <label for="destination" class="l">To:</label>
                                        <select name="destination" id="destination">
                                            <option value="">Select a location...</option>                                  
                                        </select>
                                    </fieldset>
                                    <p style="margin: 0; color: red">${bad_request}</p>
                                </div>
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6" id="departure-date">
                                            <fieldset>
                                                <label for="departure" class="l">Departure date:</label>
                                                <input name="departureDate" type="date" min="${now}" class="form-control date" id="departureDate" required="" onchange='this.form.()'>
                                            </fieldset>
                                        </div>
                                        <div class="col-md-6" id="return-date">
                                            <fieldset>
                                                <label for="return" class="l">Return date:</label>
                                                <input name="returnDate" type="date" class="form-control date" id="returnDate" required onchange='this.form.()'>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <fieldset>
                                        <label for="adults" class="l">Adults (from 12 years):</label>
                                        <select required name="adults">
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                        </select>
                                    </fieldset>
                                </div>
                                <div class="col-md-4">
                                    <fieldset>
                                        <label for="children" class="l">Children (from 2 - under 12 years):</label>
                                        <select required name="children">
                                            <option value="0">0</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                        </select>
                                    </fieldset>
                                </div>
                                <div class="col-md-4">
                                    <fieldset>
                                        <label for="infants" class="l">Infants (under 2 years):</label>
                                        <select required name="infants">
                                            <option value="0">0</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                        </select>
                                    </fieldset>
                                </div>
                            </div>
                            <button type="submit" class="primary-btn view-btn color-2 mt-20 w-50 text-uppercase" style="border: 5px solid #fff; box-shadow: none; line-height: 33px;"><span>Search</span></button>
                        </form>                          
                    </div>							
                </div>
            </div>
        </section>
        <section class="men-product-area section-gap relative" id="men" style="width: 83%; margin: 15px auto;">
            <div class="overlay overlay-bg"></div>
            <div class="container">
                <div class="row d-flex justify-content-center">
                    <div class="menu-content pb-40">
                        <div class="title text-center">
                            <h1 class="text-white mb-10">Promotion</h1>
                            <p class="text-white">Who are in extremely love with eco friendly system.</p>
                        </div>
                    </div>
                </div>

                <div id="carousel-example" class="carousel slide" data-ride="carousel" style="position: static;">
                    <a class="carousel-control-prev" href="#carousel-example" role="button" data-slide="prev" style="width: 3%; top: 230px; bottom: 145px;">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                                  
                    <a class="carousel-control-next" href="#carousel-example" role="button" data-slide="next" style="width: 3%; top: 230px; bottom: 145px;">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>           


            </div>                   
        </section>      
        <jsp:include page="include/footer.jsp"/>

        <!-- Javascript -->  
        <script src="<c:url value="/resources/user/js/slide/jquery-migrate-3.0.0.min.js"/>"></script>
        <script src="<c:url value="/resources/user/js/slide/jquery.backstretch.min.js"/>"></script>
        <script src="<c:url value="/resources/user/js/slide/wow.min.js"/>"></script>
        <script src="<c:url value="/resources/user/js/slide/scripts.js"/>"></script>
        <script src="<c:url value="/resources/user/js/app.js"/>"></script>
        <script>
            $(function () {
                sessionStorage.clear();
                $.ajax({url: "/FlightBooking/api/cancel_booking"});
            });
        </script>
    </body>
</html>    
