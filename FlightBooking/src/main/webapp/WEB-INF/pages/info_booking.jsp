<%-- 
    Document   : info_booking
    Created on : Dec 16, 2020, 8:16:11 PM
    Author     : huythang
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="include/head.jsp"/>
    </head>
    <body>
        <jsp:include page="include/user/header.jsp"/>
        <section class="banner-area organic-breadcrumb" style="margin-bottom: 40px; margin-top: 130px;">
            <div class="container">
                <div class="breadcrumb-banner d-flex flex-wrap align-items-center">
                    <div class="col-first">
                        <h1>Info Booking Page</h1>
                        <nav class="d-flex align-items-center justify-content-start">
                            <a href="<c:url value="/home"/>">Home<i class="fa fa-caret-right" aria-hidden="true"></i></a>
                            <a href="<c:url value="/info_booking"/>">Info Booking</a>
                        </nav>
                    </div>
                </div>
            </div>
        </section>
        <div class="container">
            <div class="row">
                <div class="col-xs-6 col-md-6">

                </div>
                <div class="col-xs-6 col-md-6">
                    <form action="search_booking" method="GET"> 
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Enter booking code to search" id="txtSearch" name="code"/>
                            <div class="input-group-btn">
                                <button class="btn btn-primary" type="submit">
                                    seach
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <c:if test="${message != null && message != ''}">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-top: 10px">

                        <c:if test="${type == 'false'}">
                            <div class="alert alert-danger">${message}</div>
                        </c:if>

                        <c:if test="${type == 'true'}">
                            <div class="alert alert-success">${message}</div>
                        </c:if>

                    </div>
                </div>
            </c:if>
        </div>            
        <div class="row" style="width: 80%; margin: auto;">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-top: 10px">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover" style="text-align: center">
                        <thead>
                            <tr>
                                <th>Full name</th>
                                <th>Email</th><!-- comment -->
                                <th>Address</th><!-- comment -->
                                <th>Birth date</th>
                                <th>Phone</th><!-- comment -->
                                <th>Gender</th>
                                <th>Id Card</th>
                                <th>Booking date</th>
                                <th>Discount</th>
                                <th>Total price</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:if test="${list_booking != null && fn:length(list_booking) > 0}">
                                <c:forEach items="${list_booking}" var="booking">
                                    <tr>
                                        <td>${booking.fullName}</td><!-- comment -->
                                        <td>${booking.email}</td>
                                        <td>${booking.address}</td>
                                        <td><fmt:formatDate value="${booking.birthDate}" pattern="dd/MM/yyyy"/>
                                        <td>${booking.phone}</td>
                                        <td>${booking.gender}</td>
                                        <td>${booking.idCard}</td>
                                        <td><fmt:formatDate value="${booking.bookingDate}" pattern="dd/MM/yyyy"/>
                                        <td>${booking.discount}</td>
                                        <td><fmt:formatNumber value="${booking.totalPrice}" pattern="###,###" type="number"/> $</td>                       
                                        <td>${booking.status}</td>
                                        <td>
                                            <button class="btn btn-primary" onclick="location.href = '<c:url value="/booking_detail?id=${booking.id}"></c:url>'">Detail</button>
                                            <c:if test="${booking.status != 'CANCEL'}">
                                                <button class="btn btn-danger" onclick="location.href = '<c:url value="/cancel_booking/${booking.id}"></c:url>'">Cancel</button>
                                            </c:if>
                                        </td>
                                    </tr>            
                                </c:forEach>
                            </c:if>
                            <c:if test="${list_booking == null || fn:length(list_booking) <= 0}">
                                <tr>
                                    <td colspan="11" style="color: red">Empty List!!!</td>
                                </tr>
                            </c:if>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <jsp:include page="include/footer.jsp"/>
        <script>
            $(function () {
                sessionStorage.clear();
                $.ajax({url: "/FlightBooking/api/cancel_booking"});
            });
        </script>
    </body>
</html>
