<%-- 
    Document   : result
    Created on : Nov 22, 2020, 3:24:42 PM
    Author     : huythang
--%>

<%@page import="java.time.LocalDate"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://sargue.net/jsptags/time" prefix="javatime" %>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="include/head.jsp"/>
        <style>
            .task {
                text-align: center;
                color: #007bff;
                background-color: #fff;
                font-weight: bold;
            }
            .ticket {
                display: grid;
                grid-template-columns: 1fr 1fr 1fr;
                grid-gap: 10px;
                padding: 0px;
                margin-bottom: 10px;
                background-color: #e5e5e5;
            }
            .type {
                display: grid;
                grid-template-columns: 1fr 1fr 1fr;
                grid-gap: 10px;
                grid-template-rows: 45px;
                padding: 0px;
                margin-bottom: 10px;
            }
            .tg {
                font-size: 18px;
                font-weight: bold;
                text-align: center;
                margin: 15px 0px;
            }
            .tg a {
                font-size: 14px;
                font-weight: 300;
                background-color: #0789d3;
                color: #fff;
                border-radius: 5px;
                padding: 0 5px;
            }
            .duration {
                font-size: 12px;
                font-weight: normal;
                background-color: #B1B3B6;
                color: #fff;
                padding: 3px 6px;
                border-radius: 12px;
                margin: 0 10px;
            }
            .blue {
                text-align: center;
                background-color: #0789d3;
                color: white;
            }
            .green {
                text-align: center; 
                background-color: #1d9e22;
                color: white;
            }
            .aircraft {
                font-size: 12px;
                font-weight: bold;
            }
            .airport {
                font-size: 12px;
                font-weight: normal;
            }
            hr {
                margin-top: 2em;
                border-top: 1px solid #F44336;
            }
            .date {
                display: flex;
                justify-content: space-around;
                font-size: x-small;
                font-weight: 400;
            }
            .ticketECONOMY {
                text-align: center;
                background-color: #1d9e22;
                color: white;
            }
            .ticketBUSINESS {
                text-align: center;
                background-color: #0789d3;
                color: white;
            }
            .sticky {
                position: fixed;
                top: 200px;
                right: 20%;
            }
            #c {
                float: right;
                background-color: white;
                width: 265px;
                padding: 20px;
                border: 2px solid blue;
            }
            .cost {
                margin: 0px;
                font-size: small;
            }
        </style>
    </head>
    <body style="background: #EDF3F7;">
        <jsp:include page="include/user/header.jsp"/>
        <section class="banner-area organic-breadcrumb" style="margin-bottom: 20px; margin-top: 130px;">
            <div class="container">
                <div class="breadcrumb-banner d-flex flex-wrap align-items-center">
                    <div class="col-first">
                        <h1>Flights List</h1>
                        <nav class="d-flex align-items-center justify-content-start">
                            <a href="<c:url value="/home"/>">Home<i class="fa fa-caret-right" aria-hidden="true"></i></a>
                            <a href="" onclick="history.go(0)">Flights</a>
                        </nav>
                    </div>
                </div>
            </div>
        </section>
        <div class="container">
            <div class="row">
                <div class="col-md-2 col-xl-2 col-lg-2">
                    <p class="task" onclick="history.go(0)" style="background-color: #0789d3; color: white">1) Flights</p>
                </div>
                <div class="col-md-3 col-xl-3 col-lg-3">
                    <p class="task">2) Select seats</p>
                </div>
                <div class="col-md-2 col-xl-2 col-lg-2">
                    <p class="task">3) Customer info</p>
                </div>
                <div class="col-md-3 col-xl-3 col-lg-3">
                    <p class="task">4) Passengers info</p>
                </div>
                <div class="col-md-2 col-xl-2 col-lg-2">
                    <p class="task">5) Payment</p>
                </div>
            </div>
            <div id="warning">The remaining booking time is <span id="time" style="color: red;"></span> minutes!</div>
            <div style="width: 100%; text-align: center; margin-top: 20px">
                <p style="color: red;">${message}</p>
            </div>
            <div style="width: 100%; text-align: center; margin-top: 20px">
                <p style="color: red;" id="message"></p>
            </div>
            <div class="section-header locale_en_US">
                <h3 style="margin-top: 15px;">
                    <img src="<c:url value="/resources/user/img/airplane-take-off.png"/>" alt="" style="vertical-align: bottom; width: 70px"/>
                    <p style="display: inline-block"><span style="font-weight: 200;">Choose your flight</span><br>		           
                        <strong>${departure.name}</strong> <span style="font-weight: 200;">to</span> <strong>${destination.name} </strong></p>              
                </h3>
            </div>
            <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9" style="padding: 0 0 10px 0">
                <h4 style="background-color: #0789d3; color: white; padding: 8px;">Departure date: ${dateFormat1.getDayOfWeek()}, ${dateFormat1.getDayOfMonth()}. ${dateFormat1.getMonth()} ${dateFormat1.getYear()}</h4>
            </div>           
            <div id="fixed">
                <div id="v">

                </div>
                <div id="c">
                    <div>
                        <p id="title1" class="cost" style="font-weight: bold"></p>
                        <p id="price_of_members_flightGo" class="cost" style="margin-bottom: 10px;"></p>
                    </div>
                    <div>
                        <p id="title2" class="cost" style="font-weight: bold"></p>
                        <p id="price_of_members_flightReturn" class="cost"></p>
                    </div>
                    <div style="text-align: center">
                        <hr>
                        <strong>Total: <span id="total_price"></span> $</strong><br>
                        <button disabled class="btn btn-success" id="btn">Continue</button>
                    </div>    
                </div>
            </div>
            <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9 type">			       
                <div></div>
                <div class="green"><br>
                    ECONOMY</div>				     

                <div class="blue"><br>
                    BUSINESS</div>				     
            </div>
            <c:forEach items="${goFlights}" var="flight">                                            
                <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9 ticket">
                    <div class="tg">
                        <div class="date">
                            <span><javatime:format value="${flight.departureDate}" pattern="dd-MM-yyyy"/></span>
                            <span><javatime:format value="${flight.arriveTime}" pattern="dd-MM-yyyy"/></span>
                        </div>
                        <c:if test="${flight.departureTime.getHour()-1 <= 9}">0</c:if>${flight.departureTime.getHour()-1}:<c:if test="${flight.departureTime.getMinute() <= 9}">0</c:if>${flight.departureTime.getMinute()}
                        <span class="duration"><c:if test="${flight.flightRoute.duration.getHour()-1 <= 9}">0</c:if>${flight.flightRoute.duration.getHour()-1}:<c:if test="${flight.flightRoute.duration.getMinute() <= 9}">0</c:if>${flight.flightRoute.duration.getMinute()}</span>
                        <c:if test="${flight.arriveTime.getHour() <= 9}">0</c:if>${flight.arriveTime.getHour()}:<c:if test="${flight.arriveTime.getMinute() <= 9}">0</c:if>${flight.arriveTime.getMinute()}<br>
                        <span class="airport">${flight.flightRoute.distance} km</span><br>
                        <span class="airport">${flight.flightRoute.departure.name}</span><br>
                        <span class="aircraft">${flight.aircraft.name} ${flight.aircraft.aircraftNumber}</span><br>
                    </div>      
                    <c:forEach items="${seatTypes}" var="seatType">
                        <div class="ticket${seatType.type}">
                            <br><span style="font-weight: bold"><fmt:formatNumber value="${seatType.priceOfSeat}" type="number" pattern="###,###"/> $</span><br>
                            <span class="availableSeatNum" id="availableSeatNum${seatType.type}_${flight.id}"></span><br>                 
                            <input type="radio" name="select-seat-type-go" id="${seatType.type}_${flight.id}" class="select-seat-type-go ${seatType.type}1" value="${seatType.priceOfSeat}">
                            <input value="" type="hidden" class="input_of_availableSeatNum" id="${seatType.type}_${flight.id}_${flight.aircraft.id}"><br>
                            <span></span>
                        </div>
                    </c:forEach>
                    <input value="${flight.flightRoute.price}" type="hidden" id="priceOfRoute_${flight.id}">
                    <input value="${flight.aircraft.id}" type="hidden" class="aircraft_id">
                </div>
            </c:forEach>

            <c:if test="${!returnFlights.isEmpty() && returnFlights != null}">
                <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9"><hr></div>
                <div class="section-header locale_en_US">
                    <h3 style="margin-top: 15px;">
                        <img src="<c:url value="/resources/user/img/airplane-take-of.png"/>" alt="" style="vertical-align: bottom; width: 70px;"/>
                        <p style="display: inline-block"><span style="font-weight: 200;">Choose your flight</span><br>		           
                            <strong>${destination.name}</strong> <span style="font-weight: 200;">to</span> <strong>${departure.name} </strong></p>              
                    </h3>
                </div>
                <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9" style="padding: 0px 0px 10px 0px">
                    <h4 style="background-color: #0789d3; color: white; padding: 8px;">Return date: ${dateFormat2.getDayOfWeek()}, ${dateFormat2.getDayOfMonth()}. ${dateFormat2.getMonth()} ${dateFormat2.getYear()}</h4>
                </div>
                <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9 type">			       
                    <div></div>
                    <div class="green"><br>
                        ECONOMY</div>				     

                    <div class="blue"><br>
                        BUSINESS</div>				     
                </div>
                <c:forEach items="${returnFlights}" var="flight">                                            
                    <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9 ticket">
                        <div class="tg">
                            <div class="date">
                                <span><javatime:format value="${flight.departureDate}" pattern="dd-MM-yyyy"/></span>
                                <span><javatime:format value="${flight.arriveTime}" pattern="dd-MM-yyyy"/></span>
                            </div>
                            <c:if test="${flight.departureTime.getHour()-1 <= 9}">0</c:if>${flight.departureTime.getHour()-1}:<c:if test="${flight.departureTime.getMinute() <= 9}">0</c:if>${flight.departureTime.getMinute()}
                            <span class="duration"><c:if test="${flight.flightRoute.duration.getHour()-1 <= 9}">0</c:if>${flight.flightRoute.duration.getHour()-1}:<c:if test="${flight.flightRoute.duration.getMinute() <= 9}">0</c:if>${flight.flightRoute.duration.getMinute()}</span>
                            <c:if test="${flight.arriveTime.getHour() <= 9}">0</c:if>${flight.arriveTime.getHour()}:<c:if test="${flight.arriveTime.getMinute() <= 9}">0</c:if>${flight.arriveTime.getMinute()}<br>
                            <span class="airport">${flight.flightRoute.distance} km</span><br>
                            <span class="airport">${flight.flightRoute.departure.name}</span><br>
                            <span class="aircraft">${flight.aircraft.name} ${flight.aircraft.aircraftNumber}</span><br>
                        </div>  
                        <c:forEach items="${seatTypes}" var="seatType">
                            <div class="ticket${seatType.type}">
                                <br><span style="font-weight: bold"><fmt:formatNumber value="${seatType.priceOfSeat}" type="number" pattern="###,###"/> $</span><br>
                                <span class="availableSeatNum" id="availableSeatNum${seatType.type}_${flight.id}"></span><br>    
                                <input type="radio" name="select-seat-type-return" id="${seatType.type}_${flight.id}" class="select-seat-type-return ${seatType.type}2" value="${seatType.priceOfSeat}">
                                <input value="" type="hidden" class="input_of_availableSeatNum" id="${seatType.type}_${flight.id}_${flight.aircraft.id}"><br>
                                <span></span>
                            </div>
                        </c:forEach>
                        <input value="${flight.flightRoute.price}" type="hidden" id="priceOfRoute_${flight.id}">
                        <input value="${flight.aircraft.id}" type="hidden" class="aircraft_id">
                    </div>
                </c:forEach>
            </c:if>
            <input value="${infants}" type="hidden" id="infants">
            <input value="${adults}" type="hidden" id="adults">
            <input value="${children}" type="hidden" id="children">      
        </div>
        <jsp:include page="include/footer.jsp"/>
        <script src="<c:url value="/resources/user/js/app.js"/>"></script>
        <script>
                        window.onscroll = function () {
                            myFunction();
                        };

                        var c = document.getElementById("c");
                        var sticky = c.offsetTop;

                        function myFunction() {
                            if (window.pageYOffset >= sticky) {
                                c.classList.add("sticky");
                            } else {
                                c.classList.remove("sticky");
                            }
                        }
        </script>
        <script src="<c:url value="/resources/user/js/timeLeft2.js"/>" type="text/javascript"></script>
    </body>
</html>
