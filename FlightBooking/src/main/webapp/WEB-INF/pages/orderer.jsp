<%-- 
    Document   : orderer
    Created on : Dec 13, 2020, 10:23:39 AM
    Author     : huythang
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="mvc" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="include/head.jsp"/>
        <style>
            .task {
                text-align: center;
                color: #007bff;
                background-color: #fff;
                font-weight: bold;
            }
            form {
                display: grid;
                margin: 0 50px;
            }
            .common-input.mt-20 {
                margin-top: 0px;
                margin-bottom: 15px;
            }
            h5 {
                background-color: #0d89d2;
                color: white;
                padding: 10px;
            }
            .framework {
                border: 4px solid white;
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body style="background: #EDF3F7;">
        <jsp:include page="include/user/header.jsp"/>
        <section class="banner-area organic-breadcrumb" style="margin-bottom: 40px; margin-top: 130px;">
            <div class="container">
                <div class="breadcrumb-banner d-flex flex-wrap align-items-center">
                    <div class="col-first">
                        <h1>Orderer Page</h1>
                        <nav class="d-flex align-items-center justify-content-start">
                            <a href="<c:url value="/home"/>">Home<i class="fa fa-caret-right" aria-hidden="true"></i></a>
                            <a href="" onclick="history.go(0)">Orderer</a>
                        </nav>
                    </div>
                </div>
            </div>
        </section>
        <div class="container">
            <div class="row">
                <div class="col-md-2 col-xl-2 col-lg-2">
                    <p onclick="history.go(-2)" class="task">1) Flights</p>
                </div>   
                <div class="col-md-3 col-xl-3 col-lg-3">
                    <p class="task" onclick="history.back()">2) Select seats</p>
                </div>
                <div class="col-md-2 col-xl-2 col-lg-2">
                    <p class="task" onclick="history.go(0)" style="background-color: #0789d3; color: white">3) Customer info</p>
                </div>
                <div class="col-md-3 col-xl-3 col-lg-3">
                    <p class="task">4) Passengers info</p>
                </div>
                <div class="col-md-2 col-xl-2 col-lg-2">
                    <p class="task">5) Payment</p>
                </div>
            </div>
            <div>The remaining booking time is <span id="time" style="color: red;"></span> minutes!</div>
            <h4 style="margin-top: 30px;">Who is booking?</h4>
            <p>Your contact information</p>
            <div class="row">
                <div class="col-md-12">
                    <h5 class="billing-title">Contact data</h5>
                    <div class="framework">     
                        <mvc:form action="add_orderer" method="POST" modelAttribute="orderer">
                            <label>Full name*</label>
                            <input type="text" placeholder="Full name*" value="${user.fullName}" name="fullName" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Full name*'" required class="common-input mt-20 booking_people">
                            <label>Email address*</label>
                            <input type="email" placeholder="Email address*" value="${user.email}" name="email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email address*'" required class="common-input mt-20 booking_people">
                            <label>Phone number*</label>
                            <input type="tel" placeholder="Phone number*" value="${user.phone}" name="phone" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Phone number*'" required class="common-input mt-20 booking_people">
                            <label>Address*</label>
                            <input type="text" placeholder="Address*" value="${user.address}" name="address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Address*'" required class="common-input mt-20 booking_people">
                            <label>BrithDate*</label>
                            <input type="date" placeholder="BirthDate*" value="${user.birthDate}" name="birthDate" onfocus="this.placeholder = ''" onblur="this.placeholder = 'BirthDate*'" required class="common-input mt-20 booking_people">
                            <label>Gender*</label>
                            <select name="gender" required class="common-input mt-20 booking_people">
                                <c:if test="${user.gender == 'MALE'}">
                                    <option selected value="${user.gender}">${user.gender}</option>
                                    <option value="FEMALE">FEMALE</option>
                                </c:if>
                                <c:if test="${user.gender != 'MALE'}">
                                    <option value="FEMALE">FEMALE</option>
                                    <option value="MALE">MALE</option>
                                </c:if>
                            </select>
                            <label>IdCard*</label>
                            <input type="number" placeholder="IdCard*" value="${user.idCard}" name="idCard" onfocus="this.placeholder = ''" onblur="this.placeholder = 'IdCard*'" required class="common-input mt-20 booking_people">
                            <button type="submit"class="view-btn color-2 mt-20 w-100" id="btn" style="margin-bottom: 20px"><span>Continue</span></button>
                        </mvc:form>
                    </div>
                </div>
            </div>           
        </div>
        <jsp:include page="include/footer.jsp"/>
        <script src="<c:url value="/resources/user/js/timeLeft.js"/>" type="text/javascript"></script>
    </body>
</html>
