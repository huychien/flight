<%-- 
    Document   : register
    Created on : Nov 21, 2020, 8:46:07 AM
    Author     : huythang
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="mvc" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="include/head.jsp"/>
    </head>
    <body>
        <jsp:include page="include/header.jsp"/>
        <section class="banner-area organic-breadcrumb" style="margin-bottom: 40px; margin-top: 130px;">
            <div class="container">
                <div class="breadcrumb-banner d-flex flex-wrap align-items-center">
                    <div class="col-first">
                        <h1>Register Page</h1>
                        <nav class="d-flex align-items-center justify-content-start">
                            <a href="<c:url value="/home"/>">Home<i class="fa fa-caret-right" aria-hidden="true"></i></a>
                            <a href="<c:url value="/register"/>">Register</a>
                        </nav>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Banner Area -->

        <!-- Start My Account -->
        <div class="container">
            <c:if test="${message != null}">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-top: 10px">
                        <div class="alert alert-success">${message}</div>
                    </div>
                </div>
            </c:if>
            <div class="row">
                <div class="col-md-12">
                    <div class="register-form">
                        <h3 class="billing-title text-center">Register</h3>
                        <p class="text-center mt-40 mb-30">Create your very own account </p>
                        <mvc:form action="addUser" method="post" modelAttribute="user">
                            <input type="text" placeholder="Full name*" name="fullName" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Full name*'" required class="common-input mt-20">
                            <input type="email" placeholder="Email address*" name="email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email address*'" required class="common-input mt-20">
                            <input type="password" placeholder="Password*" name="password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Password*'" required class="common-input mt-20">
                            <input type="tel" placeholder="Phone number*" name="phone" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Phone number*'" required class="common-input mt-20">
                            <input type="text" placeholder="Address*" name="address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Address*'" required class="common-input mt-20">
                            <input type="date" placeholder="BirthDate*" name="birthDate" onfocus="this.placeholder = ''" onblur="this.placeholder = 'BirthDate*'" required class="common-input mt-20"><br>
                            <c:forEach items="${gender}" var="g">
                                <c:if test="${g == account.gender}">
                                    <input name="gender" type="radio" value="${g}" checked required/> <a style="color: white">${g}</a>
                                </c:if>
                                <c:if test="${g != account.gender}">
                                    <input name="gender" type="radio" value="${g}" required/>  <a style="color: white">${g}</a>
                                </c:if>
                            </c:forEach>
                            <input type="number" placeholder="IdCard*" name="idCard" onfocus="this.placeholder = ''" onblur="this.placeholder = 'IdCard*'" required class="common-input mt-20">
                            <p style="color: red;margin-top: 10px; text-align: center;">${fail}</p>
                            <button type="submit" class="view-btn color-2 mt-20 w-100"><span>Register</span></button>
                        </mvc:form>
                    </div>
                </div>
            </div>
        </div>     
        <jsp:include page="include/footer.jsp"/>
        <script>
            $(function () {
                sessionStorage.clear();
                $.ajax({url: "/FlightBooking/api/cancel_booking"});
            });
        </script>
    </body>
</html>
