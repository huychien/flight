<%-- 
    Document   : thankyou
    Created on : Dec 16, 2020, 3:59:48 PM
    Author     : huythang
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="include/head.jsp"/>
        <style>
            footer {
                margin-top: 530px !important;
            }
        </style>
    </head>
    <body>
        <jsp:include page="include/user/header.jsp"/>
        <section class="banner-area organic-breadcrumb" style="margin-bottom: 40px; margin-top: 130px;">
            <div class="container">
                <div class="breadcrumb-banner d-flex flex-wrap align-items-center">
                    <div class="col-first">
                        <h1>Booking Success</h1>
                        <nav class="d-flex align-items-center justify-content-start">
                            <a href="<c:url value="/home"/>">Home<i class="fa fa-caret-right" aria-hidden="true"></i></a>
                        </nav>
                    </div>
                </div>
            </div>
        </section>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p style="font-weight: bold">
                        Thank you for using our service. We have sent booking code to your email please check now:
                        
                        <a href="https://mail.google.com/mail/u/0/?tab=rm&ogbl#inbox">https://mail.google.com/mail/u/0/?tab=rm&ogbl#inbox</a>
                    </p>
                </div>
            </div>
        </div>
        <jsp:include page="include/footer.jsp"/>
        <script>
            $(function () {
                sessionStorage.clear();
                $.ajax({url: "/FlightBooking/api/clearSessionScop"});
            });
        </script>
    </body>
</html>
