<%-- 
    Document   : updateInfo
    Created on : Dec 21, 2020, 7:29:39 PM
    Author     : huythang
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="mvc" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="../include/head.jsp"/>
    </head>
    <body>
        <jsp:include page="../include/user/header.jsp"/>
        <section class="banner-area organic-breadcrumb" style="margin-bottom: 40px; margin-top: 130px;">
            <div class="container">
                <div class="breadcrumb-banner d-flex flex-wrap align-items-center">
                    <div class="col-first">
                        <h1>Info Booking Page</h1>
                        <nav class="d-flex align-items-center justify-content-start">
                            <a href="<c:url value="/user/home"/>">Home<i class="fa fa-caret-right" aria-hidden="true"></i></a>
                            <a href="<c:url value="/user/change_password_page"/>">Change password page</a>
                        </nav>
                    </div>
                </div>
            </div>
        </section>
        <div class="container">
            <c:if test="${message != null && message != ''}">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-top: 10px">

                        <c:if test="${type == 'false'}">
                            <div class="alert alert-danger">${message}</div>
                        </c:if>

                        <c:if test="${type == 'true'}">
                            <div class="alert alert-success">${message}</div>
                        </c:if>

                    </div>
                </div>
            </c:if>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-top: 30px">
                    <div class="table-responsive">
                        <mvc:form action="update_info" method="post" modelAttribute="user">
                            <input class="common-input mt-20" type="hidden" name="id" required value="${user.id}">
                            <input class="common-input mt-20" type="hidden" name="password" required value="${user.password}">
                            <input class="common-input mt-20" type="hidden" name="status" required value="${user.status}">
                            <input class="common-input mt-20" type="hidden" name="email" required value="${user.email}">
                            <input class="common-input mt-20" type="hidden" name="birthDateStr" value="<fmt:formatDate value="${user.birthDate}" pattern="yyyy-MM-dd"/>">
                            <table class="table table-bordered table-hover" style="text-align: center">
                                <tr>
                                    <th>Full name</th>
                                    <td><input class="common-input mt-20" type="text" name="fullName" required value="${user.fullName}"></td>
                                </tr>
                                <tr>
                                    <th>Address</th><!-- comment -->
                                    <td><input class="common-input mt-20" type="text" name="address" required value="${user.address}"></td>
                                </tr>
                                <tr>
                                    <th>Phone</th><!-- comment -->
                                    <td><input class="common-input mt-20" type="tel" name="phone" required value="${user.phone}"></td>
                                </tr>
                                <tr>
                                    <th>Gender</th>
                                    <td>
                                        <select name="gender" required class="common-input mt-20">
                                            <c:if test="${user.gender == 'MALE'}">
                                                <option selected value="${user.gender}">${user.gender}</option>
                                                <option value="FEMALE">FEMALE</option>
                                            </c:if>
                                            <c:if test="${user.gender != 'MALE'}">
                                                <option value="FEMALE">FEMALE</option>
                                                <option value="MALE">MALE</option>
                                            </c:if>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Id Card</th>
                                    <td><input class="common-input mt-20" type="number" name="idCard" value="${user.idCard}"></td>
                                </tr>
                            </table>
                            <button class="btn btn-primary" type="submit">Update</button>
                        </mvc:form>
                    </div>
                </div>
            </div>
        </div>
        <jsp:include page="../include/footer.jsp"/>
        <script>
            $(function () {
                sessionStorage.clear();
                $.ajax({url: "/FlightBooking/api/cancel_booking"});
            });
        </script>
    </body>
</html>
