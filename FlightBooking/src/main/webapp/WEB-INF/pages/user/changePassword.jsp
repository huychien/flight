<%-- 
    Document   : changePasswordPage
    Created on : Dec 21, 2020, 7:29:01 PM
    Author     : huythang
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="../include/head.jsp"/>
    </head>
    <body>
        <jsp:include page="../include/user/header.jsp"/>    
        <section class="banner-area organic-breadcrumb" style="margin-bottom: 40px; margin-top: 130px;">
            <div class="container">
                <div class="breadcrumb-banner d-flex flex-wrap align-items-center">
                    <div class="col-first">
                        <h1>Info Booking Page</h1>
                        <nav class="d-flex align-items-center justify-content-start">
                            <a href="<c:url value="/user/home"/>">Home<i class="fa fa-caret-right" aria-hidden="true"></i></a>
                            <a href="<c:url value="/user/change_password_page"/>">Change password page</a>
                        </nav>
                    </div>
                </div>
            </div>
        </section>
        <div class="container">
            <c:if test="${message != null && message != ''}">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-top: 10px">

                        <c:if test="${type == 'false'}">
                            <div class="alert alert-danger">${message}</div>
                        </c:if>

                        <c:if test="${type == 'true'}">
                            <div class="alert alert-success">${message}</div>
                        </c:if>

                    </div>
                </div>
            </c:if>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-top: 30px">
                    <div class="table-responsive">
                        <form action="change_password" method="get">
                            <table class="table table-hover" style="text-align: center">
                                <tr>
                                    <th>Old password</th>
                                    <td><input type="password" name="old_password" required></td>
                                </tr>
                                <tr>
                                    <th>New password</th><!-- comment -->
                                    <td><input type="password" name="new_password_1" required></td>
                                </tr>
                                <tr>
                                    <th>Enter again new password</th><!-- comment -->
                                    <td><input type="password" name="news_password_2" required></td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <td><button class="btn btn-primary" type="submit">Change</button></td>
                                </tr>
                            </table>
                        </form>
                    </div>
                </div>
            </div>              
        </div>
        <jsp:include page="../include/footer.jsp"/>
        <script>
            $(function () {
                sessionStorage.clear();
                $.ajax({url: "/FlightBooking/api/cancel_booking"});
            });
        </script>
    </body>
</html>
