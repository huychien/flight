<%-- 
    Document   : viewInfo
    Created on : Dec 21, 2020, 7:28:41 PM
    Author     : huythang
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="../include/head.jsp"/>
    </head>
    <body>
        <jsp:include page="../include/user/header.jsp"/>
        <section class="banner-area organic-breadcrumb" style="margin-bottom: 40px; margin-top: 130px;">
            <div class="container">
                <div class="breadcrumb-banner d-flex flex-wrap align-items-center">
                    <div class="col-first">
                        <h1>Info Booking Page</h1>
                        <nav class="d-flex align-items-center justify-content-start">
                            <a href="<c:url value="/user/home"/>">Home<i class="fa fa-caret-right" aria-hidden="true"></i></a>
                            <a href="<c:url value="/user/viewInfo"/>">User Info</a>
                        </nav>
                    </div>
                </div>
            </div>
        </section>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-top: 30px">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover" style="text-align: center">
                            <tr>
                                <th>Full name</th>
                                <td>${user.fullName}</td>
                            </tr>
                            <tr>
                                <th>Email</th><!-- comment -->
                                <td>${user.email}</td>
                            </tr>
                            <tr>
                                <th>Address</th><!-- comment -->
                                <td>${user.address}</td>
                            </tr>
                            <tr>
                                <th>Birth date</th>
                                <td><fmt:formatDate value="${user.birthDate}"/></td>
                            </tr>
                            <tr>
                                <th>Phone</th><!-- comment -->
                                <td>${user.phone}</td>
                            </tr>
                            <tr>
                                <th>Gender</th>
                                <td>${user.gender}</td>
                            </tr>
                            <tr>
                                <th>Id Card</th>
                                <td>${user.idCard}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <jsp:include page="../include/footer.jsp"/>
        <script>
            $(function () {
                sessionStorage.clear();
                $.ajax({url: "/FlightBooking/api/cancel_booking"});
            });
        </script>
    </body>
</html>
