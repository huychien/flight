<%-- 
    Document   : payment
    Created on : Dec 13, 2020, 2:25:16 PM
    Author     : huythang
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="mvc" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://sargue.net/jsptags/time" prefix="javatime" %>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="include/head.jsp"/>
        <style>
            .task {
                text-align: center;
                color: #007bff;
                background-color: #fff;
                font-weight: bold;
            }
            form {
                display: grid;
                margin: 0 50px;
            }
            .common-input.mt-20 {
                margin-top: 0px;
                margin-bottom: 15px;
            }
            h5 {
                background-color: #0d89d2;
                color: white;
                padding: 10px;
            }
            .framework {
                border: 4px solid white;
                margin-bottom: 30px;
            }
            .method {
                padding: 20px;
                color: white;
                background-color: #089de7;
            }
            .method:hover {
                color: white;
            }
        </style>
    </head>
    <body style="background: #EDF3F7;">
        <jsp:include page="include/user/header.jsp"/>
        <section class="banner-area organic-breadcrumb" style="margin-bottom: 40px; margin-top: 130px;">
            <div class="container">
                <div class="breadcrumb-banner d-flex flex-wrap align-items-center">
                    <div class="col-first">
                        <h1>Payment Page</h1>
                        <nav class="d-flex align-items-center justify-content-start">
                            <a href="<c:url value="/home"/>">Home<i class="fa fa-caret-right" aria-hidden="true"></i></a>
                            <a href="" onclick="history.go(0)">Payment</a>
                        </nav>
                    </div>
                </div>
            </div>
        </section>
        <div class="container">
            <div class="row">
                <div class="col-md-2 col-xl-2 col-lg-2">
                    <p onclick="history.go(-4)" class="task">1) Flights</p>
                </div>
                <div class="col-md-3 col-xl-3 col-lg-3">
                    <p onclick="history.go(-3)" class="task">2) Select seats</p>
                </div>
                <div class="col-md-2 col-xl-2 col-lg-2">
                    <p onclick="history.go(-2)" class="task">3) Customer info</p>
                </div>
                <div class="col-md-3 col-xl-3 col-lg-3">
                    <p onclick="history.back()" class="task">4) Passengers info</p>
                </div>
                <div class="col-md-2 col-xl-2 col-lg-2">
                    <p class="task" onclick="history.go(0)" style="background-color: #0789d3; color: white">5) Payment</p>
                </div>
            </div>
            <div>The remaining booking time is <span id="time" style="color: red;"></span> minutes!</div>
            <div class="row">
                <div class="col-md-12" style="margin-top: 50px">
                    <h5 class="billing-title">Booking info</h5>
                    <div class="framework">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover" style="text-align: center">
                                <thead>
                                    <tr>
                                        <th colspan="10" style="text-align: center; color: #0d89d2">${flightGo.flightRoute.departure.cityOfAirportEntity.name} - ${flightGo.flightRoute.destination.cityOfAirportEntity.name}</th>
                                    </tr>
                                    <tr>
                                        <th>Full name</th>
                                        <th>Address</th><!-- comment -->
                                        <th>Birth date</th>
                                        <th>Phone</th><!-- comment -->
                                        <th>Gender</th>
                                        <th>Id Card</th>
                                        <th>Take-off time</th>
                                        <th>Seat code</th>
                                        <th>Aircraft</th>
                                        <th>Unit price</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:if test="${booking_details != null && fn:length(booking_details) > 0}">
                                        <c:forEach items="${booking_details}" var="booking_detail">
                                            <c:if test="${booking_detail.flight.departureDate == flightGo.departureDate && booking_detail.flight.departureTime == flightGo.departureTime}">
                                                <tr>
                                                    <td>${booking_detail.passenger.fullName}</td><!-- comment -->
                                                    <td>${booking_detail.passenger.address}</td>
                                                    <td><fmt:formatDate value="${booking_detail.passenger.birthDate}" pattern="dd/MM/yyyy"/>
                                                    <td>${booking_detail.passenger.phone}</td>
                                                    <td>${booking_detail.passenger.gender}</td>
                                                    <td>${booking_detail.passenger.idCard}</td>           
                                                    <td>
                                                        <javatime:format value="${booking_detail.flight.departureDate}" pattern="dd-MM-yyyy"/> 
                                                        <c:if test="${booking_detail.flight.departureTime.getHour()-1 != -1}">
                                                            <c:if test="${booking_detail.flight.departureTime.getHour()-1 <= 9}">0</c:if>${booking_detail.flight.departureTime.getHour()-1}:<c:if test="${booking_detail.flight.departureTime.getMinute() <= 9}">0</c:if>${booking_detail.flight.departureTime.getMinute()}
                                                        </c:if>
                                                        <c:if test="${booking_detail.flight.departureTime.getHour()-1 == -1}">
                                                            23:<c:if test="${booking_detail.flight.departureTime.getMinute() <= 9}">0</c:if>${booking_detail.flight.departureTime.getMinute()}
                                                        </c:if>
                                                    </td>
                                                    <td>${booking_detail.seat.code}</td>
                                                    <td>${booking_detail.flight.aircraft.name} - ${booking_detail.flight.aircraft.aircraftNumber}</td>
                                                    <td><fmt:formatNumber value="${booking_detail.unitPrice}" pattern="###,###" type="number"/> $</td>
                                                </tr>
                                            </c:if>
                                        </c:forEach>
                                    </c:if>
                                    <c:if test="${booking_details == null || fn:length(booking_details) <= 0}">
                                        <tr>
                                            <td colspan="11" style="color: red">Empty List!!!</td>
                                        </tr>
                                    </c:if>
                                </tbody>
                            </table>

                            <c:if test="${flightReturn != null}">
                                <table class="table table-bordered table-hover" style="text-align: center">
                                    <thead>
                                        <tr>
                                            <th colspan="10" style="text-align: center; color: #0d89d2">${flightReturn.flightRoute.departure.cityOfAirportEntity.name} - ${flightReturn.flightRoute.destination.cityOfAirportEntity.name}</th>
                                        </tr>
                                        <tr>
                                            <th>Full name</th>
                                            <th>Address</th><!-- comment -->
                                            <th>Birth date</th>
                                            <th>Phone</th><!-- comment -->
                                            <th>Gender</th>
                                            <th>Id Card</th>
                                            <th>Take-off time</th>
                                            <th>Seat code</th>
                                            <th>Aircraft</th>
                                            <th>Unit price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:if test="${booking_details != null && fn:length(booking_details) > 0}">
                                            <c:forEach items="${booking_details}" var="booking_detail">
                                                <c:if test="${booking_detail.flight.departureDate == flightReturn.departureDate && booking_detail.flight.departureTime == flightReturn.departureTime}">
                                                    <tr>
                                                        <td>${booking_detail.passenger.fullName}</td><!-- comment -->
                                                        <td>${booking_detail.passenger.address}</td>
                                                        <td><fmt:formatDate value="${booking_detail.passenger.birthDate}" pattern="dd/MM/yyyy"/>
                                                        <td>${booking_detail.passenger.phone}</td>
                                                        <td>${booking_detail.passenger.gender}</td>
                                                        <td>${booking_detail.passenger.idCard}</td>           
                                                        <td>
                                                            <javatime:format value="${booking_detail.flight.departureDate}" pattern="dd-MM-yyyy"/> 
                                                            <c:if test="${booking_detail.flight.departureTime.getHour()-1 != -1}">
                                                                <c:if test="${booking_detail.flight.departureTime.getHour()-1 <= 9}">0</c:if>${booking_detail.flight.departureTime.getHour()-1}:<c:if test="${booking_detail.flight.departureTime.getMinute() <= 9}">0</c:if>${booking_detail.flight.departureTime.getMinute()}
                                                            </c:if>
                                                            <c:if test="${booking_detail.flight.departureTime.getHour()-1 == -1}">
                                                                23:<c:if test="${booking_detail.flight.departureTime.getMinute() <= 9}">0</c:if>${booking_detail.flight.departureTime.getMinute()}
                                                            </c:if>
                                                        </td>
                                                        <td>${booking_detail.seat.code}</td>
                                                        <td>${booking_detail.flight.aircraft.name} - ${booking_detail.flight.aircraft.aircraftNumber}</td>
                                                        <td><fmt:formatNumber value="${booking_detail.unitPrice}" pattern="###,###" type="number"/> $</td>
                                                    </tr>
                                                </c:if>
                                            </c:forEach>
                                        </c:if>
                                        <c:if test="${booking_details == null || fn:length(booking_details) <= 0}">
                                            <tr>
                                                <td colspan="11" style="color: red">Empty List!!!</td>
                                            </tr>
                                        </c:if>
                                    </tbody>
                                </table>
                            </c:if>
                            <strong style="color: red">Total price: <fmt:formatNumber value="${totalPrice}" pattern="###,###" type="number"/> $</strong>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12" style="margin-top: 18px;">
                    <h5 class="billing-title">Payment method</h5>
                    <div class="framework">
                        <ul>
                            <li style="padding: 20px"><a href="payment_by_credit_card" class="method">Credit card</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <p style="color: red">${message}</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h5 class="billing-title">Credit card</h5>
                    <div class="framework">
                        <mvc:form action="confirm" method="POST" modelAttribute="creditCard">
                            <label>Code*</label>
                            <input type="text" placeholder="code*" name="code" onfocus="this.placeholder = ''" onblur="this.placeholder = 'code*'" required class="common-input mt-20">
                            <label>Card number*</label>
                            <input type="text" placeholder="Card number*" name="cardNumber" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Card number*'" required class="common-input mt-20">
                            <label>Card name*</label>
                            <input type="text" placeholder="Card name*" name="cardName" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Card name*'" required class="common-input mt-20">
                            <label>Expiration date*</label>
                            <input type="month" placeholder="Expiration date*" name="cardExDate" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Expiration date*'" required class="common-input mt-20">
                            <button type="submit"class="view-btn color-2 mt-20 w-100" id="btn" style="margin-bottom: 20px"><span>Confirm</span></button>
                        </mvc:form>
                    </div>
                </div>
            </div>
        </div>
        <jsp:include page="include/footer.jsp"/>
        <script src="<c:url value="/resources/user/js/timeLeft.js"/>" type="text/javascript"></script>
    </body>
</html>