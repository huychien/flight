
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%> 
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://www.springframework.org/tags/form"
          prefix="mvc"%>
<!DOCTYPE html>
<html dir="ltr" lang="en">

    <head>
        <jsp:include page="../include/admin/head.jsp"/>

        <title>Ticket Form</title>

    </head>

    <body> 

        <div class="preloader">
            <div class="lds-ripple">
                <div class="lds-pos"></div>
                <div class="lds-pos"></div>
            </div>
        </div>

        <div id="main-wrapper" data-theme="light" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full"
             data-sidebar-position="fixed" data-header-position="fixed" data-boxed-layout="full">

            <header>
                <jsp:include page="../include/admin/header.jsp"/>
            </header>

            <aside>
                <jsp:include page="../include/admin/aside.jsp"/>
            </aside>

            <div class="page-wrapper">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="page-breadcrumb">
                    <div class="row">
                        <div class="col-7 align-self-center">
                            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Add Ticket</h4>
                            <div class="d-flex align-items-center">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb m-0 p-0">
                                        <li class="breadcrumb-item"><a href="index.html" class="text-muted">Ticket Manager</a></li>
                                        <li class="breadcrumb-item"><a href="index.html" class="text-muted">Ticket</a></li>
                                        <li class="breadcrumb-item text-muted active" aria-current="page">Add Ticket</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="container-fluid">

                    <mvc:form action="${pageContext.request.contextPath}/admin/${action}"
                              method="POST" modelAttribute="seats">
                        <c:if test="${action == 'update_ticket'}">
                            <input value="${seats.id}" name="id" hidden />
                        </c:if>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">


                                        <div class="form-body">
                                            <div class="row">
                                                <div class="col-md-11">                                               

                                                    
                                                    
                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <label>Code:</label><br>
                                                            <td><c:forEach items="${seatsList}" var="g">
                                                                    <c:if test="${g == seats.code}">
                                                                        <label>
                                                                            <input type="radio" name="code" 
                                                                                   value="${g}" checked/>${g}
                                                                        </label>
                                                                    </c:if>
                                                                    <c:if test="${g != seats.code}">
                                                                        <label>
                                                                            <input type="radio" name="code" 
                                                                                   value="${g}" required/>${g}
                                                                        </label>
                                                                    </c:if>

                                                                </c:forEach></td>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <label>SeatStatus:</label><br>  
                                                            <td><c:forEach items="${seatStatus}" var="g">
                                                                    <c:if test="${g == seats.seatStatus}">
                                                                        <label>
                                                                            <input type="radio" name="seatStatus" 
                                                                                   value="${g}" checked/>${g}
                                                                        </label>
                                                                    </c:if>
                                                                    <c:if test="${g != seats.seatStatus}">
                                                                        <label>
                                                                            <input type="radio" name="seatStatus" 
                                                                                   value="${g}" required/>${g}
                                                                        </label>
                                                                    </c:if>

                                                                </c:forEach></td>
                                                        </div>
                                                    </div>



                                                    <div class="col-md-11">
                                                        <div class="form-group">
                                                            <label>Aircraft</label>

                                                            <select name="aircraft" class="form-control" required>
                                                                <option value="">select </option>
                                                                <c:forEach items="${aircrafts}" var="ad">
                                                                    <c:if test="${ad.id == seats.aircraft.id}">
                                                                        <option value="${ad.id}" selected> ${ad.name} - ${ad.aircraftNumber}</option>
                                                                    </c:if>
                                                                    <c:if test="${ad.id != seats.aircraft.id}">
                                                                        <option value="${ad.id}"> ${ad.name} - ${ad.aircraftNumber}</option>
                                                                    </c:if>
                                                                </c:forEach>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <label>Seat Type:</label><br>
                                                            <c:forEach items="${seatType}" var="g">

                                                                <c:if test="${g.id == seats.seatType.id}">
                                                                    <label>
                                                                        <input type="radio" name="seatType" 
                                                                               value="${g.id}" checked/>${g.type}
                                                                    </label>
                                                                </c:if>
                                                                <c:if test="${g.id != seats.seatType.id}">
                                                                    <label>
                                                                        <input type="radio" name="seatType" 
                                                                               value="${g.id}" required/>${g.type}
                                                                    </label>
                                                                </c:if>
                                                            </c:forEach>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                            <div class="form-actions">
                                                <div class="text-right">


                                                    <c:if test="${action == 'add_ticket_form'}">
                                                        <button class="btn btn-success" type="submit">Create Ticket</button>
                                                        <button type="reset" class="btn btn-dark">Reset</button>
                                                    </c:if>
                                                    <c:if test="${action == 'update_ticket'}">
                                                        <button class="btn btn-warning" type="submit">Update Ticket</button>
                                                    </c:if>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>


                        </mvc:form>
                    </div>
                </div>

                <script src="<c:url value="/resources/admin/assets/libs/jquery/dist/jquery.min.js"></c:url>"></script>
                <script src="<c:url value="/resources/admin/assets/libs/popper.js/dist/umd/popper.min.js"></c:url>"></script>
                <script src="<c:url value="/resources/admin/assets/libs/bootstrap/dist/js/bootstrap.min.js"></c:url>"></script>
                    <!-- apps -->
                    <!-- apps -->
                    <script src="<c:url value="/resources/admin/dist/js/app-style-switcher.js"></c:url>"></script>
                <script src="<c:url value="/resources/admin/dist/js/feather.min.js"></c:url>"></script>
                <script src="<c:url value="/resources/admin/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></c:url>"></script>
                <script src="<c:url value="/resources/admin/dist/js/sidebarmenu.js"></c:url>"></script>
                    <!--Custom JavaScript -->
                    <script src="<c:url value="/resources/admin/dist/js/custom.min.js"></c:url>"></script>
                    <!--This page JavaScript -->
                    <script src="<c:url value="/resources/admin/assets/extra-libs/c3/d3.min.js"></c:url>"></script>
                <script src="<c:url value="/resources/admin/assets/extra-libs/c3/c3.min.js"></c:url>"></script>
                <script src="<c:url value="/resources/admin/assets/libs/chartist/dist/chartist.min.js"></c:url>"></script>
                <script src="<c:url value="/resources/admin/assets/libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js"></c:url>"></script>
                <script src="<c:url value="/resources/admin/assets/extra-libs/jvector/jquery-jvectormap-2.0.2.min.js"></c:url>"></script>
                <script src="<c:url value="/resources/admin/assets/extra-libs/jvector/jquery-jvectormap-world-mill-en.js"></c:url>"></script>
                <script src="<c:url value="/resources/admin/dist/js/pages/dashboards/dashboard1.min.js"></c:url>"></script>
                </body>
                </html>