
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%> 
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://www.springframework.org/tags/form"
          prefix="mvc"%>
<!DOCTYPE html>
<html dir="ltr" lang="en">

    <head>
        <jsp:include page="../include/admin/head.jsp"/>

        <title>AirPort Form</title>

    </head>

    <body> 

        <div class="preloader">
            <div class="lds-ripple">
                <div class="lds-pos"></div>
                <div class="lds-pos"></div>
            </div>
        </div>

        <div id="main-wrapper" data-theme="light" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full"
             data-sidebar-position="fixed" data-header-position="fixed" data-boxed-layout="full">

            <header>
                <jsp:include page="../include/admin/header.jsp"/>
            </header>

            <aside>
                <jsp:include page="../include/admin/aside.jsp"/>
            </aside>

            <div class="page-wrapper">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="page-breadcrumb">
                    <div class="row">
                        <div class="col-7 align-self-center">
                            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Add Airport</h4>
                            <div class="d-flex align-items-center">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb m-0 p-0">
                                        <li class="breadcrumb-item"><a href="index.html" class="text-muted">Flight Manager</a></li>
                                        <li class="breadcrumb-item"><a href="index.html" class="text-muted">Airport</a></li>
                                        <li class="breadcrumb-item text-muted active" aria-current="page">Add Airport</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="container-fluid">

                    <mvc:form action="${pageContext.request.contextPath}/admin/${action}"
                              method="POST" modelAttribute="airport">

                        <c:if test="${action == 'update'}">
                            <input value="${airport.id}" name="id" hidden />
                        </c:if>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">


                                        <div class="form-body">
                                            <div class="row">
                                                <div class="col-md-11">
                                                    <div class="form-group">
                                                        <label>Airport:</label>
                                                        <input type="text" class="form-control" id="name"
                                                               name="name" value="${airport.name}">
                                                    </div>
                                                </div>



                                                <div class="col-md-11">
                                                    <div class="form-group">
                                                        <label>City:</label>

                                                        <select name="cityOfAirportEntity.id" class="form-control" required>
                                                            <option value="">select city</option>
                                                            <c:forEach items="${cityOfAirports}" var="c">
                                                                <c:if test="${c.id == airport.cityOfAirportEntity.id}">
                                                                    <option value="${c.id}" selected>${c.name}</option>
                                                                </c:if>
                                                                <c:if test="${c.id != airport.cityOfAirportEntity.id}">
                                                                    <option value="${c.id}">${c.name}</option>
                                                                </c:if>
                                                            </c:forEach>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                    <div class="form-group">
                                                        <label>Status:</label>
                                                        <td><c:forEach items="${commomstatus}" var="g">
                                                                <c:if test="${g == airport.status}">
                                                                    <label>
                                                                        <input type="radio" name="status" 
                                                                               value="${g}" checked/>${g}
                                                                    </label>
                                                                </c:if>
                                                                <c:if test="${g != airport.status}">
                                                                    <label>
                                                                        <input type="radio" name="status" 
                                                                               value="${g}"/>${g}
                                                                    </label>
                                                                </c:if>

                                                            </c:forEach></td>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="form-actions">
                                            <div class="text-right">


                                                <c:if test="${action == 'add'}">
                                                    <button class="btn btn-success" type="submit">Create Airport</button>
                                                    <button type="reset" class="btn btn-dark">Reset</button>
                                                </c:if>
                                                <c:if test="${action == 'update'}">
                                                    <button class="btn btn-warning" type="submit">Update Airport</button>
                                                </c:if>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </mvc:form>
                </div>

            </div>


            <script src="<c:url value="/resources/admin/assets/libs/jquery/dist/jquery.min.js"></c:url>"></script>
            <script src="<c:url value="/resources/admin/assets/libs/popper.js/dist/umd/popper.min.js"></c:url>"></script>
            <script src="<c:url value="/resources/admin/assets/libs/bootstrap/dist/js/bootstrap.min.js"></c:url>"></script>
                <!-- apps -->
                <!-- apps -->
                <script src="<c:url value="/resources/admin/dist/js/app-style-switcher.js"></c:url>"></script>
            <script src="<c:url value="/resources/admin/dist/js/feather.min.js"></c:url>"></script>
            <script src="<c:url value="/resources/admin/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></c:url>"></script>
            <script src="<c:url value="/resources/admin/dist/js/sidebarmenu.js"></c:url>"></script>
                <!--Custom JavaScript -->
                <script src="<c:url value="/resources/admin/dist/js/custom.min.js"></c:url>"></script>
                <!--This page JavaScript -->
                <script src="<c:url value="/resources/admin/assets/extra-libs/c3/d3.min.js"></c:url>"></script>
            <script src="<c:url value="/resources/admin/assets/extra-libs/c3/c3.min.js"></c:url>"></script>
            <script src="<c:url value="/resources/admin/assets/libs/chartist/dist/chartist.min.js"></c:url>"></script>
            <script src="<c:url value="/resources/admin/assets/libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js"></c:url>"></script>
            <script src="<c:url value="/resources/admin/assets/extra-libs/jvector/jquery-jvectormap-2.0.2.min.js"></c:url>"></script>
            <script src="<c:url value="/resources/admin/assets/extra-libs/jvector/jquery-jvectormap-world-mill-en.js"></c:url>"></script>
            <script src="<c:url value="/resources/admin/dist/js/pages/dashboards/dashboard1.min.js"></c:url>"></script>
    </body>
</html>