
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%> 
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html dir="ltr" lang="en">

    <head>
        <jsp:include page="../include/admin/head.jsp"/>
        <link rel="stylesheet" href="<c:url value="/resources/user/css/bootstrap.css"/>">
    </head>

    <body>

        <div class="preloader">
            <div class="lds-ripple">
                <div class="lds-pos"></div>
                <div class="lds-pos"></div>
            </div>
        </div>

        <div id="main-wrapper" data-theme="light" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full"
             data-sidebar-position="fixed" data-header-position="fixed" data-boxed-layout="full">

            <header>
                <jsp:include page="../include/admin/header.jsp"/>
            </header>

            <aside>
                <jsp:include page="../include/admin/aside.jsp"/>
            </aside>

            <div class="page-wrapper">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="page-breadcrumb">
                    <div class="row">
                        <div class="col-7 align-self-center">
                            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Booking</h4>
                            <div class="d-flex align-items-center">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb m-0 p-0">
                                        <li class="breadcrumb-item"><a href="index.html" class="text-muted">Booking Manager</a></li>
                                        <li class="breadcrumb-item text-muted active" aria-current="page">List Booking</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                        <div class="col-5 align-self-center">

                            <form action="${pageContext.request.contextPath}/admin/search_booking"
                                  method="get" class="form-inline" name="strSearch">
                                <div class="form-group">
                                    <input name="strSearch" class="form-control"/>
                                    <input type="submit" value="Search" 
                                           class="btn btn-info" />
                                </div>
                            </form>
                        </div>

                    </div>
                </div>

                <div class="container-fluid">

                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <c:if test="${message != null && message != ''}">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-top: 10px">

                                                <c:if test="${type != null && type != '' && type == 'error'}">
                                                    <div class="alert alert-danger">${message}</div>
                                                </c:if>

                                                <c:if test="${type != null && type != '' && type == 'success'}">
                                                    <div class="alert alert-success">${message}</div>
                                                </c:if>

                                            </div>
                                        </div>
                                    </c:if>

                                    <div class="table-responsive">
                                        <!--                                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link"
                                                                                                             href="<c:url value="/admin/add_airport"></c:url>" aria-expanded="false"><span class="hide-menu">ADD Airport
                                                                                                                 </span></a>-->
                                            </li>
                                            <table id="zero_config" class="table table-striped table-bordered no-wrap">
                                                <thead>
                                                    <tr>
                                                        <th>Booking Code</th>
                                                        <th>Booking Date</th>
                                                        <th>Full Name</th>
                                                        <th>Discount </th>
                                                        <th>Total Price</th>
                                                        <th>Status </th>
                                                        
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <c:forEach items="${bookinges}" var="booking">
                                                    <tr>

                                                        <td>${booking.bookingCode}</td>
                                                        <td>${booking.bookingDate}</td>
                                                        <td>${booking.fullName}</td>
                                                        <td>${booking.discount}</td>
                                                        <td>${booking.totalPrice}</td>
                                                        <td>${booking.status}</td>
                                                        <td>
                                                            <button
                                                                onclick="location.href = '<c:url value="/admin/bookingdetail/${booking.id}"/>'">Detail</button>

                                                        </td>
                                                    </tr>
                                                </c:forEach>

                                            </tbody>

                                        </table>
                                        <ul class="pagination float-right">
                                            <li class="page-item disabled">
                                                <a class="page-link" href="#" tabindex="-1">Previous</a>
                                            </li>
                                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                            <li class="page-item">
                                                <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                                            </li>
                                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                                            <li class="page-item">
                                                <a class="page-link" href="#">Next</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>


            <script src="<c:url value="/resources/admin/assets/libs/jquery/dist/jquery.min.js"></c:url>"></script>
            <script src="<c:url value="/resources/admin/assets/libs/popper.js/dist/umd/popper.min.js"></c:url>"></script>
            <script src="<c:url value="/resources/admin/assets/libs/bootstrap/dist/js/bootstrap.min.js"></c:url>"></script>
                <!-- apps -->
                <!-- apps -->
                <script src="<c:url value="/resources/admin/dist/js/app-style-switcher.js"></c:url>"></script>
            <script src="<c:url value="/resources/admin/dist/js/feather.min.js"></c:url>"></script>
            <script src="<c:url value="/resources/admin/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></c:url>"></script>
            <script src="<c:url value="/resources/admin/dist/js/sidebarmenu.js"></c:url>"></script>
                <!--Custom JavaScript -->
                <script src="<c:url value="/resources/admin/dist/js/custom.min.js"></c:url>"></script>
                <!--This page JavaScript -->
                <script src="<c:url value="/resources/admin/assets/extra-libs/c3/d3.min.js"></c:url>"></script>
            <script src="<c:url value="/resources/admin/assets/extra-libs/c3/c3.min.js"></c:url>"></script>
            <script src="<c:url value="/resources/admin/assets/libs/chartist/dist/chartist.min.js"></c:url>"></script>
            <script src="<c:url value="/resources/admin/assets/libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js"></c:url>"></script>
            <script src="<c:url value="/resources/admin/assets/extra-libs/jvector/jquery-jvectormap-2.0.2.min.js"></c:url>"></script>
            <script src="<c:url value="/resources/admin/assets/extra-libs/jvector/jquery-jvectormap-world-mill-en.js"></c:url>"></script>
            <script src="<c:url value="/resources/admin/dist/js/pages/dashboards/dashboard1.min.js"></c:url>"></script>
    </body>
</html>