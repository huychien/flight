<%-- 
    Document   : passenger
    Created on : Dec 10, 2020, 1:04:09 PM
    Author     : huythang
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="include/head.jsp"/>
        <style>
            .task {
                text-align: center;
                color: #007bff;
                background-color: #fff;
                font-weight: bold;
            }
            form {
                display: grid;
                margin: 0 50px;
            }
            .common-input.mt-20 {
                margin-top: 0px;
                margin-bottom: 15px;
            }
            h5 {
                background-color: #0d89d2;
                color: white;
                padding: 10px;
            }
            .framework {
                border: 4px solid white;
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body style="background: #EDF3F7;">
        <jsp:include page="include/user/header.jsp"/>
        <section class="banner-area organic-breadcrumb" style="margin-bottom: 40px; margin-top: 130px;">
            <div class="container">
                <div class="breadcrumb-banner d-flex flex-wrap align-items-center">
                    <div class="col-first">
                        <h1>Passenger Page</h1>
                        <nav class="d-flex align-items-center justify-content-start">
                            <a href="<c:url value="/home"/>">Home<i class="fa fa-caret-right" aria-hidden="true"></i></a>
                            <a href="" onclick="history.go(0)">Passenger</a>
                        </nav>
                    </div>
                </div>
            </div>
        </section>
        <div class="container">
            <div class="row">
                <div class="col-md-2 col-xl-2 col-lg-2">
                    <p onclick="history.go(-3)" class="task">1) Flights</p>
                </div>
                <div class="col-md-3 col-xl-3 col-lg-3">
                    <p class="task" onclick="history.go(-2)">2) Select seats</p>   
                </div>
                <div class="col-md-2 col-xl-2 col-lg-2">
                    <p class="task" onclick="history.back()">3) Customer info</p>                
                </div>
                <div class="col-md-3 col-xl-3 col-lg-3">
                    <p class="task" onclick="history.go(0)" style="background-color: #0789d3; color: white">4) Passengers info</p>
                </div>
                <div class="col-md-2 col-xl-2 col-lg-2">
                    <p class="task">5) Payment</p>
                </div>
            </div>
            <div>The remaining booking time is <span id="time" style="color: red;"></span> minutes!</div>
            <h4 style="margin-top: 30px;">Who is flying?</h4>
            <p>Your passenger data</p>

            <input value="${infants}" type="hidden" id="infants">
            <input value="${adults}" type="hidden" id="adults">
            <input value="${children}" type="hidden" id="children"> 

            <div class="row">
                <div class="col-md-12">
                    <p id="message_for_adults<%=0%>"></p>
                    <h5 class="billing-title">Passenger</h5>
                    <div class="framework">
                        <form action="#" id="adults<%=0%>">
                            <label>Full name*</label>
                            <input type="text" placeholder="Full name*" name="fullName" value="${user.fullName}" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Full name*'" required class="common-input mt-20 adults">
                            <label>Phone number*</label>
                            <input type="tel" placeholder="Phone number*" name="phone" value="${user.phone}" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Phone number*'" required class="common-input mt-20 adults">
                            <label>Address*</label>
                            <input type="text" placeholder="Address*" name="address" value="${user.address}" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Address*'" required class="common-input mt-20 adults">
                            <label>BrithDate*</label>
                            <input type="date" placeholder="BirthDate*" name="birthDate" value="${user.birthDate}" onfocus="this.placeholder = ''" onblur="this.placeholder = 'BirthDate*'" required class="common-input mt-20 adults">
                            <label>Gender*</label>
                            <select name="gender" required class="common-input mt-20 adults<%=0%>">
                                <c:if test="${user.gender == 'MALE'}">
                                    <option selected value="${user.gender}">${user.gender}</option>
                                    <option value="FEMALE">FEMALE</option>
                                </c:if>
                                <c:if test="${user.gender != 'MALE'}">
                                    <option value="FEMALE">FEMALE</option>
                                    <option value="MALE">MALE</option>
                                </c:if>
                            </select>
                            <label>IdCard*</label>
                            <input type="number" placeholder="IdCard*" name="idCard" value="${user.idCard}" onfocus="this.placeholder = ''" onblur="this.placeholder = 'IdCard*'" required class="common-input mt-20 adults">
                        </form>
                    </div>
                </div>
            </div>

            <%  int adults = (int) session.getAttribute("adults");
                int children = (int) session.getAttribute("children");
                int infants = (int) session.getAttribute("infants");

                for (int i = 1; i < adults; i++) {%>
            <div class="row">
                <div class="col-md-12">
                    <p id="message_for_adults<%=i%>"></p>
                    <h5 class="billing-title">Passenger</h5>
                    <div class="framework">
                        <form action="#" id="adults<%=i%>">
                            <label>Full name*</label>
                            <input type="text" placeholder="Full name*" name="fullName" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Full name*'" required class="common-input mt-20 adults">
                            <label>Phone number*</label>
                            <input type="tel" placeholder="Phone number*" name="phone" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Phone number*'" required class="common-input mt-20 adults">
                            <label>Address*</label>
                            <input type="text" placeholder="Address*" name="address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Address*'" required class="common-input mt-20 adults">
                            <label>BrithDate*</label>
                            <input type="date" placeholder="BirthDate*" name="birthDate" onfocus="this.placeholder = ''" onblur="this.placeholder = 'BirthDate*'" required class="common-input mt-20 adults">
                            <label>Gender*</label>
                            <select name="gender" required class="common-input mt-20 adults<%=i%>">
                                <option selected value="MALE">MALE</option>
                                <option value="FEMALE">FEMALE</option>
                            </select>
                            <label>IdCard*</label>
                            <input type="number" placeholder="IdCard*" name="idCard" onfocus="this.placeholder = ''" onblur="this.placeholder = 'IdCard*'" required class="common-input mt-20 adults">
                        </form>
                    </div>
                </div>
            </div>
            <% } %>   

            <% for (int i = 0; i < children; i++) {%>
            <div class="row">
                <div class="col-md-12">
                    <p id="message_for_children<%=i%>"></p>
                    <h5 class="billing-title">Passenger(children)</h5>
                    <div class="framework">               
                        <form action="#" id="children<%=i%>">
                            <label>Full name*</label>
                            <input type="text" placeholder="Full name*" name="fullName" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Full name*'" required class="common-input mt-20 children"> 
                            <label>Address*</label>
                            <input type="text" placeholder="Address*" name="address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Address*'" required class="common-input mt-20 children">
                            <label>BrithDate*</label>
                            <input type="date" placeholder="BirthDate*" name="birthDate" onfocus="this.placeholder = ''" onblur="this.placeholder = 'BirthDate*'" required class="common-input mt-20 children">
                            <label>Gender*</label>
                            <select name="gender" required class="common-input mt-20 children<%=i%>">
                                <option selected value="MALE">MALE</option>
                                <option value="FEMALE">FEMALE</option>
                            </select>                 
                        </form>
                    </div>
                </div>
            </div>
            <% } %>

            <% for (int i = 0; i < infants; i++) {%>
            <div class="row">
                <div class="col-md-12">
                    <p id="message_for_infants<%=i%>"></p>
                    <h5 class="billing-title">Passenger(infants)</h5>
                    <div class="framework">                
                        <form action="#" id="infants<%=i%>">
                            <label>Full name*</label>
                            <input type="text" placeholder="Full name*" name="fullName" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Full name*'" required class="common-input mt-20 infants"> 
                            <label>Address*</label>
                            <input type="text" placeholder="Address*" name="address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Address*'" required class="common-input mt-20 infants">
                            <label>BrithDate*</label>
                            <input type="date" placeholder="BirthDate*" name="birthDate" onfocus="this.placeholder = ''" onblur="this.placeholder = 'BirthDate*'" required class="common-input mt-20 infants">
                            <label>Gender*</label>
                            <select name="gender" required class="common-input mt-20 infants<%=i%>">
                                <option selected value="MALE">MALE</option>
                                <option value="FEMALE">FEMALE</option>
                            </select>                  
                        </form>
                    </div>
                </div>
            </div>
            <% }%>     
            <button class="view-btn color-2 mt-20 w-100" id="btn" style="margin-bottom: 20px"><span>Continue</span></button>
        </div>
        <jsp:include page="include/footer.jsp"/>
        <script src="<c:url value="/resources/user/js/passenger.js"/>" type="text/javascript"></script>
        <script src="<c:url value="/resources/user/js/timeLeft.js"/>" type="text/javascript"></script>
    </body>
</html>