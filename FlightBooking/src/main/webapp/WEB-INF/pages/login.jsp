<%-- 
    Document   : login
    Created on : Jun 20, 2019, 8:17:26 PM
    Author     : AnhLe
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="include/head.jsp"/>
    </head>
    <body>
        <jsp:include page="include/header.jsp"/>
        <section class="banner-area organic-breadcrumb" style="margin-bottom: 40px; margin-top: 130px;">
            <div class="container">
                <div class="breadcrumb-banner d-flex flex-wrap align-items-center">
                    <div class="col-first">
                        <h1>Login Page</h1>
                        <nav class="d-flex align-items-center justify-content-start">
                            <a href="<c:url value="/home"/>">Home<i class="fa fa-caret-right" aria-hidden="true"></i></a>
                            <a href="<c:url value="/login"/>">Login</a>
                        </nav>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Banner Area -->

        <!-- Start My Account -->
        <div class="container">
            <c:if test="${message != null}">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-top: 10px">

                        <c:if test="${type == 'false'}">
                            <div class="alert alert-danger">${message}</div>
                        </c:if>

                        <c:if test="${type == 'true'}">
                            <div class="alert alert-success">${message}</div>
                        </c:if>

                    </div>
                </div>
            </c:if>
            <div class="row">
                <div class="col-md-12">
                    <div class="login-form">
                        <h3 class="billing-title text-center">Login</h3>
                        <p class="text-center mt-80 mb-40">Welcome back! Sign in to your account </p>
                        <form action="/FlightBooking/j_spring_security_check" method="post">
                            <p style="color: red; text-align: center">${error}</p>
                            <input type="email" name="username" placeholder="Email*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email*'" required class="common-input mt-20">
                            <input type="password" name="password" placeholder="Password*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Password*'" required class="common-input mt-20">          
                            <button type="submit" class="view-btn color-2 mt-20 w-100"><span>Login</span></button>
                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                            <div class="mt-20 d-flex align-items-center justify-content-between">
                                <div class="d-flex align-items-center"><input type="checkbox" class="pixel-checkbox" id="login-1"><label for="login-1">Remember me</label></div>
                                <a href="#">Lost your password?</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>     
        <jsp:include page="include/footer.jsp"/>
        <script>
            $(function () {
                sessionStorage.clear();
                $.ajax({url: "/FlightBooking/api/cancel_booking"});
            });
        </script>
    </body>
</html>