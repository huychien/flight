<%-- 
    Document   : select_seats
    Created on : Dec 8, 2020, 12:57:28 PM
    Author     : huythang
--%>

<%@page import="java.time.LocalDate"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://sargue.net/jsptags/time" prefix="javatime" %>
<%@ taglib prefix="mvc" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="include/head.jsp"/>
        <style>
            .task {
                text-align: center;
                color: #007bff;
                background-color: #fff;
                font-weight: bold;
            }
            .aircraft1, .aircraft2 {
                display: grid;
                grid-template-columns: auto auto auto auto;
                grid-gap: 10px;
                margin: 20px auto;
            }
            .aircraft1 {
                position: absolute;
                top: 555px;
                left: 32.5%;
            }
            .aircraft2 {
                position: absolute;
                top: 555px;
                right: 32.3%;
            }
            .note {
                background-color: #31ff39;
            }
            .red {
                background-color: red !important;
            }
        </style>     
    </head>
    <body style="background: #EDF3F7;">
        <jsp:include page="include/user/header.jsp"/>
        <section class="banner-area organic-breadcrumb" style="margin-bottom: 20px; margin-top: 130px;">
            <div class="container">
                <div class="breadcrumb-banner d-flex flex-wrap align-items-center">
                    <div class="col-first">
                        <h1>Seats List</h1>
                        <nav class="d-flex align-items-center justify-content-start">
                            <a href="<c:url value="/home"/>">Home<i class="fa fa-caret-right" aria-hidden="true"></i></a>
                            <a href="" onclick="history.go(0)">Select seats</a>
                        </nav>
                    </div>
                </div>
            </div>
        </section>
        <div class="container">
            <div class="row">
                <div class="col-md-2 col-xl-2 col-lg-2">
                    <p onclick="history.back()" class="task">1) Flights</p>
                </div>   
                <div class="col-md-3 col-xl-3 col-lg-3">
                    <p class="task" onclick="history.go(0)" style="background-color: #0789d3; color: white">2) Select seats</p>   
                </div>
                <div class="col-md-2 col-xl-2 col-lg-2">
                    <p class="task">3) Customer info</p>                
                </div>
                <div class="col-md-3 col-xl-3 col-lg-3">
                    <p class="task">4) Passengers info</p>
                </div>
                <div class="col-md-2 col-xl-2 col-lg-2">
                    <p class="task">5) Payment</p>
                </div>
            </div>
            <div id="warning">The remaining booking time is <span id="time" style="color: red;"></span> minutes!</div>
            <div class="row">
                <div style="text-align: center; width: 100%; margin:20px 0;">
                    <h3>Select for your seats</h3>
                </div>
            </div>
            <p id="message" style="text-align: center; color: red;"></p>
            <div class="row">
                <p style="text-align: center; color: black; margin: 0 auto">Select seats for flight away</p>
                <c:if test="${seatsOfAircraftReturn != null}">
                    <p style="text-align: center; color: black; margin: 0 auto">Select seats for return flight</p>
                </c:if>
            </div>

            <div class="row">
                <div style="margin: 0 auto">
                    <img src="<c:url value="/resources/user/img/ok.jpg"/>" width="460px" height="500px">
                </div>
                <c:if test="${seatsOfAircraftReturn != null}">
                    <div style="margin: 0 auto">
                        <img src="<c:url value="/resources/user/img/ok.jpg"/>" width="460px" height="500px">
                    </div>
                </c:if>
            </div>

            <div class="row">
                <div style="text-align: right; width: 100%; margin: 20px">
                    <span class="note" style="background-color: #00BCD4">_</span>&nbsp;BUSINESS&nbsp;&nbsp;&nbsp;
                    <span class="note">_</span>&nbsp;ECONOMY&nbsp;&nbsp;&nbsp;
                    <span class="note" style="background-color: red">_</span>&nbsp;BOOKED&nbsp;&nbsp;&nbsp;
                </div>
            </div>

            <div class="row">
                <div class="aircraft1" <c:if test="${seatsOfAircraftReturn == null}">style="position: absolute; top: 555px; left: 47.9%;"</c:if>>
                    <c:forEach items="${seatsOfAircraftGo}" var="seat">
                        <div style="text-align: center">
                            <span class="note <c:if test="${seat.seatStatus == 'BOOKED'}">red</c:if>" <c:if test="${seat.seatType.type == 'BUSINESS'}">style="background-color: #00BCD4"</c:if>>_</span><br>
                            <input type="radio" value="${seat.code}" <c:if test="${seat.seatType.type != seatType_go || seat.seatStatus == 'BOOKED'}">disabled</c:if> class="seatsOfAircraftGo" id="${seat.id}">
                            </div>
                    </c:forEach>        
                </div>
                <c:if test="${seatsOfAircraftReturn != null}">
                    <div class="aircraft2">
                        <c:forEach items="${seatsOfAircraftReturn}" var="seat">
                            <div style="text-align: center">
                                <span class="note <c:if test="${seat.seatStatus == 'BOOKED'}">red</c:if>" <c:if test="${seat.seatType.type == 'BUSINESS'}">style="background-color: #00BCD4"</c:if>>_</span><br>
                                <input type="radio" value="${seat.code}" <c:if test="${seat.seatType.type != seatType_return || seat.seatStatus == 'BOOKED'}">disabled</c:if> class="seatsOfAircraftReturn" id="${seat.id}">
                                </div>
                        </c:forEach>
                    </div>
                </c:if>
            </div>           

            <div class="row" style="margin-bottom: 20px;">
                <input value="${adults + children}" type="hidden" id="total_people">
            <input TYPE="button" onClick="history.go(0)" VALUE="Reselect">
            </div>
            <p id="advice" style="text-align: center; color: red;">Please choose enough seats</p>
            <div style="text-align: center; margin-bottom: 20px;">              
                <button id="button" disabled class="btn btn-success">Continue</button></td>
            </div>
        </div>           
        <jsp:include page="include/footer.jsp"/>
        <script src="<c:url value="/resources/user/js/selectSeats.js"/>" type="text/javascript"></script>
        <script src="<c:url value="/resources/user/js/timeLeft2.js"/>" type="text/javascript"></script>
    </body>
</html>
