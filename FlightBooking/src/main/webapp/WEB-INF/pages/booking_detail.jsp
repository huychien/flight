<%-- 
    Document   : booking_de
    Created on : Dec 23, 2020, 7:16:45 PM
    Author     : huythang
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://sargue.net/jsptags/time" prefix="javatime" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="include/head.jsp"/>
    </head>
    <body>
        <jsp:include page="include/user/header.jsp"/>
        <section class="banner-area organic-breadcrumb" style="margin-bottom: 40px; margin-top: 130px;">
            <div class="container">
                <div class="breadcrumb-banner d-flex flex-wrap align-items-center">
                    <div class="col-first">
                        <h1>Info Booking Page</h1>
                        <nav class="d-flex align-items-center justify-content-start">
                            <a href="<c:url value="/home"/>">Home<i class="fa fa-caret-right" aria-hidden="true"></i></a>
                            <a href="" onclick="history.go(0)">Booking Detail</a>
                        </nav>
                    </div>
                </div>
            </div>
        </section>
        <div class="container">
            <c:if test="${message != null}">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-top: 10px">

                        <c:if test="${type == 'false'}">
                            <div class="alert alert-danger">${message}</div>
                        </c:if>

                        <c:if test="${type == 'true'}">
                            <div class="alert alert-success">${message}</div>
                        </c:if>

                    </div>
                </div>
            </c:if>
        </div>            
        <div class="row" style="width: 80%; margin: auto;">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-top: 10px">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover" style="text-align: center">
                        <thead>
                            <tr>
                                <th>Full name</th>
                                <th>Address</th><!-- comment -->
                                <th>Birth date</th>
                                <th>Phone</th><!-- comment -->
                                <th>Gender</th>
                                <th>Id Card</th>
                                <th>Take-off time</th>
                                <th>Seat code</th>
                                <th>Aircraft</th>
                                <th>Unit price</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:if test="${booking_details != null && fn:length(booking_details) > 0}">
                                <c:forEach items="${booking_details}" var="booking_detail">
                                    <tr>
                                        <td>${booking_detail.passenger.fullName}</td><!-- comment -->
                                        <td>${booking_detail.passenger.address}</td>
                                        <td><fmt:formatDate value="${booking_detail.passenger.birthDate}" pattern="dd/MM/yyyy"/>
                                        <td>${booking_detail.passenger.phone}</td>
                                        <td>${booking_detail.passenger.gender}</td>
                                        <td>${booking_detail.passenger.idCard}</td>           
                                        <td><javatime:format value="${booking_detail.flight.departureDate}" pattern="dd-MM-yyyy"/> <c:if test="${booking_detail.flight.departureTime.getHour()-1 <= 9}">0</c:if>${booking_detail.flight.departureTime.getHour()-1}:<c:if test="${booking_detail.flight.departureTime.getMinute() <= 9}">0</c:if>${booking_detail.flight.departureTime.getMinute()}</td>
                                        <td>${booking_detail.seat.code}</td>
                                        <td>${booking_detail.flight.aircraft.name} - ${booking_detail.flight.aircraft.aircraftNumber}</td>
                                        <td><fmt:formatNumber value="${booking_detail.unitPrice}" pattern="###,###" type="number"/> $</td>
                                        <td>${booking_detail.status}</td>

                                        <c:if test="${booking_detail.status == 'BOOKED'}">
                                            <td>
                                                <button class="btn btn-danger" onclick="location.href = '<c:url value="/cancel_booking_detail/${booking_detail.id}/${booking_detail.booking.id}"></c:url>'">Cancel</button>
                                                </td>
                                        </c:if>
                                    </tr>               
                                </c:forEach>
                            </c:if>
                            <c:if test="${booking_details == null || fn:length(booking_details) <= 0}">
                                <tr>
                                    <td colspan="11" style="color: red">Empty List!!!</td>
                                </tr>
                            </c:if>
                                <tr>
                                    <th colspan="11" style="color: red">The amount paid: <fmt:formatNumber value="${paymentAmount.amount}" pattern="###,###" type="number"/> $</th>
                                </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <jsp:include page="include/footer.jsp"/>
        <script>
            $(function () {
                sessionStorage.clear();
                $.ajax({url: "/FlightBooking/api/cancel_booking"});
            });
        </script>
    </body>
</html>
