<%-- 
    Document   : head
    Created on : Nov 21, 2020, 9:51:12 AM
    Author     : huythang
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Author Meta -->
<meta name="author" content="CodePixar">
<!-- Meta Description -->
<meta name="description" content="">
<!-- Meta Keyword -->
<meta name="keywords" content="">
<!-- meta character set -->
<meta charset="UTF-8">
<!-- Site Title -->
<title>Flight</title>

<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">

<link rel="stylesheet" href="<c:url value="/resources/user/css/linearicons.css"/>">
<link rel="stylesheet" href="<c:url value="/resources/user/css/font-awesome.min.css"/>">
<link rel="stylesheet" href="<c:url value="/resources/user/css/fontawesome.min.css"/>">
<link rel="stylesheet" href="<c:url value="/resources/user/css/nice-select.css"/>">
<link rel="stylesheet" href="<c:url value="/resources/user/css/ion.rangeSlider.css"/>">
<link rel="stylesheet" href="<c:url value="/resources/user/css/ion.rangeSlider.skinFlat.css"/>">
<link rel="stylesheet" href="<c:url value="/resources/user/css/bootstrap.css"/>">
<link rel="stylesheet" href="<c:url value="/resources/user/css/main.css"/>">
<link rel="stylesheet" href="<c:url value="/resources/user/css/newcss.css"/>">
