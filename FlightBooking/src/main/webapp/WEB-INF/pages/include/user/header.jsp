<%-- 
    Document   : header
    Created on : Dec 21, 2020, 10:05:45 PM
    Author     : huythang
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<style>
    .menu-top li:hover {
        background-color: white;
    }
    .menu-top a:hover {
        color: #005cbf;
    }
    .dropdown:hover .dropdown-content {
        display: block;
    }
    .dropdown-content a:hover {background-color: #f1f1f1}
    .dropbtn {
        background-color: #4CAF50;
        color: white;
        padding: 16px;
        font-size: 16px;
        border: none;
        cursor: pointer;
    }

    .dropdown {
        position: relative;
        display: inline-block;
    }

    .dropdown-content {
        display: none;
        position: absolute;
        background-color: #f9f9f9;
        min-width: 160px;
        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
        z-index: 1;
    }

    .dropdown-content a {
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
    }
</style>
<header class="default-header">
    <div class="menutop-wrap">
        <div class="menu-top container">
            <div class="d-flex justify-content-between align-items-center">
                <ul class="list" style="display: flex;">
                    <li class="nav-item"><a class="nav-link" href="tel:0707572379">0707572379</a></li>
                    <li class="nav-item"><a class="nav-link" href="mailto:flightnow@online.com">flightnow@online.com</a></li>								
                </ul>
                <ul class="list" style="display: flex;">
                    <c:if test="${user != null}">
                        <li class="nav-item">
                            <div class="dropdown">
                                <a class="ddropbtn nav-link d-flex" href="viewInfo">
                                    Hello: ${user.fullName}
                                </a>
                                <div class="dropdown-content">
                                    <a href="<c:url value="/user/change_password_page" />">Change password</a>
                                    <a href="<c:url value="/user/update_info_page" />">Update info</a>
                                </div>
                            </div>
                        </li>
                    </c:if>
                    <jsp:include page="../menu.jsp" />
                </ul>
            </div>
        </div>					
    </div>
    <nav class="navbar navbar-expand-lg  navbar-light">
        <div class="container">
            <a href="<c:url value="/home" />" style="width: 150px;">
                <img src="<c:url value="/resources/user/img/logo (1).png"/>" alt="" style="width: 100%">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end align-items-center" id="navbarSupportedContent">
                <ul class="navbar-nav">
                    <li><a href="<c:url value="/home" />">Home</a></li>
                    <li><a href="<c:url value="/info_booking" />">Info booking</a></li>
                    <li><a href="#">promotion</a></li>
                    <li><a href="#">About us</a></li>
                    <!-- Dropdown -->
                    <li class="dropdown">
                        <a class="dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                            Pages
                        </a>
                        <div class="dropdown-menu" style="display: none;">
                            <a class="dropdown-item" href="<c:url value="/home" />">Home</a>
                            <a class="dropdown-item" href="#">promotion</a>
                            <a class="dropdown-item" href="#">About us</a>
                            <a class="dropdown-item" href="<c:url value="/view_booking" />">Info booking</a>
                            <a class="dropdown-item" href="<c:url value="/login" />">Login</a>
                            <a class="dropdown-item" href="<c:url value="/register" />">Register</a>
                        </div>
                    </li>									
                </ul>
            </div>						
        </div>
    </nav>
</header>
