<%-- 
    Document   : menu
    Created on : Jun 24, 2019, 7:18:41 PM
    Author     : AnhLe
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  uri="http://www.springframework.org/security/tags" prefix="sec" %>

<sec:authorize access="isAuthenticated()">
    <sec:authorize access="hasRole('ROLE_ADMIN')">
        <li class="nav-item">
            <a class="nav-link" href="<c:url value="/admin/home" />">Admin Home</a>
        </li>
    </sec:authorize>
    <sec:authorize access="hasRole('ROLE_ADMIN')">
        <li class="nav-item">
            <a class="nav-link" href="<c:url value="/user/home" />">User Home</a>
        </li>
    </sec:authorize>
    <sec:authorize access="hasRole('ROLE_MANAGER')">
        <li class="nav-item">
            <a class="nav-link" href="<c:url value="/manager/home" />">Manager Home</a>
        </li>
    </sec:authorize>
    <li class="nav-item">
        <a class="nav-link d-flex" href="<c:url value="/logout" />">
            <span>Logout</span>
        </a>
    </li>
</sec:authorize>
<sec:authorize access="!isAuthenticated()">
    <li class="nav-item">
        <a class="nav-link" href="<c:url value="/login" />">Login</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="<c:url value="/register" />">Register</a>
    </li>
</sec:authorize>
