<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<c:url value="/resources/admin/assets/images/favicon.png"></c:url>"/>
    <title>Adminmart Template - The Ultimate Multipurpose admin template</title>
    <!-- Custom CSS -->
    <link href="<c:url value="/resources/admin/assets/extra-libs/c3/c3.min.css"></c:url>" rel="stylesheet"/>
    <link href="<c:url value="/resources/admin/assets/libs/chartist/dist/chartist.min.css"></c:url>" rel="stylesheet"/>
    <link href="<c:url value="/resources/admin/assets/extra-libs/jvector/jquery-jvectormap-2.0.2.css"></c:url>" rel="stylesheet" />
    <!-- Custom CSS -->
    <link href="<c:url value="/resources/admin/dist/css/style.min.css"></c:url>" rel="stylesheet"/>
 
    