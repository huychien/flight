<%-- 
    Document   : aside
    Created on : Nov 29, 2020, 8:42:10 PM
    Author     : huycong
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<aside class="left-sidebar" data-sidebarbg="skin6">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar" data-sidebarbg="skin6">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                    <li class="sidebar-item"> 
                        <a class="sidebar-link sidebar-link" 
                            href="<c:url value="/admin/home"></c:url>" aria-expanded="false"></i>
                        <span class="hide-menu">Dashboard</span>
                        </a>
                    </li>
                    <li class="list-divider"></li>
                    
                    <li class="nav-small-cap"><span class="hide-menu">ADIMIN MANAGER</span></li>

                    <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="app-chat.html"
                                                 aria-expanded="false"></i><span
                                class="hide-menu">User List</span></a></li>

                    <li class="list-divider"></li>
                    <li class="nav-small-cap"><span class="hide-menu">FLIGHT BOOKING</span></li>
                    <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="<c:url value="/admin/booking"></c:url>"
                                                 aria-expanded="false"></i><span class="hide-menu">Booking</span></a></li>
                    
                    <li class="nav-small-cap"><span class="hide-menu">FLIGHT MANAGER</span></li>
                    
                    <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="<c:url value="/admin/flight"></c:url>"
                                                 aria-expanded="false"></i><span class="hide-menu">Flight</span></a></li>
                    
                    
                    <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="<c:url value="/admin/aircraft"></c:url>"
                                                 aria-expanded="false"></i><span class="hide-menu">Aircraft</span></a></li>
                                                 
                    <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="<c:url value="/admin/flight_route"></c:url>"
                                                 aria-expanded="false"></i>
                            <span class="hide-menu">Flight Route</span></a></li>



                    <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="<c:url value="/admin/airport"></c:url>"
                                                 aria-expanded="false"></i><span
                                class="hide-menu">Airport</span></a></li>


                    <li class="list-divider"></li>
                    <li class="nav-small-cap"><span class="hide-menu">TICKET MANAGER</span></li>

                    <li class="sidebar-item"> <a class="sidebar-link sidebar-link"
                                                 href="<c:url value="/admin/ticket"></c:url>" aria-expanded="false"><span class="hide-menu">List Ticket
                            </span></a>
                    </li>
                    <li class="list-divider"></li>
                    <li class="nav-small-cap"><span class="hide-menu">PROMOTION MANAGER</span></li>
                    <li class="sidebar-item"> <a class="sidebar-link sidebar-link"
                                                 href="<c:url value="/admin/promotion"></c:url>" aria-expanded="false"><span class="hide-menu">List Promotion
                        </span></a>
                </li>
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
