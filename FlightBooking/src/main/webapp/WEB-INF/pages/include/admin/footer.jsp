<%-- 
    Document   : footer
    Created on : Nov 21, 2020, 2:47:26 PM
    Author     : huythang
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<footer class="row tm-mt-small">
    <div class="col-12 font-weight-light">
        <p class="d-inline-block tm-bg-black text-white py-2 px-4">
            Copyright &copy; 2018 Admin Dashboard . Created by
            <a rel="nofollow" href="https://www.tooplate.com" class="text-white tm-footer-link">Tooplate</a>
        </p>
    </div>
</footer>
<script src="<c:url value="/resources/admin/js/jquery-3.3.1.min.js"/>"></script>
<!-- https://jquery.com/download/ -->
<script src="<c:url value="/resources/admin/js/moment.min.js"/>"></script>
<!-- https://momentjs.com/ -->
<script src="<c:url value="/resources/admin/js/utils.js"/>"></script>
<script src="<c:url value="/resources/admin/js/Chart.min.js"/>"></script>
<!-- http://www.chartjs.org/docs/latest/ -->
<script src="<c:url value="/resources/admin/js/fullcalendar.min.js"/>"></script>
<!-- https://fullcalendar.io/ -->
<script src="<c:url value="/resources/admin/js/bootstrap.min.js"/>"></script>
<!-- https://getbootstrap.com/ -->
<script src="<c:url value="/resources/admin/js/tooplate-scripts.js"/>"></script>
<script>
    let ctxLine,
            ctxBar,
            ctxPie,
            optionsLine,
            optionsBar,
            optionsPie,
            configLine,
            configBar,
            configPie,
            lineChart;
    barChart, pieChart;
    // DOM is ready
    $(function () {
        updateChartOptions();
        drawLineChart(); // Line Chart
        drawBarChart(); // Bar Chart
        drawPieChart(); // Pie Chart
        drawCalendar(); // Calendar

        $(window).resize(function () {
            updateChartOptions();
            updateLineChart();
            updateBarChart();
            reloadPage();
        });
    });
</script>
